   <!-- END THEME PANEL -->
                    <h1 class="page-title"> <?php echo __('Inbox')?> 
                    </h1>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a ><?php echo __('Account')?></a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <span><?php echo __('Inbox')?></span>
                            </li>
                        </ul> 
                    </div>
                    <!-- END PAGE HEADER-->
                    <div class="inbox">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="inbox-sidebar">
                                    <a href="javascript:;" data-title="Compose" class="btn red compose-btn btn-block">
                                        <i class="fa fa-edit"></i> <?php echo __('Compose')?> </a>
                                    <ul class="inbox-nav">
                                        <li class="active">
                                            <a href="javascript:;" data-type="inbox" data-title="Inbox"> <?php echo __('Inbox')?>
                                                <span class="badge badge-success">3</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" data-type="sent" data-title="Sent"> <?php echo __('Sent')?> </a>
                                        </li> 
                                        <li class="divider"></li>
                                        <li>
                                            <a href="javascript:;" class="sbold uppercase" data-title="Trash"> <?php echo __('Trash')?>
                                                <span class="badge badge-info">23</span>
                                            </a>
                                        </li>  
                                    </ul>
                                    
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="inbox-body">
                                    <div class="inbox-header">
                                        <h1 class="pull-left">Inbox</h1>
                                        <form class="form-inline pull-right" >
                                            <div class="input-group input-medium">
                                                <input type="text" class="form-control" placeholder="<?php echo __('Search')?>">
                                                <span class="input-group-btn">
                                                    <button type="submit" class="btn green">
                                                        <i class="fa fa-search"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="inbox-content" style="position: relative; zoom: 1;"> 
                                    
                                    
                                    
<table class="table table-striped table-advance table-hover">
    <thead>
        <tr>
            <th colspan="3">
                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                    <input type="checkbox" class="mail-group-checkbox" />
                    <span></span>
                </label>
                <div class="btn-group input-actions">
                    <a class="btn btn-sm blue btn-outline dropdown-toggle sbold" href="javascript:;" data-toggle="dropdown"> <?php echo __('Actions')?>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="javascript:;">
                                <i class="fa fa-pencil"></i> <?php echo __('Mark as Read')?> </a>
                        </li> 
                        <li class="divider"> </li>
                        <li>
                            <a href="javascript:;">
                                <i class="fa fa-trash-o"></i> <?php echo __('Delete')?> </a>
                        </li>
                    </ul>
                </div>
            </th>
            <th class="pagination-control" colspan="3">
                <span class="pagination-info"> 1-30 of 789 </span>
                <a class="btn btn-sm blue btn-outline">
                    <i class="fa fa-angle-left"></i>
                </a>
                <a class="btn btn-sm blue btn-outline">
                    <i class="fa fa-angle-right"></i>
                </a>
            </th>
        </tr>
    </thead>
    <tbody>
    	<?php foreach ($messages as $m):?>
        <tr class="unread" data-messageid="<?php $m->id?>">
            <td class="inbox-small-cells">
                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                    <input type="checkbox" class="mail-checkbox" value="<?php $m->id?>" />
                    <span></span>
                </label>
            </td> 
            <td class="view-message "> <?php echo $m->sender->username?> </td>
            <td class="view-message "> <?php echo $m->subject?> </td> 
            <td class="view-message text-right"> <?php echo $m->created?> </td>
        </tr> 
         <?php endforeach;?>
          
    </tbody>
</table>
                                    
                    </div>
                            </div>
                        </div>
                    </div>
    </div> 
<?= $this->Html->css('/assets/apps/css/inbox.min.css',['block' => true])?>           
<?= $this->Html->script('/kladstat/scripts/inbox.js?a='.time(),  ['block' => 'scriptBottom'])?> 