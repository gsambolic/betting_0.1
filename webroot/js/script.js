 (function(kladstat) {

	 kladstat(window.jQuery, window, document);

  }(function($, window, document) {

	$.fn.deserialize = function (serializedString)
	  {
	      var $form = $(this);
	      $form[0].reset();    // (A) optional
	      serializedString = serializedString.replace(/\+/g, '%20'); // (B)
	      var formFieldArray = serializedString.split("&");

	      // Loop over all name-value pairs
	      $.each(formFieldArray, function(i, pair){
	          var nameValue = pair.split("=");
	          var name = decodeURIComponent(nameValue[0]); // (C)
	          var value = decodeURIComponent(nameValue[1]);
	          // Find one or more fields
	          var $field = $form.find('[name="' + name + '"]');

            if(typeof $field[0] != "undefined")
            {
              // Checkboxes and Radio types need to be handled differently
  	          if ($field[0].type == "radio" || $field[0].type == "checkbox")
  	          {
  	              var $fieldWithValue = $field.filter('[value="' + value + '"]');
  	              var isFound = ($fieldWithValue.length > 0);
  	              // Special case if the value is not defined; value will be "on"
  	              if (!isFound && value == "on") {
  	                  $field.first().prop("checked", true);
  	              } else {
  	                  $fieldWithValue.prop("checked", isFound);
  	              }
  	          } else { // input, textarea
  	              $field.val(value);
  	          }
            }
	      });
	      return this;
	  }


   $(function() {
	   $(document).on('click',"#composeInboxAction",
		  function()
		  {
			ajaxForm.send($(this).closest('form'),
				function(parsedData,form)
				{
					bootbox.alert(parsedData.message);
					$(form)[0].reset();
					$("#inboxSendAction").click();
				}
			);
		  }
	   )

	   if($("#addEntityAction").length)
	   	{
	   		$("#addEntityAction").on('click',
	   				function()
	   				{
	   					var entityDatatableInner =  entryDataTable.getEntityDatatable().getDataTable().ajax;
	   					ajaxForm.send($(this).closest('form'),
   							function(parsedData,form)
   							{
   								bootbox.alert(parsedData.message);
   								$(form)[0].reset();
   								entityDatatableInner.reload();
   							}
	   					);

	   				}
	   		);
	   	}

	   if($("#addTicketAction").length)
	   	{
	   		$("#addTicketAction").on('click',
	   				function()
	   				{
	   					ajaxForm.send($(this).closest('form'),
  							function(parsedData,form)
  							{
			   					if( !$("#ticketId").val() )
								{
			   						$("#entityDatatable").attr('data-url',$("#entityDatatable").attr('data-url')+parsedData.id);
			   						bootbox.alert(parsedData.message);
			   						 entryDataTable.init();
			   						 $("#ticketId").val(parsedData.id);
	  								 $("#betTicketId").val(parsedData.id);
	  								 $("#addTicketAction").html(translate('Update'));
								}
  							},
	   						function(parsedData,form)
  							{
	   							bootbox.alert(parsedData.message);
  							}
	   					);
	   				}
	   		);
	   	}

	   if($(".myAccountAction").length)
	   	{
	   		$(".myAccountAction").on('click',
	   				function()
	   				{
			   			ajaxForm.send($(this).closest('form'),
							function(parsedData)
							{
								bootbox.alert(parsedData.message);
							}
		   				 );
	   				}
	   		);
	   	}

	   if($(".messageChangeStatusAction").length)
	   	{
	   		$(document).on('click','.messageChangeStatusAction',
   				function()
   				{
	   				var form = $("#formMessages");
   					var data = form.serialize();
   					if(data=='')
   					{
   						return;
   					}

   					url = form.attr('action');
   					if($(this).attr('data-action')=='delete')
   					{
   						url=url+"/"+$(this).attr('data-action')
   					}

	   				 $.ajax( {
	   			         type: "POST",
	   			         url: url,
	   			         data:data,
	   			         success: function( response )
	   			         {
	   			        	var parsedData = jQuery.parseJSON(response);

	   			        	if(parsedData.status==10 )
	   			        	{
	   			        		bootbox.alert(parsedData.message);
	   			        		return;
	   			        	}
	   			        	$('.inbox-nav > li.active').click();
	   			         },
	   			     });
   				}
	   		);
	   	}


      $( document ).ajaxComplete(function() {
          App.unblockUI('#contentRight')
      });

      $( document ).ajaxSend(function() {
        App.blockUI({
              target: '#contentRight',
              boxed: true,
              message: translate('Please wait...')
          });
      });
 
	   $.ajaxSetup({
	        error: function(jqXHR, exception)
	        {
	            if (jqXHR.status === 0)
	            {
	                alert(translate("Error on server, please try later."));
	                App.unblockUI('#contentRight');
	            }
	        },
	        statusCode: {
	            403: function (response) {
	            	window.location = BASE_URL;
	            },
	         },
	    });

   });

}));



//ajax from does not use validation, only one per page, needs to removed fixed id for multiple
var entryDataTable = (function() {

	var entityDatatable;

	 var getEntityDatatable = function()
	{
		return entityDatatable;
	}

   var init = function()
   {

	   entityDatatable = new  Datatable()
	   var dataEditRedirect= $("#entityDatatable").attr('data-edit-redirect');
	   var editUrl= $("#entityDatatable").attr('data-edit');

		  entityDatatable.init({
	    	  src: $("#entityDatatable"),
	          loadingMessage:translate('Loading...'),
	          dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
	        	  "columnDefs": [
	        	                 {
	        	                     "render": function ( data, type, row ) {

	        	                    	 if(dataEditRedirect=='true')
		        	                    	{
		        	                    		var columnRender = '<a   href="'+editUrl+data+'" data-id="'+data+'" class="btn btn-outline btn-circle btn-sm purple"><i class="fa fa-edit"></i>'+translate('Edit')+'</a>';
		    	        	                }
		        	                    	else
		        	                    	{
		        	                    		var columnRender = '<a href="javascript:void(0)" data-id="'+data+'" class="editRowEntityDatatableAction btn btn-outline btn-circle btn-sm purple"><i class="fa fa-edit"></i>'+translate('Edit')+'</a>';
		  	    	        	             }

	        	                    	 var columnRender=columnRender+'<a  href="javascript:void(0)"  data-id="'+data+'" class="deleteRowEntityDatatableAction btn btn-outline btn-circle red btn-sm blue"><i class="fa fa-trash-o"></i>'+translate('Delete')+'</a>';
		        	                    return columnRender;
	        	                     },
	        	                     "targets": -1
	        	                 },
                             {
                                "targets"  : 'no-sort',
                                "orderable": false,
                              }
	        	             ],
	        	  "pagingType": "full_numbers",
	        	  "serverSide": true,
	        	  "bStateSave": true,
	              "bLengthChange":false,
	              "searching": true,
	              "pageLength":20,
	              "ajax": {
	                  "url": $("#entityDatatable").attr('data-url'),
	              },
	          }
	      });

		  $('#entityDatatable').on( 'draw.dt', function () {
			    var searchValue = entryDataTable.getEntityDatatable().getDataTable().ajax.params().search.value;
				if(searchValue)
				{
					$("#searchEntityForm").deserialize(searchValue);
				}
		  } );

		  $(document).on('click',"#searchEntityAction",
			  function()
			  {
		  		entryDataTable.getEntityDatatable().getDataTable().search($("#searchEntityForm").serialize()).draw();
			  }
		  )

		   $(document).on('click',"#searchEntityActionReset",
			  function()
			  {
			    $('#searchEntityForm')[0].reset();
			   	entryDataTable.getEntityDatatable().getDataTable().search("").draw();
			  }
		  )

		  $(document).on('click',".deleteRowEntityDatatableAction",
			  function()
			  {
			  	var message =translate("Delete entry?");
			  	var entityDatatableInner = entityDatatable.getDataTable().ajax;
				var url = $("#entityDatatable").attr('data-delete')+$(this).attr('data-id');
			  	bootbox.confirm(message,
		  			function(result)
		  			{
			  			if(result)
			  			{
			  				$.ajax( {
			   			         type: "POST",
			   			         url: url,
			   			         success: function( response )
			   			         {
			   			        	entityDatatableInner.reload();
			   			         },
			   			     });
			  			}
		  			}
			  	)
			  }
		  )

		   $(document).on('click',"#editRowEntityButtonAction",
				 function()
				 {
			   		var entityDatatableInner = entityDatatable.getDataTable().ajax;
			   		ajaxForm.send($(this).closest('form'),
						function(parsedData,form)
						{
					  		bootbox.hideAll()
							entityDatatableInner.reload();
						}
					);
				 }
		   )

		   $(document).on('click',".editRowEntityDatatableAction",
			  function()
			  {
				   var entityDatatableInner = entityDatatable.getDataTable().ajax;
				   var url = $("#entityDatatable").attr('data-edit')+$(this).attr('data-id');

		  		    $.ajax( {
	   			         type: "POST",
	   			         url: url,
	   			         success: function( response )
	   			         {
		   			        bootbox.dialog({
		   			             message: response,
		   			             title: '',
		   			             buttons: {
		   			                 success: {
		   			                     label: translate("Close"),
		   			                     className: "btn-success",
		   			                 },
		   			             }
		   			         });
	   			         },
	   			     });
			  }
		  )
   }

   return {
	   init: init,
	   getEntityDatatable:getEntityDatatable
   };
})();

//ajax from does not use validation
var ajaxForm = (function() {

    var send = function(formObject,successCallback,failedCallback)
    {
    	if($(".ajaxFormErrorContainer").length)
    	{
    		$(".ajaxFormErrorContainer").remove();
    	}
    	var form = $(formObject);
    	var data = form.serialize();
    	url = form.attr('action');
    	 $.ajax( {
	         type: "POST",
	         url: url,
	         data:data,
	         success: function( response )
	         {
	        	var parsedData = jQuery.parseJSON(response);

	        	if(parsedData.status==1 )
	        	{
	        		if (typeof successCallback === "function") {
	        			successCallback(parsedData,form)
	        		}
		         }
	        	 else  if(parsedData.status==2 )
	        	{
	        		 window.location=parsedData.url;
	        	}
	        	 else  if(parsedData.status==10 )
	        	{
	        		 if (typeof failedCallback === "function") {
	        			 failedCallback(parsedData,form)
		        	 }
	        		 var messageData = "";
	        		 $.each(parsedData.errors,function( key, value ) {
	        			 $.each(value,function( keyInner, valueInner ) {
		        			  messageData=messageData+"<span>"+valueInner+"</span>";
		        			  var elementInputName = "#"+key;
		        			  var errorHtml = '<span id="'+key+'-error" class="ajaxFormErrorContainer has-error help-block">'+valueInner+'</span>';

		        			  $(elementInputName).after(errorHtml);
		        		 });
	        		 });

	        		 var errorMessage =  '<div class="alert alert-danger ajaxFormErrorContainer"><button class="close" data-close="alert"></button>'+messageData+'</div>';
	        	}
	     }});
    }

    return {
    	send: send
    };
})();
