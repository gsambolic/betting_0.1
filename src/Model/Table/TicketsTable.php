<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tickets Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\HasMany $Bets
 * @property \Cake\ORM\Association\BelongsToMany $Tags
 *
 * @method \App\Model\Entity\Ticket get($primaryKey, $options = [])
 * @method \App\Model\Entity\Ticket newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Ticket[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Ticket|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Ticket patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Ticket[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Ticket findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TicketsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tickets');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Bets', [
            'foreignKey' => 'ticket_id'
        ]);
        $this->belongsToMany('Tags', [
            'foreignKey' => 'ticket_id',
            'targetForeignKey' => 'tag_id',
            'joinTable' => 'tickets_tags'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('date_of_ticket')
            ->allowEmpty('date_of_ticket');

        $validator
            ->allowEmpty('system');

        $validator
            ->numeric('amount')
            ->allowEmpty('amount');

        $validator
            ->allowEmpty('comments');
      $validator
                ->allowEmpty('winning');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function getAmountSum($userId)
    {
      $ticketTableQuery = $this->find();
      return $ticketTableQuery->select(['sum' => $ticketTableQuery->func()->sum('Tickets.amount')])
      ->where(['Tickets.user_id' => $userId ])
      ->first();
    }

    public function getAmountOnWinSum($userId)
    {
      $ticketTableQuery = $this->find();
      return $ticketTableQuery->select(['sum' => $ticketTableQuery->func()->sum('Tickets.amount')])
      ->where(['Tickets.win' =>1,'Tickets.user_id' => $userId ])
      ->first();
    }

    public function getAmountOnLostSum($userId)
    {
      $ticketTableQuery = $this->find();
      return $ticketTableQuery->select(['sum' => $ticketTableQuery->func()->sum('Tickets.amount')])
      ->where(['Tickets.win' =>0,'Tickets.user_id' => $userId ])
      ->first();
    }

    public function getWinningSum($userId)
    {
      $ticketTableQuery = $this->find();
      return $ticketTableQuery->select(['sum' => $ticketTableQuery->func()->sum('Tickets.winning')])
      ->where(['Tickets.win' =>1,'Tickets.user_id' => $userId ])
      ->first();
    }

    public function getTotalCount($userId)
    {
      $ticketTableQuery = $this->find();
      return $ticketTableQuery->select(['count' => $ticketTableQuery->func()->count('Tickets.user_id')])
      ->where(['Tickets.user_id' => $userId ])
      ->first();
    }

    public function getWinCount($userId)
    {
      $ticketTableQuery = $this->find();
      return $ticketTableQuery->select(['count' => $ticketTableQuery->func()->count('Tickets.id')])
      ->where(['Tickets.win' =>1,'Tickets.user_id' => $userId ])
      ->first();
    }

    public function getLostCount($userId)
    {
      $ticketTableQuery = $this->find();
      return $ticketTableQuery->select(['count' => $ticketTableQuery->func()->count('Tickets.id')])
      ->where(['Tickets.win' =>0,'Tickets.user_id' => $userId ])
      ->first();
    }

}
