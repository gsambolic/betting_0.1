<?php
namespace App\Test\TestCase\Controller;

use App\Controller\BetsTypesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\BetsTypesController Test Case
 */
class BetsTypesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.bets_types',
        'app.users',
        'app.users_roles',
        'app.bets_slips',
        'app.transactions',
        'app.user_money_overalls',
        'app.bets',
        'app.groups',
        'app.sports'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
