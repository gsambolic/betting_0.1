<?php echo $this->element('breadcrumps',['data'=>[ ['name'=>__('Ticket')],['name'=>'List'] ]])?>

<h1 class="page-title"><?php echo __('Tickets')?></h1>


                            <div class="portlet light portlet-fit portlet-datatable ">
                                 <div class="portlet-body">
                                      <div class="row">
                                         <?= $this->Form->create(null,['id'=>'searchEntityForm','class'=>'iFormTopLeft' ]) ?>



                                         <div class="col-md-4 selectBoxWithFilter">
                                            <?php echo $this->Form->input('amount',array('type'=>'number','required'=>true,'class'=>'form-control','placeholder'=>__('Amount')))?>
                                            <?php echo $this->element('form_filter_by_size',['name'=>'amount_filter','options'=>['class'=>'form-control']]);?>
                                        </div>
                                        <div class="col-md-4 ">
                                              <?php echo $this->Form->input('win',['templates'=>$this->Layout->checkboxTemplate(),'class' => 'form-control', 'type'=>'checkbox']); ?>
                                        </div>

                                               <div class="col-md-4">
                                                  <?php echo $this->Form->input('name',array('required'=>true,'class'=>'form-control','placeholder'=>__('Name')))?>
                                              </div>

                                              <div class="col-md-4">
                                                 <?php echo $this->Form->input('system',array('required'=>true,'class'=>'form-control','placeholder'=>__('System')))?>
                                             </div>



                                            <div class="col-md-4">
                                              <div class="form-group">
                                                <label><?php echo __('Created');?></label>
                                                <div class="input-group input-daterange">
                                                  <input data-date-format="dd-mm-yyyy" type="text" name="created_from" class="form-control" >
                                                  <div class="input-group-addon"><?php echo __('to')?></div>
                                                  <input data-date-format="dd-mm-yyyy" type="text" name="created_to" class="form-control" >
                                                </div>
                                              </div>
                                            </div>

                                            <div class="col-md-4">
                                              <div class="form-group">
                                                <label><?php echo __('Time');?></label>
                                                <div class="input-group input-daterange">
                                                  <input date-format="dd-mm-yyyy" type="text" name="date_of_ticket_from"  class="form-control" >
                                                  <div class="input-group-addon"><?php echo __('to')?></div>
                                                  <input data-date-format="dd-mm-yyyy" type="text"  name="date_of_ticket_to" class="form-control" >
                                                </div>
                                              </div>
                                            </div>
                                            <div class="col-md-4 mt-checkbox-inlineBlock tagsTicketCont">
                                                <?php echo $this->Form->input('tags',['templates'=>$this->Layout->checkboxTemplate(),'class' => 'form-control','options'=>$tags,'multiple' => 'checkbox']); ?>
                                            </div>


                                            <div class="col-md-12" style="text-align:right;">
                                              <button type="button" id="searchEntityAction" class="btn green"><?php echo __('Search')?></button>
                                              <button type="button" id="searchEntityActionReset" class="btn green"><?php echo __('Reset')?></button>
                                            </div>
                                          <?= $this->Form->end() ?>
                                       </div>

                                        <table class="table table-striped table-bordered table-hover table-checkable" data-edit-redirect="true" data-delete="<?php echo  $this->Url->build(array('controller'=>'tickets','action'=>'delete')).DS;?>"  data-edit="<?php echo  $this->Url->build(array('controller'=>'bets','action'=>'entry')).DS;?>" data-url="<?php echo  $this->Url->build(array('controller'=>'tickets','action'=>'lists'));?>" id="entityDatatable">
                                            <thead>
                                                <tr role="row" class="heading">
                                                    <th  style="width:55px"> <?php echo __('Date')?> </th>
                                                      <th  > <?php echo __('Win')?> </th>
                                                    <th  > <?php echo __('Name')?> </th>
                                                    <th  > <?php echo __('Amount')?> </th>
                                                    <th  > <?php echo __('System')?> </th>
                                                    <th class="no-sort" > <?php echo __('Tags')?> </th>
                                                    <th  style="width:120px"> <?php echo __('Edit')?> </th>
                                                </tr>
                                            </thead>
                                            <tbody> </tbody>
                                        </table>
                                    </div>
                                </div>


   <script>
   $( document ).ready(function() {
	    entryDataTable.init()
    $('.input-daterange').datepicker({});
   });
   </script>



          <?= $this->Html->css('/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css',['block' => true])?>
          <?= $this->Html->css('/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',['block' => true])?>
          <?= $this->Html->css('/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',['block' => true])?>
          <?= $this->Html->css('/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',['block' => true])?>
          <?= $this->Html->css('/assets/global/plugins/clockface/css/clockface.css',['block' => true])?>

<?= $this->Html->css('/assets/global/plugins/datatables/datatables.min.css',['block' => true])?>
<?= $this->Html->css('/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',['block' => true])?>


<?= $this->Html->script('/assets/global/scripts/datatable.js',  ['block' => 'scriptBottom'])?>
 <?= $this->Html->script('/assets/global/plugins/datatables/datatables.min.js',  ['block' => 'scriptBottom'])?>
  <?= $this->Html->script('/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js',  ['block' => 'scriptBottom'])?>

  <?= $this->Html->script('/assets/global/plugins/moment.min.js',  ['block' => 'scriptBottom'])?>
  <?= $this->Html->script('/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js',['block' => 'scriptBottom'])?>
  <?= $this->Html->script('/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',['block' => 'scriptBottom'])?>
  <?= $this->Html->script('/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js',['block' => 'scriptBottom'])?>
  <?= $this->Html->script('/assets/global/plugins/clockface/js/clockface.js',['block' => 'scriptBottom'])?>
  <?= $this->Html->script('/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',['block' => 'scriptBottom'])?>
  <?= $this->Html->script('/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',['block' => 'scriptBottom'])?>
