<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $bet->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $bet->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Bets'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Groups'), ['controller' => 'Groups', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Group'), ['controller' => 'Groups', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Bets Types'), ['controller' => 'BetsTypes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Bets Type'), ['controller' => 'BetsTypes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sports'), ['controller' => 'Sports', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sport'), ['controller' => 'Sports', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="bets form large-9 medium-8 columns content">
    <?= $this->Form->create($bet) ?>
    <fieldset>
        <legend><?= __('Edit Bet') ?></legend>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('group_id', ['options' => $groups, 'empty' => true]);
            echo $this->Form->input('date_of_event', ['empty' => true]);
            echo $this->Form->input('bets_type_id', ['options' => $betsTypes, 'empty' => true]);
            echo $this->Form->input('winner_pick');
            echo $this->Form->input('selected_pick');
            echo $this->Form->input('user_id', ['options' => $users, 'empty' => true]);
            echo $this->Form->input('sport_id', ['options' => $sports, 'empty' => true]); 
            echo $this->Form->input('selected_pick_odds');
            echo $this->Form->input('win');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
