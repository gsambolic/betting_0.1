<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry; 
use App\Model\Application\Ajax\ResponsCodes;
/**
 * Groups Controller
 *
 * @property \App\Model\Table\GroupsTable $Groups
 */
class GroupsController extends AppController
{

	
	public function userGroups($id=0)
	{ 
		$groups = TableRegistry::get('Groups');
		$results = $groups->find('all', [ 
				'order' => ['Groups.name' => 'asc'], 
				'conditions' => [
					  ['Groups.sport_id'=>$id]	 ,
						[
						'OR'=>[ ['Groups.user_id' => $this->Auth->user('id')],['Groups.user_id' => 0]]	
						]
				],
				'fields'=>['Groups.id','Groups.name']
		]); 
		 
		$jsonData = array();
		
		foreach ($results as $s)
		{ 
			$jsonData[]=array('id'=>$s->id,'name'=>$s->name);
		} 
		echo json_encode($jsonData);
		exit;
	}
	   
	public function lists()
	{
		$order='Groups.name asc';
		if(!empty($this->request->data['order'][0]['dir'])):  
			if($this->request->data['order'][0]['column']==1)
			{
				$order='Sports.name '.$this->request->data['order'][0]['dir'];
			}
			else {
				$order='Groups.name '.$this->request->data['order'][0]['dir'];
			}
		endif;
	
		$conditions[] = array('Groups.user_id'=>$this->Auth->user('id'));
		if(!empty($this->request->data['search']['value'])):
			parse_str($this->request->data['search']['value'], $searchArray);
			if(!empty($searchArray['name']))
			{
				$conditions[] = array('Groups.name Like'=>'%'.$searchArray['name'].'%');
			}	
			
			if(!empty($searchArray['sport_id']))
			{
				$conditions[] = array('Groups.sport_id'=>$searchArray['sport_id']);
			}
			
		endif;
		
		$this->request->query['page']=(int) (((int)$this->request->data('start')+(int)$this->request->data('length'))/20);
		$this->paginate = [
				'contain'=>'Sports',
				'maxLimit' => 20,
				'order' => [
						$order
				],
				'conditions'=>$conditions
		];
	
	
		$groups = $this->paginate($this->Groups);
		$groupPrepared = [];
		foreach($groups as $g)
		{ 
			 $sportValue='';
			 if(!empty($g->sport->name))
			 {
			 	$sportValue = $g->sport->name;
			 }
			 //id always last
			$groupPrepared[] = [$g->name,$sportValue,$g->id];
		}
		echo json_encode(['recordsTotal'=> $this->request->params['paging']['Groups']['count']  ,'recordsFiltered'=>$this->request->params['paging']['Groups']['count'],'data'=>$groupPrepared]);
		exit;
	} 
	
	public function index()
	{
		$group = $this->Groups->newEntity(); 
	 
		$sportsTable = TableRegistry::get('Sports');
		$sportOptions = $sportsTable->find('list', [
				'order' => ['Sports.name' => 'asc'],
		
				'conditions' => [
						'OR'=>
						[
								['user_id' => $this->Auth->user('id')],
								['user_id' => 0]
						]
				],
		])->toArray(); 
		 
		$this->set(compact('group','sportOptions')); 
	} 
     
    public function add()
    {
    	$group = $this->Groups->newEntity();
    	if ($this->request->is('post')) {
    		
    		if(!empty($this->request->data['sport_id']))
    		{
    			$sports = TableRegistry::get('Sports');
    			$sport = $sports->get($this->request->data['sport_id'], [
    					'contain' => []
    			]);
    		
    			if($sport->user_id!=0&&$sport->user_id!=$this->Auth->user('id'))
    			{
    				$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED]);
    			}
    		}
    		
    		$group->user_id=$this->Auth->user('id');
            $group = $this->Groups->patchEntity($group, $this->request->data);
    		if ($this->Groups->save($group)) {
    			$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_SUCCESS,'message'=>__('Sport has been added.')]);
    		} else {
    			$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED, 'errors'=>$sport->errors()]);
    		}
    	}
    	$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED]);
    } 
    
    public function edit($id = null)
    {
    	$group = $this->Groups->get($id, [
            'contain' => []
        ]);
        if($group&&$group->user_id==$this->Auth->user('id'))
        { 
        	if(!empty($this->request->data['sport_id']))
        	{
        		$sports = TableRegistry::get('Sports');
        		$sport = $sports->get($this->request->data['sport_id'], [
        				'contain' => []
        		]);
        		
        		if($sport->user_id!=0&&$sport->user_id!=$this->Auth->user('id'))
        		{
        			$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED]);
        		} 
        	}
        	
        	if ($this->request->is(['patch', 'post', 'put'])) {
        		$group = $this->Groups->patchEntity($group, $this->request->data);
        		if ($this->Groups->save($group)) {
        			$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_SUCCESS,'message'=>__('Group has been updated.')]); 
        		} else {
        			 $this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED]);
        		}
        	} 
        }  
        $this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED]);
    	
    } 
    
    public function editView($id = null)
    {
    	$group = $this->Groups->get($id, [
    			'contain' => []
    	]);
    	 
    	if($group&&$group->user_id==$this->Auth->user('id'))
    	{  
	    	$sportsTable = TableRegistry::get('Sports');
	    	 
	    	$sportOptions = $sportsTable->find('list', [
	    			'order' => ['Sports.name' => 'asc'],
	    
	    			'conditions' => [
	    					'OR'=>
	    					[
	    							['user_id' => $this->Auth->user('id')],
	    							['user_id' => 0]
	    					]
	    			],
	    	])->toArray();
	    	$this->set(compact('group','sportOptions')); 
    	}
    	else
    	{ 
    		throw new MethodNotAllowedException();
    	}
    }
    
    /**
     * Delete method
     *
     * @param string|null $id Group id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    { 	
       $group = $this->Groups->get($id);
    
	    if($group&&$group->user_id==$this->Auth->user('id'))
	    {
	    	if ($this->Groups->delete($group)) {
	    		$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_SUCCESS,'message'=>__('Group has been deleted.')]);
	    	}
	    }
	    $this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED]);
    }
    
   
}
