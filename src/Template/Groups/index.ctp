 <?php echo $this->element('breadcrumps',['data'=>[ ['name'=>__('Groups')],['name'=>__('Add/List')] ]])?>


<div class="row">
 	<div class="col-xs-12">
 		<div class="portlet light ">
	        		<h4><?= __('Add Group') ?></h4>
		    <?= $this->Form->create($group,array('url'=>['action'=>'add'])) ?>
			 <div class="row">
			    <div class="col-xs-12 col-md-4"   >
				     <?php echo $this->Form->input('name',array('label'=>false,'required'=>true,'class'=>'form-control','placeholder'=>__('Name of Group')))?>
	           </div>

		      	<div class="col-xs-12 col-md-4"  >
		      		   <?php echo $this->Form->input('sport_id',array('label'=>false,'empty'=>__('(Select Sport)'),'options'=>$sportOptions,'class'=>'form-control'))?>
		       	</div>

		      	<div class="col-xs-6 col-md-4"  >
		        	<span class="input-group-btn">
	            	   <?= $this->Form->button(__('Submit'),array('type' => 'button','id'=>'addEntityAction','class'=>'btn green')) ?>
	       		 	</span>
		       	</div>
	       	</div>
	    	<?= $this->Form->end() ?>
	    </div>
	</div>
 </div>

<h1 class="page-title"><?php echo __('Groups')?></h1>


                            <div class="portlet light portlet-fit portlet-datatable ">
                                 <div class="portlet-body">
                                    <div class="table-container">
                                          <div class="row">
                                      	  <?= $this->Form->create(null,['id'=>'searchEntityForm']) ?>
                                       		<div class="col-xs-5">
                                      	 <?php echo $this->Form->input('name',array('label'=>false,'required'=>true,'class'=>'form-control','placeholder'=>__('Search')))?>
                                        </div>

                                        	<div class="col-xs-5"  >
									      		   <?php echo $this->Form->input('sport_id',array('label'=>false,'empty'=>__('(Select Sport)'),'options'=>$sportOptions,'class'=>'form-control'))?>
									       	</div>

                                        	<div class="col-xs-2">
                                        		<button type="button" id="searchEntityAction" class="btn green"><?php echo __('Search')?></button>
                                        		<button type="button" id="searchEntityActionReset" class="btn green"><?php echo __('Reset')?></button>
                                      	  </div>
                                      	   <?= $this->Form->end() ?>
                                        </div>
                                        <table  class="table table-striped table-bordered table-hover table-checkable" data-delete="<?php echo  $this->Url->build(array('controller'=>'groups','action'=>'delete')).DS;?>"  data-edit="<?php echo  $this->Url->build(array('controller'=>'groups','action'=>'edit_view')).DS;?>" data-url="<?php echo  $this->Url->build(array('controller'=>'groups','action'=>'lists'));?>" id="entityDatatable">
                                            <thead>
                                                <tr role="row" class="heading">

                                                    <th width="5%"> <?php echo __('Name')?> </th>
                                                    <th width="15%"> <?php echo __('Sport')?> </th>
                                                    <th width="15%"> <?php echo __('Edit')?> </th>
                                                </tr>
                                            </thead>
                                            <tbody> </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

   <script>
   $( document ).ready(function() {
	    entryDataTable.init()
   });
   </script>

<?= $this->Html->css('/assets/global/plugins/datatables/datatables.min.css',['block' => true])?>
<?= $this->Html->css('/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',['block' => true])?>
<?= $this->Html->script('/assets/global/scripts/datatable.js',  ['block' => 'scriptBottom'])?>
 <?= $this->Html->script('/assets/global/plugins/datatables/datatables.min.js',  ['block' => 'scriptBottom'])?>
  <?= $this->Html->script('/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js',  ['block' => 'scriptBottom'])?>
