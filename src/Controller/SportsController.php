<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use App\Model\Application\Ajax\ResponsCodes;
/**
 * Sports Controller
 *
 * @property \App\Model\Table\SportsTable $Sports
 */
class SportsController extends AppController
{
 
    public function index()
    {
    	$sport = $this->Sports->newEntity(); 
        $this->set(compact('sport')); 
    }
     
    
    public function lists()
    { 
    	$order='Sports.name asc';
    	if(!empty($this->request->data['order'][0]['dir'])):
    		 $order='Sports.name '.$this->request->data['order'][0]['dir']; 
    	endif;
    	 
    	$conditions[] = array('Sports.user_id'=>$this->Auth->user('id'));
    	if(!empty($this->request->data['search']['value'])):
    		parse_str($this->request->data['search']['value'], $searchArray);
    		$conditions[] = array('Sports.name Like'=>'%'.$searchArray['name'].'%');
    	endif;
    	
    	
    	$this->request->query['page']=(int) (((int)$this->request->data('start')+(int)$this->request->data('length'))/20);
    	$this->paginate = [
    			'maxLimit' => 20,
    			'order' => [
    					$order
    			],
    			'conditions'=>$conditions
    	];
    	 
    	 
    	$sports = $this->paginate($this->Sports);  
    	$sportPrepared = [];
    	foreach($sports as $s)
    	{
    		$sportPrepared[] = [$s->name,$s->id];
    	} 
     	echo json_encode(['recordsTotal'=> $this->request->params['paging']['Sports']['count']  ,'recordsFiltered'=>$this->request->params['paging']['Sports']['count'],'data'=>$sportPrepared]);
     	exit;
    }
     
    
    public function userSports()
    {  
    	$sports = TableRegistry::get('Sports');
    	$results = $sports->find('all', [
		    'order' => ['Sports.name' => 'DESC'],
	    			'fields' =>[
	    					'Sports.id','Sports.name'
	    			],
    			'conditions' => [
    					'OR'=>
    					[
    							['user_id' => $this->Auth->user('id')],
    							['user_id' => 0]
    					]
    			],
		]);
    	
    	$jsonData = array();
    	foreach ($results as $s)
    	{
    		$jsonData[]=array('id'=>$s->id,'name'=>$s->name);
    	}
 		echo json_encode($jsonData);
 		exit; 
    }
  
    public function add()
    {
        $sport = $this->Sports->newEntity();
        if ($this->request->is('post')) { 
        	$sport->user_id=$this->Auth->user('id');
        	$sport = $this->Sports->patchEntity($sport, $this->request->data);
            if ($this->Sports->save($sport)) { 
              $this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_SUCCESS,'message'=>__('Sport has been added.')]);
            } else {
              	$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED, 'errors'=>$sport->errors()]);
            } 
        }
        $this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED]);
    }

    
    public function edit($id = null)
    { 
        $sport = $this->Sports->get($id, [
            'contain' => []
        ]);
        if($sport&&$sport->user_id==$this->Auth->user('id'))
        { 
        	if ($this->request->is(['patch', 'post', 'put'])) {
        		$sport = $this->Sports->patchEntity($sport, $this->request->data);
        		if ($this->Sports->save($sport)) {
        			$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_SUCCESS,'message'=>__('Sport has been updated.')]); 
        		} else {
        			 $this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED]);
        		}
        	} 
        }  
        $this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED]);
    }
     
    public function editView($id = null)
    { 
    	$this->viewBuilder()->layout(false);
    	$sport = $this->Sports->get($id, [
    			'contain' => []
    	]);
    	if($sport&&$sport->user_id==$this->Auth->user('id'))
    	{
    		  $this->set(compact('sport'));
    	}
    	else
    	{
    		throw new MethodNotAllowedException();
    	} 
    }
 
    public function delete($id = null)
    {   
        $sport = $this->Sports->get($id);
        
        if($sport&&$sport->user_id==$this->Auth->user('id'))
        {
        	if ($this->Sports->delete($sport)) {
        		$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_SUCCESS,'message'=>__('Sport has been deleted.')]);
        	}
	    }
	    $this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED]);
    }
}
