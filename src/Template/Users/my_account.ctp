  <?php 
           $myTemplates = [
           		 'radio' => '<input type="radio" name="{{name}}" value="{{value}}"{{attrs}}><span></span>',
           ];
           $this->Form->templates($myTemplates);
        ?>
                    <!-- END PAGE HEADER-->
                    <div class="profile">
                        <div class="tabbable-line tabbable-full-width">
                            <ul class="nav nav-tabs"> 
                                <li>
                                    <a href="#tab_1_3" data-toggle="tab"> <?php echo __('Account')?> </a>
                                </li> 
                            </ul>
                            <div class="tab-content">
                                
                                <!--tab_1_2-->
                                <div class="tab-pane active  tab-pane" id="tab_1_3">
                                    <div class="row profile-account">
                                        <div class="col-md-3">
                                            <ul class="ver-inline-menu tabbable margin-bottom-10">
                                                <li class="active">
                                                    <a data-toggle="tab" href="#tab_1-1">
                                                        <i class="fa fa-cog"></i> <?php echo __('Personal info')?> </a>
                                                    <span class="after"> </span>
                                                </li> 
                                                <li>
                                                    <a data-toggle="tab" href="#tab_3-3">
                                                        <i class="fa fa-lock"></i> <?php echo __('Change Password')?> </a>
                                                </li>
                                                <li>
                                                    <a data-toggle="tab" href="#tab_4-4">
                                                        <i class="fa fa-eye"></i> <?php echo __('Privacity Settings')?> </a>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="tab-content">
                                                <div id="tab_1-1" class="tab-pane active">
                                                    <?php echo  $this->Form->create($user,['id'=>'myaccount-form-personal']) ?>
                                                        <div class="form-group">
                                                            <div class="form-group">
                   												<?php echo $this->Form->input('first_name', [ 'class'=>'form-control']);?>
										              		</div>
                                                            
                                                            </div>
                                                        <div class="form-group">
                                                            <?php echo $this->Form->input('last_name', [ 'class'=>'form-control']);?>
                                                       </div> 
                                                        <div class="form-group">
                                                            <?php echo $this->Form->input('email', [ 'class'=>'form-control']);?>
                                                       </div>  
                                                        <div class="margin-top-10" style="text-align:center">
                                                            <a href="javascript:void(0);"  class="btn green myAccountAction "> <?php echo __('Save Changes')?> </a> 
                                                        </div> 
           											 <?php echo  $this->Form->end() ; ?>
                                                </div> 
                                                <div id="tab_3-3" class="tab-pane">
                                                    <?php echo  $this->Form->create($user,['id'=>'myaccount-form-password']) ?>
                                                        <div class="form-group">
                   											<?php echo $this->Form->input('currentpassword', ['label'=>['text'=>__('Current Password')], 'class'=>'form-control']);?>
										              </div>
                                                            
                                                       <div class="form-group">
                   											<?php echo $this->Form->input('password', ['value'=>'','label'=>['text'=>__('Password')], 'class'=>'form-control']);?>
										              </div>
										                <div class="form-group">
										                	 <?php echo $this->Form->input('repassword', ['label'=>['text'=>__('Re-type New Password')],'type'=>'password','class'=>'form-control']);?>
										             	</div>   
										             	
										             	 <div class="margin-top-10" style="text-align:center">
                                                            <a href="javascript:void(0);"  class="btn green myAccountAction"> <?php echo __('Save Changes')?> </a> 
                                                        </div> 
           											 <?php echo  $this->Form->end() ; ?>
           											   
                                                </div>
                                                <div id="tab_4-4" class="tab-pane"> 
          											<?php echo  $this->Form->create($user,['id'=>'myaccount-form-privacy']) ?>
                                                        <table class="table table-bordered table-striped">
                                                            
                                                            <tr>
                                                                <td> <?php echo __('Newsletter?')?> </td>
                                                                <td>
                                                               
                                                                    <div class="mt-radio-inline">
                                                                        <?php 
			                                                                echo $this->Form->radio(
																				    'newsletter',
			                                                                		
																				    [
																				        ['value' => '1', 'text' => 'Yes'],
																				        ['value' => '0', 'text' => 'No'], 
																				    ],
			                                                                		[
																						'label'=>['class'=>'mt-radio'],
			                                                                			'hiddenField'=>false
			                                                                		]
																				);
			                                                                ?>
                                                                    </div>
                                                                </td>
                                                            </tr>  
                                                        </table>
                                                        <div class="margin-top-10" style="text-align:center">
                                                            <a href="javascript:void(0);"  class="btn green myAccountAction"> <?php echo __('Save Changes')?> </a> 
                                                        </div> 
                                                        <!--end profile-settings--> 
           											 <?php echo  $this->Form->end() ; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end col-md-9-->
                                    </div>
                                </div>  
                                <!--end tab-pane-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div> 
           
            <!-- END QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
<?= $this->Html->css('/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',['block' => true])?>
<?= $this->Html->css('/assets/pages/css/profile-2.min.css',['block' => true])?>
<?= $this->Html->script('/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js', ['block' => 'scriptBottom'])?>   