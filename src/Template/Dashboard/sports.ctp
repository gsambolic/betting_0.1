<script type="text/javascript">

$(function() {
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
      var data = google.visualization.arrayToDataTable([
         ['Sport', 'Win', 'Lost' ],
         <?php foreach($totalSports as $tS):?>
         ['<?php echo $tS->sport->name;?>', <?php echo $tS->win_count;?>, <?php echo $tS->count - $tS->win_count;?>],
        <?php endforeach;?>
        <?php if($sportNullCount):?>
          ['---', <?php echo $sportNullCount->win_count;?>, <?php echo $sportNullCount->count - $sportNullCount->win_count;?>],
        <?php endif;?>
       ]);
         var options = {
            width: '100%',
            legend: { position: 'top', maxLines: 3 },
            bar: { groupWidth: '75%',groupHeight:'100%' },
            isStacked: 'percent',
            chartArea:{top:20,width:"70%",height:"100%"}
         };
         var chart = new google.visualization.BarChart(document.getElementById("sportPlayedChart"));
         chart.draw(data, options);

    }
});
</script>
<?php echo $this->element('breadcrumps',['data'=>[ ['name'=>__('Sports/Groups')],['name'=>'Overview'] ]])?>


<div class="panel panel-default" style="margin-bottom:20px;">
     <div class="panel-heading">
           <h3 class="panel-title">
           <?php echo __('Sports')?>
           </h3>
     </div>
     <div class="panel-body">
       <div class="col-lg-6">
           <div class="table-scrollable table-scrollable-borderless">
                   <table class="table table-hover table-light">
                       <thead>
                           <tr class="uppercase">
                               <th ><?php echo __('Sport');?> </th>
                               <th ><?php echo __('Played');?></th>
                               <th ><?php echo __('Win');?> </th>
                               <th><?php echo __('Lost');?> </th>
                               <th><?php echo __('Percentage');?> </th>
                           </tr>
                       </thead>
                       <tbody>
                          <?php foreach($totalSports as $tS):?>
                             <tr>
                               <td ><?php echo $tS->sport->name;?></td>
                               <td><?php echo $tS->count;?></td>
                               <td><?php echo $tS->win_count;?></td>
                               <td><?php echo $tS->count - $tS->win_count;?></td>
                               <td>
                                <?php if($tS->win_count==0):?>
                                    0
                                <?php endif;?>
                                <?php if($tS->win_count!=0):?>
                                    <?php echo number_format(($tS->win_count/$tS->count)*100,2);?>%</td>
                                <?php endif; ?>
                             </tr>
                        <?php endforeach;?>
                        <?php if($sportNullCount):?>
                          <tr>
                            <td >---</td>
                            <td><?php echo $sportNullCount->count;?></td>
                            <td><?php echo $sportNullCount->win_count;?></td>
                            <td><?php echo $sportNullCount->count - $sportNullCount->win_count;?></td>
                            <td>
                             <?php if($sportNullCount->win_count==0):?>
                                 0
                             <?php endif;?>
                             <?php if($sportNullCount->win_count!=0):?>
                                 <?php echo number_format(($sportNullCount->win_count/$sportNullCount->count)*100,2);?>%</td>
                             <?php endif; ?>
                          </tr>
                       <?php endif;?>
                   </tbody></table>
               </div>
       </div>
       <div class="col-lg-6">
          <div style="height:<?php echo $totalSports->count()*34+50;?>px" id="sportPlayedChart"></div>
       </div>
  </div>
</div><


<script type="text/javascript">

$(function() {
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChartGroup);

    function drawChartGroup() {
      var data = google.visualization.arrayToDataTable([
         ['Sport', 'Win', 'Lost' ],
         <?php foreach($totalGroups as $gS):?>
         ['<?php echo $gS->group->name;?>', <?php echo $gS->win_count;?>, <?php echo $gS->count - $gS->win_count;?>],
        <?php endforeach;?>
        <?php if($groupNullCount):?>
          ['---', <?php echo $groupNullCount->win_count;?>, <?php echo $groupNullCount->count - $groupNullCount->win_count;?>],
        <?php endif;?>
       ]);
         var options = {
            width: '100%',
            legend: { position: 'top', maxLines: 3 },
            bar: { groupWidth: '75%',groupHeight:'100%' },
            isStacked: 'percent',
            chartArea:{top:20,width:"70%",height:"100%"}
         };
         var chart = new google.visualization.BarChart(document.getElementById("groupPlayedChart"));
         chart.draw(data, options);

    }
});
</script>


<div class="panel panel-default" >
     <div class="panel-heading">
           <h3 class="panel-title">
           <?php echo __('Groups')?>
           </h3>
     </div>
     <div class="panel-body">
       <div class="col-lg-6">
           <div class="table-scrollable table-scrollable-borderless">
                   <table class="table table-hover table-light">
                       <thead>
                           <tr class="uppercase">
                             <th ><?php echo __('Group');?> </th>
                             <th ><?php echo __('Sport');?> </th>
                             <th ><?php echo __('Played');?></th>
                             <th ><?php echo __('Win');?> </th>
                             <th><?php echo __('Lost');?> </th>
                             <th><?php echo __('Percentage');?> </th>
                           </tr>
                       </thead>
                  <tbody>
                          <?php foreach($totalGroups as $gS):?>
                             <tr>
                               <td ><?php echo $gS->group->name;?></td>
                              <td ><?php echo $gS->group->sport->name;?></td>
                               <td><?php echo $gS->count;?></td>
                               <td><?php echo $gS->win_count;?></td>
                               <td><?php echo $gS->count - $gS->win_count;?></td>
                               <td>
                                <?php if($gS->win_count==0):?>
                                    0
                                <?php endif;?>
                                <?php if($gS->win_count!=0):?>
                                    <?php echo number_format(($gS->win_count/$gS->count)*100,2);?>%</td>
                                <?php endif; ?>
                             </tr>
                        <?php endforeach;?>

                        <?php if($groupNullCount):?>
                          <tr>
                            <td >---</td>
                            <td><?php echo $groupNullCount->count;?></td>
                            <td><?php echo $groupNullCount->win_count;?></td>
                            <td><?php echo $groupNullCount->count - $groupNullCount->win_count;?></td>
                            <td>
                             <?php if($groupNullCount->win_count==0):?>
                                 0
                             <?php endif;?>
                             <?php if($groupNullCount->win_count!=0):?>
                                 <?php echo number_format(($groupNullCount->win_count/$groupNullCount->count)*100,2);?>%</td>
                             <?php endif; ?>
                          </tr>
                       <?php endif;?>
                   </tbody>

                 </table>
               </div>
       </div>
       <div class="col-lg-6">
          <div style="height:<?php echo $totalGroups->count()*34+100;?>px" id="groupPlayedChart"></div>
       </div>
</div>


<?= $this->Html->script('charts/google/loader.js',['block' => 'scriptBottom'])?>
