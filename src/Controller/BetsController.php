<?php
namespace App\Controller;
use Cake\ORM\TableRegistry;

use App\Controller\AppController;
use Cake\Controller\Component\AuthComponent;
use App\Model\Application\Ajax\ResponsCodes;

/**
 * Bets Controller
 *
 * @property \App\Model\Table\BetsTable $Bets
 */
class BetsController extends AppController
{

	public function listsTicketBets($ticketId)
	{
		$order='Bets.id desc';

		$this->request->query['page']=(int) (((int)$this->request->data('start')+(int)$this->request->data('length'))/20);
		$this->paginate = [
				'contain'=>['Sports','BetsTypes','Groups','Tags'],
				'maxLimit' => 50,
				'order' => [
						$order
				],
				'conditions'=>array('Bets.user_id'=>$this->Auth->user('id'),'Bets.ticket_id'=>$ticketId)
		];


		$bets = $this->paginate($this->Bets);
		$betPrepared = [];
		foreach($bets as $b)
		{
			$arrayToUser=[];
			if($b->date_of_event)
			{
				$arrayToUser[]=date('d-m-Y',strtotime($b->date_of_event));
			}
			else
			{
				$arrayToUser[]="---";
			}

			if($b->win)
			{
				$arrayToUser[]=_("Win");
			}
			else
			{
				$arrayToUser[]=__("Lost");
			}

			if(isset($b->sport))
			{
					$arrayToUser[]=$b->sport->name;
			}
			else {
					$arrayToUser[]="---";
			}

			if(isset($b->group))
			{
					$arrayToUser[]=$b->group->name;
			}
			else {
					$arrayToUser[]="---";
			}

			if(isset($b->bets_type))
			{
					$arrayToUser[]=$b->bets_type->name;
			}
			else {
					$arrayToUser[]="---";
			}

			if(isset($b->name))
			{
					$arrayToUser[]=$b->name;
			}
			else {
					$arrayToUser[]="---";
			}

			if($b->home_or_away==1)
			{
				$arrayToUser[]=__("Home");
			}
			else if($b->home_or_away==2)
			{
				$arrayToUser[]=__("Away");
			}
			else if($b->home_or_away==3)
			{
				$arrayToUser[]=__("Neutral");
			}
			else
			{
				$arrayToUser[]="---";
			}

			if($b->selected_pick)
			{
				$arrayToUser[]=$b->selected_pick;
			}
			else
			{
				$arrayToUser[]='---';
			}

			if($b->selected_pick_odds)
			{
				$arrayToUser[]=$b->selected_pick_odds;
			}
			else
			{
				$arrayToUser[]='---';
			}

			if($b->winner_pick)
			{
				$arrayToUser[]=$b->winner_pick;
			}
			else
			{
				$arrayToUser[]='---';
			}


			$tagsString='';
			if($b->tags)
			{
				$divader="";
				foreach($b->tags as $tag)
				{
					$tagsString.=$divader.$tag->name;
					$divader.=", ";
				}
			}	;
			$arrayToUser[]=$tagsString;
			$arrayToUser[]=$b->id;
			$betPrepared[]=$arrayToUser;
		}
		echo json_encode(['recordsTotal'=> $this->request->params['paging']['Bets']['count']  ,'recordsFiltered'=>$this->request->params['paging']['Bets']['count'],'data'=>$betPrepared]);
		exit;
	}

	public function editView($id=null)
	{
		$this->set('noShowTicket',true);
		$this->entry(null,$id);
		$this->render('entry');
	}


	public function entry($ticketId=null,$id=null)
	{
		$betsTable = $this->Bets;
		$bet = $this->Bets->newEntity();
		$ticketsTable = TableRegistry::get('Tickets');
		$ticket = $ticketsTable->newEntity();

		$this->set('betsTable',$betsTable);
		$tagsTable = TableRegistry::get('Tags');
		$tagsQuery =   $tagsTable->find('list');
		$tagsQuery->where(['Tags.user_id'=>$this->Auth->user('id')]);
		$tagsArray = $tagsQuery->toArray();
		$tagsBetsSelectedValues=[];
		if($id!==null)
		{
			$betsQuery = $this->Bets->find('all')
			->where(['Bets.id' => $id ])
			->where(['Bets.user_id' => $this->Auth->user('id') ])
			->contain(['Tickets','Tags']);
			if(!($bet = $betsQuery->first()))
			{
				$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED, 'errors'=>['tags'=>['not valid'=>__('Bet not valid')]]]);
			}

			foreach($bet->tags as $bArray)
			{
				$tagsBetsSelectedValues[]=$bArray->id;
			}
			$ticketId = $bet->ticket_id;
		}

		$tagsSelectedValues=[];
		if(!empty($this->request->data('ticket_id'))||!empty($ticketId))
		{
			if(!empty($ticketId))
			{
				$this->request->data['ticket_id']=$ticketId;
			}

			$ticketQuery =$ticketsTable->find('all')
			->contain(['Tags'])
			->where(['Tickets.id' => $this->request->data['ticket_id']] )
			->where(['Tickets.user_id' => $this->Auth->user('id') ]);

				if(!($ticket = $ticketQuery->first()))
				{
					$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED, 'errors'=>['tags'=>['not valid'=>__('Bet not valid')]]]);
				}

				foreach($ticket->tags as $tArray)
				{
					$tagsSelectedValues[]=$tArray->id;
				}

		}

		$this->set('tagsSelectedValues',$tagsSelectedValues);
		$this->set('tagsBetsSelectedValues',$tagsBetsSelectedValues);

		$this->set(compact('betsTable','bet'));
		$sportsTable = TableRegistry::get('Sports');

		if ($this->request->is('post')||$this->request->is('put')) {

			$error = false;
			if(!empty($this->request->data['tags']['_ids']))
			{
				if(!is_array($this->request->data['tags']['_ids']))
				{
					$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED, 'errors'=>['tags'=>['not valid'=>__('Tags are not valid')]]]);
				}
				else
				{
					$allowedTagsIds = array_keys($tagsArray);
					foreach ($this->request->data['tags']['_ids'] as $t)
					{
						if(!in_array($t, $allowedTagsIds))
						{
							$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED, 'errors'=>['tags'=>['not allowed'=>__('Tag are not allowed')]]]);
						}
					}
				}
			}

			if(!empty($this->request->data['tags_text']))
			{
				$explodedTags = explode(',', $this->request->data['tags_text']);
				foreach ($explodedTags as $keyET=>$eT)
				{
					if(!in_array($eT, $tagsArray))
					{
						$explodedTagsFiltered[$keyET]['name']=trim($eT);
						$explodedTagsFiltered[$keyET]['user_id']=$this->Auth->user('id');
					}
				}
			}
			if(!empty($explodedTagsFiltered))
			{
				if(!empty($this->request->data['tags']['_ids']))
				{
					foreach ($this->request->data['tags']['_ids'] as $tIDS)
					{
						$explodedTagsFiltered[]=array('id'=>$tIDS);
					}
				}
			}

			if(!empty($explodedTagsFiltered))
			{
				$this->request->data['tags']=$explodedTagsFiltered;
			}

			if(!$error)
			{
		 		if(!empty($this->request->data['sport_name']))
		 		{

					$sportQuery = $sportsTable->find('all')
						    ->orWhere(['user_id' => $this->Auth->user('id')])
						    ->orWhere(['user_id' => 0])
						    ->andWhere(['name' => trim(strtolower($this->request->data('sport_name')) )]);

					if($sportSelected = $sportQuery->first())
					{
						$this->request->data['sport_id']=$sportSelected->id;
					}
					else
					{
						$sport = $sportsTable->newEntity();
						$sport->user_id = $this->Auth->user('id');
						$sportData = array('name'=>trim(strtolower($this->request->data('sport_name'))));
						$sport = $sportsTable->patchEntity($sport, $sportData);
						$sportResult = $sportsTable->save($sport);
						$this->request->data['sport_id']=$sportResult->id;
					}
		 		}

			 	if(!empty($this->request->data['group_name']))
			 	{
			 		$groupsTable = TableRegistry::get('Groups');
			 		$group = $groupsTable->newEntity();
			 		$groupQuery = $groupsTable->find('all')
			 		->orWhere(['user_id' => $this->Auth->user('id')])
			 		->orWhere(['user_id' => 0])
			 		->where(['name' => trim(strtolower($this->request->data('group_name')) )]);

			 		if($groupQuerySelected = $groupQuery->first())
			 		{
			 			$this->request->data['group_id']=$groupQuerySelected->id;
			 		}
			 		else
			 		{
			 			$groupData = array('sport_id'=>$this->request->data('sport_id'),'name'=>strtolower(trim($this->request->data('group_name'))));
						$group->user_id=$this->Auth->user('id');
						$group = $groupsTable->patchEntity($group, $groupData);
			 			$groupResult = $groupsTable->save($group);
			 			$this->request->data['group_id']=$groupResult->id;
			 		}
			 	}


			 	if(!empty($this->request->data('bets_type_name')))
		 		{
		 			$betsTypesTable = TableRegistry::get('BetsTypes');
		 			$betsTypesQuery = $betsTypesTable->find('all')
		 			->where(['user_id' => $this->Auth->user('id')])
		 			->orWhere(['user_id' => 0])
		 			->where(['name' => strtolower(trim($this->request->data('bets_type_name') ))]);

		 			if($betsQuerySelected = $betsTypesQuery->first())
		 			{
		 				$this->request->data['bets_type_id']=$betsQuerySelected->id;
		 			}
		 			else
		 			{
		 				$betsTypes = $betsTypesTable->newEntity();
		 				$betsTypesData = array('name'=>strtolower(trim($this->request->data('bets_type_name'))));
						$betsTypes->user_id=$this->Auth->user('id');
						$betsTypes = $betsTypesTable->patchEntity($betsTypes, $betsTypesData);
		 				$betsTypesResult = $betsTypesTable->save($betsTypes);
		 				$this->request->data['bets_type_id']=$betsTypesResult->id;
		 			}
		 		}

			 	if(!empty($this->request->data('date_of_event')))
			 	{
			 		$this->request->data['date_of_event'] = date('Y-m-d H:i:s',strtotime($this->request->data('date_of_event')));
			 	}

			 	if($id===null)
			 	{
			 		$bet = $betsTable->newEntity();
			 	}
				$bet->user_id = $this->Auth->user('id');
			 	$bet = $betsTable->patchEntity($bet, $this->request->data
				, [
						'associated' => [
								'Tags' => [
											'accessibleFields' => ['user_id' => true]
										]
								]
						]

				);
			 	if ($this->Bets->save($bet)) {
			 		$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_SUCCESS,'message'=>__('Bet has been added.')]);
			 	} else {
			 			$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED, 'errors'=>$bet->errors()]);
			 	}
			 }
		}

		if($id!==null)
		{
			$betsQuery = $this->Bets->find('all')
			->where(['Bets.id' => $id ])
			->where(['Bets.user_id' => $this->Auth->user('id') ])
			->contain(['Sports','Groups','BetsTypes']);
			if(!($bet = $betsQuery->first()))
			{
				throw new MethodNotAllowedException(__('Bet not found'));
			}
			if($bet->has('sport'))
			{
				$this->request->data['sport_name'] = $bet->sport->name;
			}
			if($bet->has('group'))
			{
				$this->request->data['group_name'] = $bet->group->name;
			}
			if($bet->has('bets_type'))
			{
				$this->request->data['bets_type_name'] = $bet->bets_type->name;
			}
		}
		$this->set(compact('betsTable','bet'));

		$resultsSports = $sportsTable->find('all', [
				'order' => ['Sports.name' => 'asc'],
				'fields' =>[
						'Sports.id','Sports.name'
				],
				'conditions' => [
						'OR'=>
						[
								['user_id' => $this->Auth->user('id')],
								['user_id' => 0]
						]
				],
		]);

		$jsonDataSports = array();
		foreach ($resultsSports as $s)
		{
			$jsonDataSports[]=array('id'=>$s->id,'name'=>$s->name);
		}
		$this->set('availableSports',json_encode($jsonDataSports));

		$betsTypes = TableRegistry::get('BetsTypes');
		$resultsBetsTypes = $betsTypes->find('all', [
				'order' => ['BetsTypes.name' => 'asc'],
				'conditions' => [
						'OR'=>
						[
								['user_id' => $this->Auth->user('id')],
								['user_id' => 0]
						]
				],
				'fields'=>['BetsTypes.id','BetsTypes.name']
		]);

		$jsonDataBetsTypes = array();

		foreach ($resultsBetsTypes as $s)
		{
			$jsonDataBetsTypes[]=array('id'=>$s->id,'name'=>$s->name);
		}

		$this->set('availableBetsTypes',json_encode($jsonDataBetsTypes));
		$this->set('tags',$tagsArray);
		$this->set('ticket',$ticket);

	}

	public function index()
	{
		$tagsTable = TableRegistry::get('Tags');
		$tagsQuery =   $tagsTable->find('list');
		$tagsQuery->where(['Tags.user_id'=>$this->Auth->user('id')]);
		$tagsArray = $tagsQuery->toArray();
		$this->set('tags',$tagsArray);

		$betsTable = $this->Bets;
		$this->set('betsTable',$betsTable);

		$sportsTable = TableRegistry::get('Sports');
		$sportOptions = $sportsTable->find('list', [
				'order' => ['Sports.name' => 'asc'],

				'conditions' => [
						'OR'=>
						[
								['user_id' => $this->Auth->user('id')],
								['user_id' => 0]
						]
				],
		])->toArray();
		$this->set('sportOptions',$sportOptions);

		$groupsTable = TableRegistry::get('Groups');
		$groupOptions = $groupsTable->find('list', [
				'order' => ['Groups.name' => 'asc'],

				'conditions' => [
						'OR'=>
						[
								['user_id' => $this->Auth->user('id')],
								['user_id' => 0]
						]
				],
		])->toArray();
		$this->set('groupOptions',$groupOptions);

		$betsTypesTable = TableRegistry::get('BetsTypes');
		$betsTypesTableOptions = $betsTypesTable->find('list', [
				'order' => ['BetsTypes.name' => 'asc'],

				'conditions' => [
						'OR'=>
						[
								['user_id' => $this->Auth->user('id')],
								['user_id' => 0]
						]
				],
		])->toArray();
		$this->set('betsTypesTableOptions',$betsTypesTableOptions);

	}

	public function lists()
	{
		$order='Bets.created desc';
		$fields=['Bets.name','Bets.created','Bets.home_or_away','Bets.win','Bets.date_of_event','Bets.selected_pick'];

		if(!empty($this->request->data['order'][0]['dir'])):
			 $order='Bets.created '.$this->request->data['order'][0]['dir'];
		endif;



		if(!empty($this->request->data['order'][1]['dir'])):
			 $order='Bets.win'.$this->request->data['order'][1]['dir'];
		endif;

		if(!empty($this->request->data['order'][2]['dir'])):
			 $order='Bets.selected_pick_odds'.$this->request->data['order'][2]['dir'];
		endif;

		if(!empty($this->request->data['order'][3]['dir'])):
			 $order='Bets.selected_pick'.$this->request->data['order'][3]['dir'];
		endif;

		if(!empty($this->request->data['order'][4]['dir'])):
			 $order='Bets.home_or_away'.$this->request->data['order'][4]['dir'];
		endif;

		if(!empty($this->request->data['order'][4]['dir'])):
			 $order='Bets.name'.$this->request->data['order'][4]['dir'];
		endif;

		if(!empty($this->request->data['order'][4]['dir'])):
			 $order='Bets.sport_id'.$this->request->data['order'][4]['dir'];
		endif;

		$conditions[] = array('Bets.user_id'=>$this->Auth->user('id'));
		$joins = array();
		if(!empty($this->request->data['search']['value'])):
			parse_str($this->request->data['search']['value'], $searchArray);

			if(!empty($searchArray['name']))
			{
					$conditions[] = array('Bets.name Like'=>'%'.$searchArray['name'].'%');
			}

			if(!empty($searchArray['selected_pick']))
			{
					$conditions[] = array('Bets.selected_pick'=>$searchArray['selected_pick']);
			}

			if(!empty($searchArray['bets_type_id']))
			{
					$conditions[] = array('Bets.bets_type_id'=>$searchArray['bets_type_id']);
			}

			if(!empty($searchArray['result']))
			{
					$conditions[] = array('Bets.bets_type_id'=>$searchArray['result']);
			}

			if(!empty($searchArray['win']))
			{
					$conditions[] = array('Bets.win'=>$searchArray['win']);
			}

			if(!empty($searchArray['winner_pick']))
			{
					$conditions[] = array('Bets.winner_pick'=>$searchArray['winner_pick']);
			}

			if(!empty($searchArray['sport_id']))
			{
					$conditions[] = array('Bets.sport_id'=>$searchArray['sport_id']);
			}

			if(!empty($searchArray['group_id']))
			{
					$conditions[] = array('Bets.group_id'=>$searchArray['group_id']);
			}

			if(!empty($searchArray['home_or_away']))
			{
					$conditions[] = array('Bets.home_or_away'=>$searchArray['home_or_away']);
			}

			if(!empty($searchArray['selected_pick_odds']))
			{
					if(!empty($searchArray['selected_pick_odds_filter'])||@$searchArray['selected_pick_odds_filter']==0)
					{
						if($searchArray['selected_pick_odds_filter']==0)
						{
								$conditions[] = array('Bets.selected_pick_odds'=>$searchArray['selected_pick_odds']);
						}
						if($searchArray['selected_pick_odds_filter']==1)
						{
								$conditions[] = array('Bets.selected_pick_odds >'=>$searchArray['selected_pick_odds']);
						}
						if($searchArray['selected_pick_odds_filter']==2)
						{
								$conditions[] = array('Bets.selected_pick_odds <'=>$searchArray['selected_pick_odds']);
						}
					}
			}



			if(!empty($searchArray['created_from']))
			{
					$conditions[] = array('Bets.created >='=>date('Y-m-d H:i:s',strtotime($searchArray['created_from'])));
			}

			if(!empty($searchArray['created_to']))
			{
					$conditions[] = array('Bets.created <='=>date('Y-m-d H:i:s',strtotime($searchArray['created_to'])));
			}

			if(!empty($searchArray['date_of_event']))
			{
					$conditions[] = array('Bets.date_of_event >='=>date('Y-m-d H:i:s',strtotime($searchArray['date_of_ticket_from'])));
			}

			if(!empty($searchArray['date_of_event']))
			{
					$conditions[] = array('Bets.date_of_event <='=>date('Y-m-d H:i:s',strtotime($searchArray['date_of_event'])));
			}

			if(!empty($searchArray['tags'])&&is_array($searchArray['tags']))
			{
					$conditions[] = array('BetsTags.tag_id IN'=>$searchArray['tags']);
					$joins[]=[
							'alias'=>'BetsTags',
							'table' => 'tickets_tags',
							'type' => 'LEFT',
							'conditions' => 'BetsTags.ticket_id = Bets.id',
					];
			}

		endif;

		$this->request->query['page']=(int) (((int)$this->request->data('start')+(int)$this->request->data('length'))/20);
		$this->paginate = [
				'join'=>$joins,
				'maxLimit' => 20,
				'contain'=>['Sports' ,'Tags','Groups','BetsTypes'],
				'order' => [
						$order
				],
				'conditions'=>$conditions
		];

		$bets = $this->paginate($this->Bets);
		$betPrepared = [];

		foreach($bets as $b)
		{
			$arrayToUser=[];
			if($b->date_of_event)
			{
				$arrayToUser[]=date('d-m-Y',strtotime($b->date_of_event));
			}
			else
			{
				$arrayToUser[]="---";
			}

			if($b->win)
			{
				$arrayToUser[]=_("Win");
			}
			else
			{
				$arrayToUser[]=__("Lost");
			}

			if(isset($b->sport))
			{
					$arrayToUser[]=$b->sport->name;
			}
			else {
					$arrayToUser[]="---";
			}

			if(isset($b->group))
			{
					$arrayToUser[]=$b->group->name;
			}
			else {
					$arrayToUser[]="---";
			}

			if(isset($b->bets_type))
			{
					$arrayToUser[]=$b->bets_type->name;
			}
			else {
					$arrayToUser[]="---";
			}

			if(isset($b->name))
			{
					$arrayToUser[]=$b->name;
			}
			else {
					$arrayToUser[]="---";
			}

			if($b->home_or_away==1)
			{
				$arrayToUser[]=__("Home");
			}
			else if($b->home_or_away==2)
			{
				$arrayToUser[]=__("Away");
			}
			else if($b->home_or_away==3)
			{
				$arrayToUser[]=__("Neutral");
			}
			else
			{
				$arrayToUser[]="---";
			}

			if($b->selected_pick)
			{
				$arrayToUser[]=$b->selected_pick;
			}
			else
			{
				$arrayToUser[]='---';
			}

			if($b->selected_pick_odds)
			{
				$arrayToUser[]=$b->selected_pick_odds;
			}
			else
			{
				$arrayToUser[]='---';
			}

			if($b->winner_pick)
			{
				$arrayToUser[]=$b->winner_pick;
			}
			else
			{
				$arrayToUser[]='---';
			}


			$tagsString='';
			if($b->tags)
			{
				$divader="";
				foreach($b->tags as $tag)
				{
					$tagsString.=$divader.$tag->name ;
					$divader.=", ";
				}
			}	;
			$arrayToUser[]=$tagsString;
			$betPrepared[]=$arrayToUser;
		}
		echo json_encode(['recordsTotal'=> $this->request->params['paging']['Bets']['count']  ,'recordsFiltered'=>$this->request->params['paging']['Bets']['count'],'data'=>$betPrepared]);
		exit;
	}



    public function delete($id = null)
    {
        $bet = $this->Bets->get($id);

        if($bet&&$bet->user_id==$this->Auth->user('id'))
        {
        	if ($this->Bets->delete($bet)) {
        		$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_SUCCESS,'message'=>__('Sport has been deleted.')]);
        	}
	    }
	    $this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED]);
    }
}
