 <div class="row">
 	<div class="col-xs-12">
 		<div class="portlet light "> 
 			<h3><?= __('Edit Group') ?></h3> 
		    <?= $this->Form->create($group,array('url'=>['action'=>'edit'])) ?>
			 <div class="row">
			    <div class="col-xs-5"  > 
				       <?php echo $this->Form->input('name',array('class'=>'form-control','placeholder'=>__('Name of Group')))?> 
			    </div>
		      	
		      	<div class="col-xs-5"  >
		      		   <?php echo $this->Form->input('sport_id',array('empty'=>__('(Select Sport)'),'options'=>$sportOptions,'class'=>'form-control'))?> 
		       	</div>
		      	
		      	<div class="col-xs-2"  >
		      		<span class="input-group-btn"> 
		            	   <?= $this->Form->submit(__('Submit'),array('id'=>'editRowEntityButtonAction','type'=>'button','style'=>'margin-top: 24px;','class'=>'btn green')) ?>
		       		 </span>
		       	</div>
	       	</div>
	    	<?= $this->Form->end() ?>
	    </div>
	</div> 
 </div>  