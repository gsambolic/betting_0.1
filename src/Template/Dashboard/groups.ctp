
<script type="text/javascript">

$(function() {
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
      var data = google.visualization.arrayToDataTable([
         ['Sport', 'Win', 'Lost' ],
         <?php foreach($totalGroups as $gS):?>
         ['<?php echo $gS->group->name;?>', <?php echo $gS->win_count;?>, <?php echo $gS->count - $gS->win_count;?>],
        <?php endforeach;?>
        <?php if($groupNullCount):?>
          ['---', <?php echo $groupNullCount->win_count;?>, <?php echo $groupNullCount->count - $groupNullCount->win_count;?>],
        <?php endif;?>
       ]);
         var options = {
            width: '100%',
            legend: { position: 'top', maxLines: 3 },
            bar: { groupWidth: '75%',groupHeight:'100%' },
            isStacked: 'percent',
            chartArea:{top:20,width:"70%",height:"100%"}
         };
         var chart = new google.visualization.BarChart(document.getElementById("sportPlayedChart"));
         chart.draw(data, options);

    }
});
</script>
<?php echo $this->element('breadcrumps',['data'=>[ ['name'=>__('Dashboard')],['name'=>'Overview'] ]])?>


<div class="panel panel-default">
     <div class="panel-heading">
           <h3 class="panel-title">
           <?php echo __('Groups')?>
           </h3>
     </div>
     <div class="panel-body">
       <div class="col-lg-6">
           <div class="table-scrollable table-scrollable-borderless">
                   <table class="table table-hover table-light">
                       <thead>
                           <tr class="uppercase">
                             <th ><?php echo __('Group');?> </th>
                             <th ><?php echo __('Sport');?> </th>
                             <th ><?php echo __('Played');?></th>
                             <th ><?php echo __('Win');?> </th>
                             <th><?php echo __('Lost');?> </th>
                             <th><?php echo __('Percentage');?> </th>
                           </tr>
                       </thead>
                  <tbody>
                          <?php foreach($totalGroups as $gS):?>
                             <tr>
                               <td ><?php echo $gS->group->name;?></td>
                              <td ><?php echo $gS->group->sport->name;?></td>
                               <td><?php echo $gS->count;?></td>
                               <td><?php echo $gS->win_count;?></td>
                               <td><?php echo $gS->count - $gS->win_count;?></td>
                               <td>
                                <?php if($gS->win_count==0):?>
                                    0
                                <?php endif;?>
                                <?php if($gS->win_count!=0):?>
                                    <?php echo number_format(($gS->win_count/$gS->count)*100,2);?>%</td>
                                <?php endif; ?>
                             </tr>
                        <?php endforeach;?>

                        <?php if($groupNullCount):?>
                          <tr>
                            <td >---</td>
                            <td><?php echo $groupNullCount->count;?></td>
                            <td><?php echo $groupNullCount->win_count;?></td>
                            <td><?php echo $groupNullCount->count - $groupNullCount->win_count;?></td>
                            <td>
                             <?php if($groupNullCount->win_count==0):?>
                                 0
                             <?php endif;?>
                             <?php if($groupNullCount->win_count!=0):?>
                                 <?php echo number_format(($groupNullCount->win_count/$groupNullCount->count)*100,2);?>%</td>
                             <?php endif; ?>
                          </tr>
                       <?php endif;?>
                   </tbody>

                 </table>
               </div>
       </div>
       <div class="col-lg-6">
          <div style="height:<?php echo $totalGroups->count()*34+100;?>px" id="sportPlayedChart"></div>
       </div>
</div>
<?= $this->Html->script('charts/google/loader.js',['block' => 'scriptBottom'])?>
