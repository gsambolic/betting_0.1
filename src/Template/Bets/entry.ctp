<script>
var AVAILABLE_SPORTS_NEW_ENTRY = <?php echo $availableSports;?>;
var AVAILABLE_BETS_TYPES_NEW_ENTRY = <?php echo $availableBetsTypes;?>;
</script>

 <?php
 $ticketButtonLable = __('Submit');
 $showBackbutton=false;
if(isset($ticket->id))
{
	$ticketButtonLable =__('Update');
	$showBackbutton=true;
	$backbuttonUrl = $this->Url->build(['controller'=>'tickets','action'=>'index'],true);
?>
<script>
<?php if(!isset($noShowTicket)):?>
jQuery(document).ready(function() {
	entryDataTable.init()
})
<?php endif;?>
<?php if(isset($noShowTicket)):?>
jQuery(document).ready(function() {
	$(document).on('click',".editEntryAction",
			  function()
			  {
				ajaxForm.send($(this).closest('form'),
					function(parsedData,form)
					{
						window.location='<?php echo $this->Url->build(['controller'=>'bets','action'=>'entry',$bet->ticket_id],true);?>'
						$("#backbuttonAction").click();
					}
				);
			  }
		   )
});
<?php endif;?>
</script>
<?php
}
$mode =  __('Add');

if(isset($noShowTicket)):
	$mode = __('Edit');
	$showBackbutton=true;
	$backbuttonUrl = $this->Url->build(['controller'=>'bets','action'=>'entry',$bet->ticket_id],true);
endif;

if(empty($backbuttonUrl))
{
$backbuttonUrl='';
}
?>

 <?php echo $this->element('breadcrumps',['backbutton'=>['url'=>$backbuttonUrl,'show'=>$showBackbutton,'label'=>__('Back To Tickets')],'data'=>[ ['name'=>__('Ticket')],['name'=>$mode] ]])?>
  <?php if(!isset($noShowTicket)):?>
  <?php echo $this->Form->create($ticket,array('id'=>'newTicketForm','method'=>'post' ,'class'=>'iFormTopLeft', 'url' => array('controller' => 'tickets', 'action' => 'add')
  ))?>

 <div class="row">
 	<div class="col-md-12">
		<div class="portletAddEntry portlet light">
			<div class="portlet-body form">

              <h3><?php echo __('Add Ticket'); ?></h3>
				<div class="row">

            												 	 <?php    echo $this->Form->input('id',['type' => 'hidden','id'=>'ticketId']);?>
	            												 <div class="col-md-4 col-lg-1">
                                          <i data-container="body" data-trigger="hover" data-placement="top" data-content="<?php echo __('Time when event started.')?>" data-original-title="<?php echo __('Time when event started.')?>"  class="fa fa-info-circle popovers" aria-hidden="true"></i>
                                                                 	 <?php    echo $this->Form->input('amount',['class' => 'form-control','label'=>__('Amount')]);?>
                                          </div>


                                          <div class="form-group col-md-4 col-lg-1" style="padding-left: 30px;">
                                            <i style="display:block;" data-container="body" data-trigger="hover" data-placement="top" data-content="<?php echo __('Time when event started.')?>" data-original-title="<?php echo __('Time when event started.')?>"  class="fa fa-info-circle popovers" aria-hidden="true"></i>

                                            <?php echo $this->Form->input('win',['class' => 'form-control', 'type'=>'checkbox','templates'=>$this->Layout->checkboxTemplate()]); ?>
                                          </div>
                                                   <div class="form-group col-md-4 col-lg-1">
                                                     <i data-container="body" data-trigger="hover" data-placement="top" data-content="<?php echo __('Time when event started.')?>" data-original-title="<?php echo __('Time when event started.')?>"  class="fa fa-info-circle popovers" aria-hidden="true"></i>

                                                     <?php echo $this->Form->input('system', ['class' => 'form-control','label'=>__('System')]) ?>
                                                  </div>

                                                                 <div class="form-group col-md-4 col-lg-1">
                                                                   <i data-container="body" data-trigger="hover" data-placement="top" data-content="<?php echo __('Time when event started.')?>" data-original-title="<?php echo __('Time when event started.')?>"  class="fa fa-info-circle popovers" aria-hidden="true"></i>

                                                                   <?php echo $this->Form->input('name', ['class' => 'form-control','label'=>__('Name')]) ?>
                                                                </div>

                                                               <div class="form-group col-lg-2  " >
                                                                 <i data-container="body" data-trigger="hover" data-placement="top" data-content="<?php echo __('Time when event started.')?>" data-original-title="<?php echo __('Time when event started.')?>"  class="fa fa-info-circle popovers" aria-hidden="true"></i>

                                              <?php echo $this->Form->input('date_of_ticket', ['format' => 'Y-m-d 00:00', 'default' => date('Y-m-d 00:00'), 'value' => !empty($ticket->date_of_ticket) ? $ticket->date_of_ticket->format('Y-m-d H:i') : date('Y-m-d H:i'),'default'=>date('Y-m-d 00:00'),'default'=>date('Y-m-d 00:00'),'type'=>'text','class' => 'form-control','data-format'=>'YYYY-MM-DD HH:mm',   'data-type'=>'combodate','label'=>['text'=>__('Date'),'style'=>'display:block;']]) ?>

                                                                </div>

                                                                  <div class="form-group col-xs-6">
                                                                  <div class="row">
                                                                  	<div class="col-md-5">
                                                                      <i data-container="body" data-trigger="hover" data-placement="top" data-content="<?php echo __('Time when event started.')?>" data-original-title="<?php echo __('Time when event started.')?>"  class="fa fa-info-circle popovers" aria-hidden="true"></i>

						                                                <?php echo $this->Form->input('tags_text',['label'=>__('Select or Enter Tags'),'type'=>'textarea','rows'=>1]); ?>
						                                             </div>
						                                             <div class="col-md-7  mt-checkbox-inlineBlock tagsTicketCont" >
						                                                  <?php echo $this->Form->input('tags[_ids]',['templates'=>$this->Layout->checkboxTemplate(),'val'=>$tagsSelectedValues,'options'=>$tags,'multiple' => 'checkbox' ,'label'=>false]); ?>
					                                               		</div>
					                                               </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-actions" style="padding:0px;padding-top:5px">
                                                                <div class="btn-set pull-left">
                                                                    <?php echo $this->Form->button($ticketButtonLable,['type'=>'button','id'=>'addTicketAction','class'=>'btn green']);?>
                                                                </div>

                                                            </div>
                                                    </div>
                                                </div>
                                            </div>


 <?php echo $this->Form->end()?>
 <?php endif;?>
	<?php echo $this->Form->create($bet,array('id'=>'newEntryForm','method'=>'post' ,'class'=>'iFormTopLeft', ))?>
	<?php echo $this->Form->input('ticket_id',['type'=>'hidden','id'=>'betTicketId','value'=>$ticket->id]); ?>

	<?php $buttonAddEditBets = 'newEntryAction';?>
	<?php if(isset($noShowTicket)):?>
	<?php $buttonAddEditBets = 'editEntryAction';?>
	<?php endif;?>

   <div class="col-md-12">
	<div id="addNewEntryCont"  >
    <div class="portlet light portletAddEntry">
      <div class="portlet-body form">
                                        <h3><?php if(!isset($noShowTicket)): echo __('Add Bet'); else: echo __('Edit Bet');endif; ?></h3>


                                <div class="portlet-body portlet-empty" style="display: block;overflow: hidden">

                                                 <div class="form-group col-md-4 col-lg-1"style="padding-left: 30px;">
                                                	<i style="display:block" data-container="body" data-trigger="hover" data-placement="top" data-content="<?php echo __('Check for win.')?>" data-original-title="<?php echo __('Win or lost on event.')?>"  class="fa fa-info-circle popovers" aria-hidden="true"></i>
                                                	<?php echo $this->Form->input('win',['class' => 'form-control', 'type'=>'checkbox','templates'=>$this->Layout->checkboxTemplate()]); ?>
                                                </div>

                                                 <div class="form-group col-md-4 col-lg-2">
                                                  <i data-container="body" data-trigger="hover" data-placement="top" data-content="<?php echo __('Its used as top category of your bet, usually its sport but can be others. ')?>" data-original-title="<?php echo __('Soccer, Basketball, etc.')?>"  class="fa fa-info-circle popovers" aria-hidden="true"></i>
                                                  <?php echo $this->Form->input('sport_name',['autocomplete'=>'off','class' => 'form-control','data-provide'=>'typeahead','id'=>'betsNewEntrySports','label'=> __('Sport')]); ?>
                                                </div>


                                                 <div class="form-group col-md-4 col-lg-2">
                                                    <i data-container="body" data-trigger="hover" data-placement="top" data-content="<?php echo __('Premiership, Seria A, NBA etc.')?>" data-original-title="<?php echo __('Premiership, Seria A, NBA etc.')?>"  class="fa fa-info-circle popovers" aria-hidden="true"></i>
                                                    <?php echo $this->Form->input('group_name',['autocomplete'=>'off','class' => 'form-control','data-provide'=>'typeahead','id'=>'betsNewEntryGroup','label'=> __('Group') ]); ?>
                                                </div>
                                                 <div class="form-group col-md-4 col-lg-2">
                                                    <div class="betsTimeSelect" >
                                                     <i data-container="body" data-trigger="hover" data-placement="top" data-content="<?php echo __('Time when event started.')?>" data-original-title="<?php echo __('Date')?>"  class="fa fa-info-circle popovers" aria-hidden="true"></i>
                                                     <?php echo $this->Form->input('date_of_event',['format' => 'Y-m-d 00:00', 'default' => date('Y-m-d 00:00'), 'value' => !empty($bet->date_of_event) ? $bet->date_of_event->format('Y-m-d 00:00') : date('Y-m-d 00:00'),'default'=>date('Y-m-d 00:00'),'class' => 'form-control','type'=>'text','data-format'=>'YYYY-MM-DD HH:mm' ,'data-type'=>'combodate' ]); ?>
                                                    </div>
                                                </div>

                                                 <div class="form-group col-md-4 col-lg-2">
                                                      <i data-container="body" data-trigger="hover" data-placement="top" data-content="<?php echo __('Full time, Half Time, Number of goals, Correct score etc.')?>" data-original-title="<?php echo __('Full time, Half Time, Number of goals, Correct score etc.')?>"  class="fa fa-info-circle popovers" aria-hidden="true"></i>
                                                      <?php echo $this->Form->input('bets_type_name',['autocomplete'=>'off','class' => 'form-control','data-provide'=>'typeahead','id'=>'betsNewEntryBetsTypes' ]); ?>
                                                </div>

                                                 <div class="form-group col-md-4 col-lg-2">
                                                  <i data-container="body" data-trigger="hover" data-placement="top" data-content="<?php echo __('Manchester vs Everton. Premiership winner, etc.')?>" data-original-title="<?php echo __('Manchester vs Everton. Premiership winner, etc.')?>"  class="fa fa-info-circle popovers" aria-hidden="true"></i>
                                                  <?php echo $this->Form->input('name',['class' => 'form-control','class' => 'form-control' ]); ?>
                                                </div>

                                                <div class="form-group col-md-4 col-lg-1">
                                                     <i data-container="body" data-trigger="hover" data-placement="top" data-content="<?php echo __('Selected pick was on home, away or neutral Location.')?>" data-original-title="<?php echo __('Hom or Away (selection).')?>"  class="fa fa-info-circle popovers" aria-hidden="true"></i>
                                                     <?php echo $this->Form->input('home_or_away',['label'=>__('Home'),'class' => 'form-control','empty'=>'---','type'=>'select','options'=>$betsTable->getLocation()]); ?>
                                                </div>

                                                 <div class="form-group col-md-4 col-lg-1">
                                              	      <i data-container="body" data-trigger="hover" data-placement="top" data-content="<?php echo __('Your pick for bet. For full time somewhere is "1x2", by others are team names and "draw". Enter as you like.')?>" data-original-title="<?php echo __('Your pick for bet. For full time somewhere is "1x2", by others are team names and "draw". Enter as you like.')?>"  class="fa fa-info-circle popovers" aria-hidden="true"></i>
                                                      <?php echo $this->Form->input('selected_pick',['class' => 'form-control','label'=>__('Pick')]); ?>
                                                </div>


                                                <div class="form-group col-md-4 col-lg-1">
                                                     <i data-container="body" data-trigger="hover" data-placement="top" data-content="<?php echo __('Odds for your selected pick on bet ticket.')?>" data-original-title="<?php echo __('Odds for your selected pick on bet ticket.')?>"  class="fa fa-info-circle popovers" aria-hidden="true"></i>
                                                     <?php echo $this->Form->input('selected_pick_odds',['class' => 'form-control','label'=>__('Odds')]); ?>
                                                 </div>



                                                 <div class="form-group col-md-4 col-lg-1">
                                                      <i data-container="body" data-trigger="hover" data-placement="top" data-content="<?php echo __('Pick for winner, if bet is winner it will be same as selected pick.')?>" data-original-title="<?php echo __('Pick for winner, if bet is winner it will be same as selected pick.')?>"  class="fa fa-info-circle popovers" aria-hidden="true"></i>
                                                      <?php echo $this->Form->input('winner_pick',['class' => 'form-control']); ?>
                                                </div>






                                                <div class="form-group col-md-4 col-lg-1">
                                                       <i data-container="body" data-trigger="hover" data-placement="top" data-content="<?php echo __('Odds for your winner pick on bet ticket.')?>" data-original-title="<?php echo __('Odds for your winner pick on bet ticket.')?>"  class="fa fa-info-circle popovers" aria-hidden="true"></i>
                                                       <?php echo $this->Form->input('result',['class' => 'form-control']); ?>
                                                 </div>




                                                  <div class="form-group col-md-6 col-lg-6 mt-checkbox-inlineBlock">  <?php echo __('Select or Enter Tags')?>
                                                      <div class="row">
                                                           <div class="form-group col-md-5">
                                                             <?php echo $this->Form->input('tags_text',['class' => 'form-control', 'type'=>'textarea','rows'=>1,'label'=>false]); ?><br />
                                                           </div>
                                                            <div class="form-group col-md-5">
                                                              <?php
                                                              $optionsBetsTags=[];
                                                                foreach($tags as $keyT=> $t)
                                                                {
                                                                  $optionsBetsTags[]=['text' => $t, 'value' => $keyT,'id'=>'bets_tags_'.$keyT];
                                                                }
                                                               ?>
                                                              <?php echo $this->Form->input('tags[_ids]',['val'=>$tagsBetsSelectedValues,'id' => 'best-tags','templates'=>$this->Layout->checkboxTemplate(),'class' => 'form-control','options'=>$optionsBetsTags,'multiple' => 'checkbox','label'=>false]); ?>
                                                            </div>
                                                      </div>
                                                  </div>

                                                 <div style="padding:0px;padding-top:5px" class="form-actions form-group col-md-12 entryFormSubmitButtons">
                                                  	<?php if(!isset($noShowTicket)):?>
                                                   <?php echo $this->Form->input('Submit & Reset',array('data-reset'=>'true','label'=>false,'type'=>'button',  'style'=>'font-weight:bold;display:inline-block; margin-right: 50px;','class'=>$buttonAddEditBets.' btn green-haze'))?>
                                                 <?php endif;?>
                                                  <?php echo $this->Form->input('Submit',array('label'=>false,'type'=>'button',  'style'=>'font-weight:bold;display:inline-block;','class'=>$buttonAddEditBets.' btn green-haze'))?>
                                                 </div>
                                    </div>
                                </div>

 <?php echo $this->Form->end()?>
</div>
</div>

   <?php if(!isset($noShowTicket)):?>
                            <div class="portlet light portlet-fit portlet-datatable ">
                                 <div class="portlet-body">
                                    <div class="table-container">

                                        <table  data-edit-redirect="true"  class="table table-striped table-bordered table-hover table-checkable" data-delete="<?php echo  $this->Url->build(array('controller'=>'bets','action'=>'delete')).DS;?>"  data-edit="<?php echo  $this->Url->build(array('controller'=>'bets','action'=>'editView')).DS;?>" data-url="<?php echo  $this->Url->build(array('controller'=>'bets','action'=>'listsTicketBets')).DS;?><?php if(isset($ticket->id)){echo $ticket->id;}?>" id="entityDatatable">
                                            <thead>
                                                <tr role="row" class="heading">
                                                  <th style="width:55px"> <?php echo __('Date')?> </th>
                                                  <th> <?php echo __('Win')?> </th>
                                                  <th> <?php echo __('Sport')?> </th>
                                                  <th> <?php echo __('Group')?> </th>
                                                  <th> <?php echo __('Bets Type')?> </th>
                                                  <th> <?php echo __('Name')?> </th>
                                                  <th> <?php echo __('Location')?> </th>
                                                  <th> <?php echo __('Pick')?> </th>
                                                  <th> <?php echo __('Odds')?> </th>
                                                  <th> <?php echo __('Winner')?> </th>
                                                  <th> <?php echo __('Tags')?> </th>
                                                  <th style="width:120px"> <?php echo __('Edit')?> </th>
                                                </tr>
                                            </thead>
                                            <tbody> </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                             <?php endif;?>
<?= $this->Html->script('/assets/pages/scripts/components-date-time-pickers.min.js',['block' => true])?>
<?= $this->Html->css('/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',['block' => true])?>
<?= $this->Html->script('/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js', ['block' => 'scriptBottom'])?>
<?= $this->Html->script('/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js', ['block' => 'scriptBottom'])?>
<?= $this->Html->script('/assets/global/plugins/moment.min.js', ['block' => 'scriptBottom'])?>
<?= $this->Html->script('/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js', ['block' => 'scriptBottom'])?>
<?= $this->Html->script('/assets/global/plugins/bootstrap-editable/inputs-ext/address/address.js', ['block' => 'scriptBottom'])?>
<?= $this->Html->script('/assets/global/plugins/bootstrap-typeahead/bootstrap3-typeahead.min.js', ['block' => 'scriptBottom'])?>
<?= $this->Html->script('/assets/global/plugins/select2/js/select2.full.min.js', ['block' => 'scriptBottom'])?>
<?= $this->Html->script('/assets/global/plugins/jquery-repeater/jquery.repeater.js?'.time(), ['block' => 'scriptBottom'])?>


<?= $this->Html->css('/assets/global/plugins/datatables/datatables.min.css',['block' => true])?>
<?= $this->Html->css('/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',['block' => true])?>
<?= $this->Html->script('/assets/global/scripts/datatable.js',  ['block' => 'scriptBottom'])?>
 <?= $this->Html->script('/assets/global/plugins/datatables/datatables.min.js',  ['block' => 'scriptBottom'])?>
  <?= $this->Html->script('/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js',  ['block' => 'scriptBottom'])?>



<?= $this->Html->css('/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',['block' => true])?>
<?= $this->Html->css('/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',['block' => true])?>



<!-- BEGIN PAGE LEVEL SCRIPTS -->
<?= $this->Html->script('/kladstat/scripts/add-bet.js?a='.time(), ['block' => 'scriptBottom']) ?>
<!-- END PAGE LEVEL SCRIPTS -->
