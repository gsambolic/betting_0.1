<?php echo $this->element('breadcrumps',['data'=>[ ['name'=>__('Bets')],['name'=>'List'] ]])?>
<h1 class="page-title"><?php echo __('Bets Stats')?></h1>


                           <div class="portlet light portlet-fit portlet-datatable ">
                                <div class="portlet-body">
                                  <?= $this->Form->create(null,['id'=>'searchEntityForm','class'=>'iFormTopLeft']) ?>


                                     <div class="row">
                                          <div class="col-md-12  mt-checkbox-inlineBlock "><?php echo __('Grouped by')?>
                                            <?php echo $this->Form->input('grouped_by',['required'=>true,'label'=>false, 'options'=>array('sport'=>__('Sport'),'group'=>__('Group'),'home_or_away'=>__('Home Or Away'),'bets_type'=>__('Bets Type')),'templates'=>$this->Layout->checkboxTemplate(),'class' => 'form-control','type' => 'radio']); ?>
                                          </div>
                                     </div


                                   <div class="row">


                                              <div class="col-md-4">
                                                 <?php echo $this->Form->input('name',array( 'class'=>'form-control','placeholder'=>__('Name')))?>
                                             </div>



                                             <div class="col-md-4">
                                                <?php echo $this->Form->input('selected_pick',array( 'class'=>'form-control','placeholder'=>__('System')))?>
                                            </div>

                                            <div class="col-md-4 selectBoxWithFilter">
                                               <?php echo $this->Form->input('selected_pick_odds',array('type'=>'number', 'class'=>'form-control','placeholder'=>__('Amount')))?>
                                               <?php echo $this->element('form_filter_by_size',['value'=>'0','name'=>'selected_pick_odds_filter','options'=>['class'=>'form-control']]);?>
                                           </div>

                                           <div class="col-md-4">
                                             <?php echo $this->Form->input('bets_type_id',['class'=>'form-control','empty'=>'---','type'=>'select','options'=>$betsTypesTableOptions]); ?>
                                          </div>

                                         



                                          <div class="col-md-4">
                                             <?php echo $this->Form->input('winner_pick',array( 'class'=>'form-control','placeholder'=>__('System')))?>
                                         </div>




                                        <div class="col-md-4">
                                              <?php echo $this->Form->input('sport_id',['class' => 'form-control', 'type'=>'select','empty'=>'---','options'=>$sportOptions]); ?>
                                       </div>

                                       <div class="col-md-4">
                                          <?php echo $this->Form->input('group_id',array( 'class'=>'form-control','empty'=>'---','options'=>$groupOptions))?>
                                      </div>

                                      <div class="col-md-4">
                                        <?php echo $this->Form->input('home_or_away',['class' => 'form-control','empty'=>'---','type'=>'select', 'options'=>$betsTable->getLocation()]); ?>
                                     </div>


                                           <div class="col-md-4">
                                             <label><?php echo __('Created');?></label>
                                             <div class="input-group input-daterange">
                                               <input data-date-format="dd-mm-yyyy" type="text" name="created_from" class="form-control" >
                                               <div class="input-group-addon"><?php echo __('to')?></div>
                                               <input data-date-format="dd-mm-yyyy" type="text" name="created_to" class="form-control" >
                                             </div>
                                           </div>

                                           <div class="col-md-4">
                                             <label><?php echo __('Time');?></label>
                                             <div class="input-group input-daterange">
                                               <input date-format="dd-mm-yyyy" type="text" name="date_of_event"  class="form-control" >
                                               <div class="input-group-addon"><?php echo __('to')?></div>
                                               <input data-date-format="dd-mm-yyyy" type="text"  name="date_of_event_to" class="form-control" >
                                             </div>
                                           </div>


                                          <div class="col-md-4 mt-checkbox-inlineBlock tagsTicketCont">

                                            <?php echo $this->Form->input('tags',['templates'=>$this->Layout->checkboxTemplate(),'class' => 'form-control','options'=>$tags,'multiple' => 'checkbox']); ?>

                                          </div>

                                          <div class="col-md-12" style="text-align:right;">
                                            <button type="submit" id="searchEntityAction" class="btn green"><?php echo __('Search')?></button>
                                            <button type="reset" id="searchEntityActionReset" class="btn green"><?php echo __('Reset')?></button>
                                          </div>



                                        </div>
          <?= $this->Form->end() ?>

     </div>


<?php if(!empty($topDashboardChart)):?>
<script type="text/javascript">

$(function() {
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
      var data = google.visualization.arrayToDataTable([
         <?php echo $topDashboardChart;?>,
         <?php foreach($betsResultParsedChart as $bS):?>
            [

                <?php echo implode("," ,$bS);?>

            ],
        <?php endforeach;?>

       ]);
         var options = {
            width: '100%',
            legend: { position: 'top', maxLines: 3 },
            bar: { groupWidth: '75%',groupHeight:'100%' },
            isStacked: 'percent',
            chartArea:{top:20,width:"70%",height:"100%"}
         };
         var chart = new google.visualization.BarChart(document.getElementById("dashboardPlayedChart"));
         chart.draw(data, options);

    }
});
</script>
<?php endif;?>
<?php if(!empty($betsResultParsed)):?>
<div class="panel panel-default" style="margin-bottom:20px;">

     <div class="panel-body">
       <div class="col-lg-6">
           <div class="table-scrollable table-scrollable-borderless">
                   <table class="table table-hover table-light">
                       <thead>
                           <tr class="uppercase">
                             <?php foreach ($betsResultTableTitle as $value):?>
                                   <th ><?php echo $value;?> </th>
                            <?php endforeach;?>

                           </tr>
                       </thead>
                       <tbody>
                          <?php foreach($betsResultParsed as $bS):?>
                             <tr>
                                 <?php foreach($bS as $bSInner):?>
                               <td><?php echo $bSInner;?></td>
                                 <?php endforeach;?>
                             </tr>
                        <?php endforeach;?>

                   </tbody></table>
               </div>
       </div>
       <div class="col-lg-6">
          <div style="height:<?php echo $dashboardHeight;?>px" id="dashboardPlayedChart"></div>
       </div>
  </div>
</div>
<?php endif;?>
</div>

</div>

</div>




  <script>
  $( document ).ready(function() {
     entryDataTable.init()
   $('.input-daterange').datepicker({});
  });
  </script>



         <?= $this->Html->css('/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css',['block' => true])?>
         <?= $this->Html->css('/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',['block' => true])?>
         <?= $this->Html->css('/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',['block' => true])?>
         <?= $this->Html->css('/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',['block' => true])?>
         <?= $this->Html->css('/assets/global/plugins/clockface/css/clockface.css',['block' => true])?>

<?= $this->Html->css('/assets/global/plugins/datatables/datatables.min.css',['block' => true])?>
<?= $this->Html->css('/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',['block' => true])?>


<?= $this->Html->script('/assets/global/scripts/datatable.js',  ['block' => 'scriptBottom'])?>
<?= $this->Html->script('/assets/global/plugins/datatables/datatables.min.js',  ['block' => 'scriptBottom'])?>
 <?= $this->Html->script('/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js',  ['block' => 'scriptBottom'])?>

 <?= $this->Html->script('/assets/global/plugins/moment.min.js',  ['block' => 'scriptBottom'])?>
 <?= $this->Html->script('/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js',['block' => 'scriptBottom'])?>
 <?= $this->Html->script('/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',['block' => 'scriptBottom'])?>
 <?= $this->Html->script('/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js',['block' => 'scriptBottom'])?>
 <?= $this->Html->script('/assets/global/plugins/clockface/js/clockface.js',['block' => 'scriptBottom'])?>
 <?= $this->Html->script('/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',['block' => 'scriptBottom'])?>
 <?= $this->Html->script('/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',['block' => 'scriptBottom'])?>
<?= $this->Html->script('https://www.gstatic.com/charts/loader.js',['block' => 'scriptBottom'])?>
