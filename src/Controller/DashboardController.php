<?php
namespace App\Controller;
use Cake\ORM\TableRegistry;

use App\Controller\AppController;
use Cake\Controller\Component\AuthComponent;
use App\Model\Application\Ajax\ResponsCodes;


class DashboardController extends AppController
{

  public function calendar()
  {
    $start = $this->request->query('start');
    $end =  $this->request->query('end');

    $ticketsTable = TableRegistry::get('Tickets');
		$ticketsQuery =   $ticketsTable->find('all');
		$ticketsQuery->where(['Tickets.user_id'=>$this->Auth->user('id')]);
    if($start)
    {
        $ticketsQuery->where(['Tickets.date_of_ticket >='=>$start." 00:00:00"]);
    }

    if($end)
    {
        $ticketsQuery->where(['Tickets.date_of_ticket <='=>$end." 00:00:00"]);
    }


		$ticketsArray = $ticketsQuery->toArray();
    $ticketsJson = [];
    foreach ($ticketsArray as $key => $bA)
    {
      $ticketsJsonTemp['title']='';
      if(!empty($bA->name))
      {
        $ticketsJsonTemp['title']=$bA->name;
      }

      if(!empty($bA->amount))
      {
        $ticketsJsonTemp['title'].=" Stake: ".$bA->amount;
      }

      if(!empty($bA->winning))
      {
        $ticketsJsonTemp['title'].=" Win: ".$bA->winning;
      }

      if($bA->date_of_event)
      {
          $ticketsJsonTemp['start']=$bA->date_of_event;
      }
      else
      {
          $ticketsJsonTemp['start']=$bA->created;
      }
      if($bA->win)
      {
          $ticketsJsonTemp['backgroundColor']='blue';
      }
      else
      {
        $ticketsJsonTemp['backgroundColor']='red';
      }
      $ticketsJson[]=$ticketsJsonTemp;
    }

      echo json_encode($ticketsJson);
      exit;
  }

	public function index()
	{
  }

  public function overview()
	{
    	$ticketTable = TableRegistry::get('Tickets');
      $betsTable = TableRegistry::get('Bets');

      $totalBets = $betsTable->getTotalCount($this->Auth->user('id'));
      $winBets = $betsTable->getWinCount($this->Auth->user('id'));
      $lostBets = $betsTable->getLostCount($this->Auth->user('id'));

      $this->set('totalBets',$totalBets->count);
      $this->set('winBets',$winBets->count);
      $this->set('lostBets',$lostBets->count);

      if((int)$winBets->count==0)
      {
        $this->set('winRatioBets',0) ;
      }
      else if((int)$lostBets->count==0)
      {
          $this->set('winRatioBets',100) ;
      }
      else {
        $winRatio = (($winBets->count/$totalBets->count)*100);
        $this->set('winRatioBets',number_format($winRatio,2));
      }

     $amountSum =   $ticketTable->getAmountSum($this->Auth->user('id'));
     $winningSum =   $ticketTable->getWinningSum($this->Auth->user('id'));
     $winningSumValue = $winningSum->sum;
     $amountSumValue = $amountSum->sum;
     if(!$winningSumValue)
     {
       $winningSumValue=0;
     }

     if(!$amountSumValue)
     {
       $amountSumValue=0;
     }

     $this->set('totalSumPlayed',$amountSumValue) ;

     $totalCount = $ticketTable->getTotalCount($this->Auth->user('id'));
     $this->set('total',$totalCount->count) ;

     $winCount = $ticketTable->getWinCount($this->Auth->user('id'));
     $this->set('win',(int)$winCount->count) ;

     $lostCount = $ticketTable->getLostCount($this->Auth->user('id'));
     $this->set('lost',(int)$lostCount->count) ;

     $amountOnWinSum = $ticketTable->getAmountOnWinSum($this->Auth->user('id'));
     $this->set('amountOnWinSum',(float)$amountOnWinSum->sum) ;

     $amountOnLostSum = $ticketTable->getAmountOnLostSum($this->Auth->user('id'));
     $this->set('amountOnLostSum',(float)$amountOnLostSum->sum) ;


     if((float)$amountOnWinSum->sum==0)
     {
       $this->set('winRatioMoney',0) ;
     }
     else if($amountSumValue==0)
     {
         $this->set('winRatioMoney',0) ;
     }
     else {
       $winRatioMoney = (($amountOnWinSum->sum/$amountSumValue)*100);
       $this->set('winRatioMoney',number_format($winRatioMoney,2));
     }


     if((int)$winCount->count==0)
     {
       $this->set('winRatio',0) ;
     }
     else if((int)$lostCount->count==0)
     {
         $this->set('winRatio',100) ;
     }
     else {
       $winRatio = (($winCount->count/$totalCount->count)*100);
       $this->set('winRatio',number_format($winRatio,2));
     }


     $this->set('winningSumValue',$winningSumValue);
     $this->set('amountSumValue',$amountSumValue);
     $this->set('totalSumValue',$winningSumValue-$amountSumValue);



     $totalGroups = $betsTable->getGroupCount($this->Auth->user('id'));
     $groupNullCount = $betsTable->getGroupNullCount($this->Auth->user('id'))->first();
     $this->set('groupNullCount',$groupNullCount);
     $this->set('totalGroups',$totalGroups);
  }

  public function sports()
	{
    $betsTable = TableRegistry::get('Bets');
    $totalSports = $betsTable->getSportCount($this->Auth->user('id'));
    $sportNullCount = $betsTable->getSportNullCount($this->Auth->user('id'))->first();
    $this->set('sportNullCount',$sportNullCount);
    $this->set('totalSports',$totalSports);

    $totalGroups = $betsTable->getGroupCount($this->Auth->user('id'));
    $groupNullCount = $betsTable->getGroupNullCount($this->Auth->user('id'))->first();
    $this->set('groupNullCount',$groupNullCount);
    $this->set('totalGroups',$totalGroups);

    $betsTable = TableRegistry::get('Bets');
    $totalGroups = $betsTable->getGroupCount($this->Auth->user('id'));
    $groupNullCount = $betsTable->getGroupNullCount($this->Auth->user('id'))->first();
    $this->set('groupNullCount',$groupNullCount);
    $this->set('totalGroups',$totalGroups);

  }

  public function generator()
  {
  		$tagsTable = TableRegistry::get('Tags');
      $betsTable = TableRegistry::get('Bets');
  		$tagsQuery =   $tagsTable->find('list');
  		$tagsQuery->where(['Tags.user_id'=>$this->Auth->user('id')]);
  		$tagsArray = $tagsQuery->toArray();
  		$this->set('tags',$tagsArray);

  		$this->set('betsTable',$betsTable);

  		$sportsTable = TableRegistry::get('Sports');
  		$sportOptions = $sportsTable->find('list', [
  				'order' => ['Sports.name' => 'asc'],

  				'conditions' => [
  						'OR'=>
  						[
  								['user_id' => $this->Auth->user('id')],
  								['user_id' => 0]
  						]
  				],
  		])->toArray();
  		$this->set('sportOptions',$sportOptions);

  		$groupsTable = TableRegistry::get('Groups');
  		$groupOptions = $groupsTable->find('list', [
  				'order' => ['Groups.name' => 'asc'],

  				'conditions' => [
  						'OR'=>
  						[
  								['user_id' => $this->Auth->user('id')],
  								['user_id' => 0]
  						]
  				],
  		])->toArray();
  		$this->set('groupOptions',$groupOptions);

  		$betsTypesTable = TableRegistry::get('BetsTypes');
  		$betsTypesTableOptions = $betsTypesTable->find('list', [
  				'order' => ['BetsTypes.name' => 'asc'],

  				'conditions' => [
  						'OR'=>
  						[
  								['user_id' => $this->Auth->user('id')],
  								['user_id' => 0]
  						]
  				],
  		])->toArray();
  		$this->set('betsTypesTableOptions',$betsTypesTableOptions);

     if ($this->request->is('post')&&!empty($this->request->data['grouped_by']))
     {
        $betsResultParsedChart=array();
        $betsResultParsed = array();
       		$order='Bets.created desc';
       		$fields=['Bets.name','Bets.created','Bets.home_or_away','Bets.win','Bets.date_of_event','Bets.selected_pick'];


       		$conditions[] = array('Bets.user_id'=>$this->Auth->user('id'));
       		$joins = array();

       			if(!empty($this->request->data['name']))
       			{
       					$conditions[] = array('Bets.name Like'=>'%'.$this->request->data['name'].'%');
       			}

       			if(!empty($this->request->data['selected_pick']))
       			{
       					$conditions[] = array('Bets.selected_pick'=>$this->request->data['selected_pick']);
       			}

       			if(!empty($this->request->data['bets_type_id']))
       			{
       					$conditions[] = array('Bets.bets_type_id'=>$this->request->data['bets_type_id']);
       			}

       			if(!empty($this->request->data['result']))
       			{
       					$conditions[] = array('Bets.bets_type_id'=>$this->request->data['result']);
       			}

       			if(!empty($this->request->data['win']))
       			{
       					$conditions[] = array('Bets.win'=>$this->request->data['win']);
       			}

       			if(!empty($this->request->data['winner_pick']))
       			{
       					$conditions[] = array('Bets.winner_pick'=>$this->request->data['winner_pick']);
       			}

       			if(!empty($this->request->data['sport_id']))
       			{
       					$conditions[] = array('Bets.sport_id'=>$this->request->data['sport_id']);
       			}

       			if(!empty($this->request->data['group_id']))
       			{
       					$conditions[] = array('Bets.group_id'=>$this->request->data['group_id']);
       			}

       			if(!empty($this->request->data['home_or_away']))
       			{
       					$conditions[] = array('Bets.home_or_away'=>$this->request->data['home_or_away']);
       			}

       			if(!empty($this->request->data['selected_pick_odds']))
       			{
       					if(!empty($this->request->data['selected_pick_odds_filter'])||@$this->request->data['selected_pick_odds_filter']==0)
       					{
       						if($this->request->data['selected_pick_odds_filter']==0)
       						{
       								$conditions[] = array('Bets.selected_pick_odds'=>$this->request->data['selected_pick_odds']);
       						}
       						if($this->request->data['selected_pick_odds_filter']==1)
       						{
       								$conditions[] = array('Bets.selected_pick_odds >'=>$this->request->data['selected_pick_odds']);
       						}
       						if($this->request->data['selected_pick_odds_filter']==2)
       						{
       								$conditions[] = array('Bets.selected_pick_odds <'=>$this->request->data['selected_pick_odds']);
       						}
       					}
       			}



       			if(!empty($this->request->data['created_from']))
       			{
       					$conditions[] = array('Bets.created >='=>date('Y-m-d H:i:s',strtotime($this->request->data['created_from'])));
       			}

       			if(!empty($this->request->data['created_to']))
       			{
       					$conditions[] = array('Bets.created <='=>date('Y-m-d H:i:s',strtotime($this->request->data['created_to'])));
       			}

       			if(!empty($this->request->data['date_of_event']))
       			{
       					$conditions[] = array('Bets.date_of_event >='=>date('Y-m-d H:i:s',strtotime($this->request->data['date_of_ticket_from'])));
       			}

       			if(!empty($this->request->data['date_of_event']))
       			{
       					$conditions[] = array('Bets.date_of_event <='=>date('Y-m-d H:i:s',strtotime($this->request->data['date_of_event'])));
       			}

       			if(!empty($this->request->data['tags'])&&is_array($this->request->data['tags']))
       			{
       					$conditions[] = array('BetsTags.tag_id IN'=>$this->request->data['tags']);
       					$joins[]=[
       							'alias'=>'BetsTags',
       							'table' => 'tickets_tags',
       							'type' => 'LEFT',
       							'conditions' => 'BetsTags.ticket_id = Bets.id',
       					];
       			}

            $betsQuery= $betsTable->find('all');

            if($this->request->data['grouped_by']=='sport')
            {
              $fields = [
                      'Sports.name',
                      'Bets.sport_id',
                      'count' => $betsQuery->func()->count('sport_id'),
                      'win_count' => $betsQuery->func()->sum('Bets.win')
                    ];
              $group='Bets.sport_id';

              $topDashboardChart="['Sport', 'Win', 'Lost' ]";
              $contains = ['Sports'];

              $betsResultTableTitle[]='Sport';
              $betsResultTableTitle[]='Played';
              $betsResultTableTitle[]='Win';
              $betsResultTableTitle[]='Lost';
              $betsResultTableTitle[]='Percentage';
            }

            if($this->request->data['grouped_by']=='group')
            {
              $fields = [
                      'Groups.name',
                      'Bets.group_id',
                      'count' => $betsQuery->func()->count('group_id'),
                      'win_count' => $betsQuery->func()->sum('Bets.win')
                    ];
              $group='Bets.group_id';

              $topDashboardChart="['Group', 'Win', 'Lost' ]";
              $contains = ['Groups'];

              $betsResultTableTitle[]='Group';
              $betsResultTableTitle[]='Played';
              $betsResultTableTitle[]='Win';
              $betsResultTableTitle[]='Lost';
              $betsResultTableTitle[]='Percentage';
            }

            if($this->request->data['grouped_by']=='bets_type')
            {
              $fields = [
                      'BetsTypes.name',
                      'Bets.bets_type_id',
                      'count' => $betsQuery->func()->count('bets_type_id'),
                      'win_count' => $betsQuery->func()->sum('Bets.win')
                    ];
              $group='Bets.bets_type_id';

              $topDashboardChart="['Bets Type', 'Win', 'Lost' ]";
              $contains = ['BetsTypes'];

              $betsResultTableTitle[]='BetsTypes';
              $betsResultTableTitle[]='Played';
              $betsResultTableTitle[]='Win';
              $betsResultTableTitle[]='Lost';
              $betsResultTableTitle[]='Percentage';
            }

            if($this->request->data['grouped_by']=='home_or_away')
            {
              $fields = [
                      'Bets.home_or_away',
                      'count' => $betsQuery->func()->count('home_or_away'),
                      'win_count' => $betsQuery->func()->sum('Bets.win')
                    ];
              $group='Bets.home_or_away';

              $topDashboardChart="['Home or Away', 'Win', 'Lost' ]";
              $contains = [];

              $betsResultTableTitle[]='Home or Away';
              $betsResultTableTitle[]='Played';
              $betsResultTableTitle[]='Win';
              $betsResultTableTitle[]='Lost';
              $betsResultTableTitle[]='Percentage';
            }
           

            $betsResult =  $betsQuery->select(
                $fields
              )
                ->contain($contains)
                 ->where($conditions)
                 ->group($group)
                 ->order(['count' => 'DESC']);


           foreach ($betsResult as $key => $bets)
           {
             if($this->request->data['grouped_by']=='sport')
             {
               if(!empty($bets->sport))
               {
                 $betsResultParsedChart[$key][]='"'.$bets->sport->name.'"';
                 $betsResultParsedChart[$key][]=$bets->win_count;
                 $betsResultParsedChart[$key][]=$bets->count-$bets->win_count;
                 $betsResultParsed[$key][]=$bets->sport->name;
                 $betsResultParsed[$key][]=$bets->count;
                 $betsResultParsed[$key][]=$bets->win_count;
                 $betsResultParsed[$key][]=$bets->count-$bets->win_count;

                 if($bets->win_count==0||$bets->count==0)
                 {
                   $betsResultParsed[$key][]=0;
                 }
                 else {
                   $betsResultParsed[$key][]=number_format((($bets->win_count/$bets->count)*100),2);
                 }
              }
             }

             if($this->request->data['grouped_by']=='group')
             {
               if(!empty($bets->group))
               {
                 $betsResultParsedChart[$key][]='"'.$bets->group->name.'"';
                 $betsResultParsedChart[$key][]=$bets->win_count;
                 $betsResultParsedChart[$key][]=$bets->count-$bets->win_count;

                 $betsResultParsed[$key][]=$bets->group->name;
                 $betsResultParsed[$key][]=$bets->count;
                 $betsResultParsed[$key][]=$bets->win_count;
                 $betsResultParsed[$key][]=$bets->count-$bets->win_count;
                 if($bets->win_count==0||$bets->count==0)
                 {
                   $betsResultParsed[$key][]=0;
                 }
                 else {
                   $betsResultParsed[$key][]=number_format((($bets->win_count/$bets->count)*100),2);
                 }
               }
             }

             if($this->request->data['grouped_by']=='bets_type')
             {
               if(!empty($bets->bets_type))
               {
                 $betsResultParsedChart[$key][]='"'.$bets->bets_type->name.'"';
                 $betsResultParsedChart[$key][]=$bets->win_count;
                 $betsResultParsedChart[$key][]=$bets->count-$bets->win_count;

                 $betsResultParsed[$key][]=$bets->bets_type->name;
                 $betsResultParsed[$key][]=$bets->count;
                 $betsResultParsed[$key][]=$bets->win_count;
                 $betsResultParsed[$key][]=$bets->count-$bets->win_count;
                 if($bets->win_count==0||$bets->count==0)
                 {
                   $betsResultParsed[$key][]=0;
                 }
                 else {
                   $betsResultParsed[$key][]=number_format((($bets->win_count/$bets->count)*100),2);
                 }
               }
             }

             if($this->request->data['grouped_by']=='home_or_away')
             {
               if($bets->home_or_away==1)
               {
                 $key=0;
                 $betsResultParsedChart[$key][]='"Home"';
                 $betsResultParsed[$key][]='Home';
               }
               else if($bets->home_or_away==2) {
                 $key=1;
                 $betsResultParsedChart[$key][]='"Away"';
                 $betsResultParsed[$key][]='Away';
               }
               else if($bets->home_or_away==3) {
                 $key=2;
                 $betsResultParsedChart[$key][]='"Neutral"';
                 $betsResultParsed[$key][]='Neutral';
               }
               else {
                 continue;
               }

               $betsResultParsedChart[$key][]=$bets->win_count;
               $betsResultParsedChart[$key][]=$bets->count-$bets->win_count;

               $betsResultParsed[$key][]=$bets->count;
               $betsResultParsed[$key][]=$bets->win_count;
               $betsResultParsed[$key][]=$bets->count-$bets->win_count;
             }
           }


            $this->set('betsResultParsedChart',$betsResultParsedChart) ;
            $this->set('betsResultTableTitle',$betsResultTableTitle) ;
            $this->set('betsResultParsed',$betsResultParsed) ;

           $dashboardHeight =  sizeof($betsResultParsed)*34+20;
           $this->set('dashboardHeight',$dashboardHeight);
           $this->set('topDashboardChart',$topDashboardChart)  ;

     }
     else {
        $this->set('betsResultParsed',array()) ;
     }
  }

  public function betsTypes()
  {
    $betsTable = TableRegistry::get('Bets');
    $totalGroups = $betsTable->getGroupCount($this->Auth->user('id'));
    $groupNullCount = $betsTable->getGroupNullCount($this->Auth->user('id'))->first();
    $this->set('groupNullCount',$groupNullCount);
    $this->set('totalGroups',$totalGroups);
  }

}
