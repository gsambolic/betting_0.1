
<div class="inbox-header inbox-view-header">
    <h1 class="pull-left"><?php echo $message->subject?> 
    </h1> 
</div>
<div class="inbox-view-info">
    <div class="row">
        <div class="col-md-12"> 
            <span class="sbold"><?php echo $message->sender->username?> </span> 
            <?php echo __('on')?> <?php echo date('H:i, d M Y.',strtotime($message->created))?> </div>
         
    </div>
</div>
<div class="inbox-view">
  	<?php echo $message->content?>
</div> 
 