<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Bet Entity
 *
 * @property int $id
 * @property string $name
 * @property int $group_id
 * @property \Cake\I18n\Time $date
 * @property \Cake\I18n\Time $created
 * @property int $bets_type_id
 * @property string $winner_pick
 * @property string $selected_pick
 * @property int $user_id
 * @property int $sport_id 
 * @property float $selected_pick_odds
 * @property bool $win
 *
 * @property \App\Model\Entity\Group $group
 * @property \App\Model\Entity\EventsBet[] $events_bets
 * @property \App\Model\Entity\TeamsHasBet[] $teams_has_bets
 */
class Bet extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    	'user_id' => false
    ];
}
