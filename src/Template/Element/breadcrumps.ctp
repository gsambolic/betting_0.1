<?php 
$breadCrumpSize = sizeof($data);
?>
<div class="page-bar">
 	<ul class="page-breadcrumb">
	<?php foreach ($data as $key=>$d):?>  
		<li>	
			<?php if($key==0):?>
			<i class="icon-home"></i>
			<?php endif;?>
			<a ><?php echo $d['name']?></a>
			<?php if($breadCrumpSize>($key+1)):?>
			 <i class="fa fa-angle-right"></i>
			 <?php endif;?>
		</li>
	<?php endforeach;?>
	</ul> 
	
	<?php if(isset($backbutton)):?>
		<?php echo $this->element('backbutton',$backbutton)?> 
	<?php endif;?>
	
</div>