<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;

/**
 * User Entity
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property \Cake\I18n\Time $last_login
 * @property \Cake\I18n\Time $created
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property int $users_role_id
 * @property string $email_validation
 * @property int $active
 * @property int $email_validated
 *
 * @property \App\Model\Entity\UsersRole $users_role
 * @property \App\Model\Entity\BetsSlip[] $bets_slips
 * @property \App\Model\Entity\Transaction[] $transactions
 * @property \App\Model\Entity\UserMoneyOverall[] $user_money_overalls
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => false,
        'id' => false
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];
    
    protected function _setPassword($password)
    {
    	return (new DefaultPasswordHasher)->hash($password);
    } 
}
