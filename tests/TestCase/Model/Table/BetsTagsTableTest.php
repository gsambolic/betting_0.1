<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BetsTagsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BetsTagsTable Test Case
 */
class BetsTagsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BetsTagsTable
     */
    public $BetsTags;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.bets_tags',
        'app.bets',
        'app.groups',
        'app.sports',
        'app.users',
        'app.users_roles',
        'app.bets_slips',
        'app.transactions',
        'app.user_money_overalls',
        'app.bets_types',
        'app.tags'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('BetsTags') ? [] : ['className' => 'App\Model\Table\BetsTagsTable'];
        $this->BetsTags = TableRegistry::get('BetsTags', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BetsTags);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
