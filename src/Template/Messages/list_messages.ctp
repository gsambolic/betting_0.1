
<form id="formMessages" action="<?php echo $this->Url->build(["controller" => "Messages","action" => "change_status"]);?>">                                              
<table class="table table-striped table-advance table-hover">
    <thead>
        <tr>
            <th colspan="2">
                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                    <input type="checkbox" class="mail-group-checkbox" />
                    <span></span>
                </label>
                <div class="btn-group input-actions">
                    <a class="btn btn-sm blue btn-outline dropdown-toggle sbold" href="javascript:;" data-toggle="dropdown"> <?php echo __('Actions')?>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li> 
                            <a class="messageChangeStatusAction"  href="javascript:;">
                                <i class="fa fa-pencil"></i> <?php echo __('Mark as Read')?> </a>
                        </li> 
                        <li class="divider"> </li>
                        <li>
                            <a  data-action="delete"  class="messageChangeStatusAction"  href="javascript:;">
                                <i class="fa fa-trash-o"></i> <?php echo __('Delete')?> </a>
                        </li>
                    </ul>
                </div>
            </th>
            <th class="pagination-control" colspan="3">
                <span class="pagination-info"> <?php echo $this->Paginator->counter(['format' => 'range']);?> </span>
               	 
               <?php 
					$this->Paginator->templates([
					    'nextActive' => '<a class="paginatorAjaxCakePHP btn btn-sm blue btn-outline" href="{{url}}">{{text}}</a>'
					]);
					   
					$this->Paginator->templates([
							'nextDisabled' => '<a class=" btn btn-sm blue btn-outline" >{{text}}</a>'
					]);
					
					$this->Paginator->templates([
							'prevActive' => '<a class="paginatorAjaxCakePHP btn btn-sm blue btn-outline" href="{{url}}">{{text}}</a>'
					]);
						
					$this->Paginator->templates([
							'prevDisabled' => '<a class=" btn btn-sm blue btn-outline">{{text}}</a>'
					]);
					
					$this->Paginator->templates([
							'first' => '<a class="paginatorAjaxCakePHP btn btn-sm blue btn-outline" href="{{url}}">{{text}}</a>'
					]);
					
					$this->Paginator->templates([
							'last' => '<a class="paginatorAjaxCakePHP btn btn-sm blue btn-outline" href="{{url}}">{{text}}</a>'
					]); 
				?>       
				<?= $this->Paginator->first('<i class="fa fa-angle-left"></i><i class="fa fa-angle-left"></i>',array('escape'=>false)) ?>
				<?= $this->Paginator->prev('<i class="fa fa-angle-left"></i>',array('escape'=>false)) ?>
				<?= $this->Paginator->next('<i class="fa fa-angle-right"></i>',array('escape'=>false)) ?> 
				<?= $this->Paginator->last('<i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i>',array('escape'=>false)) ?>
            </th>
        </tr>
    </thead>
    <tbody>
    	<?php foreach ($messages as $m): ?>
    	<?php 
    	$messageSeen = "";
    	if(isset($m->sender)&&$m->receiver_seen==0)
    	{
    		$messageSeen = "bold black"; 
    	}
    	
    	
    	?>
        <tr class="unread" data-messageid="<?php echo $m->id?>">
            <td class="inbox-small-cells">
                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                    <input type="checkbox" class="mail-checkbox" name="messages[]" value="<?php echo $m->id?>" />
                    <span></span>
                </label>
            </td> 
            <?php if(isset($m->sender)):?>
            <td style="width:180px;" class="view-message <?php echo $messageSeen?>"> <?php echo $m->sender->username?> </td>
            <?php endif;?>
            
              <?php if(isset($m->receiver)):?>
            <td style="width:180px;" class="view-message <?php echo $messageSeen?>"> <?php echo $m->receiver->username?> </td>
            <?php endif;?>
             
            
            <td class="view-message <?php echo $messageSeen?>"> <?php echo $m->subject?> </td> 
            <td style="width:100px;" class="view-message <?php echo $messageSeen?>  text-right"> <?php echo date('H:i, d M Y.',strtotime($m->created))?> </td>
        </tr> 
         <?php endforeach;?>
          
    </tbody>
</table>
</form>                           