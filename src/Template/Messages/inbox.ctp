

   <!-- END THEME PANEL -->
                    <h1 class="page-title"> <?php echo __('Inbox')?>
                    </h1>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a ><?php echo __('Account')?></a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <span><?php echo __('Inbox')?></span>
                            </li>
                        </ul>
                    </div>
                    <!-- END PAGE HEADER-->
                    <div class="inbox">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="inbox-sidebar">
                                	<a href="javascript:;" data-title="Compose" class="btn red compose-btn btn-block">
                                        <i class="fa fa-edit"></i> <?php echo __('Compose')?> </a>
                                    <ul class="inbox-nav">
                                        <li >
                                            <a href="javascript:;" id="inboxListAction" data-type="inbox" data-title="Inbox"> <?php echo __('Inbox')?>
                                                <span class="badge badge-success"><?php echo @$new_message;?></span>
                                            </a>
                                        </li>
                                         <li >
                                            <a href="javascript:;" id="inboxSendAction" data-type="sent" data-title="Sent"> <?php echo __('Sent')?> </a>
                                            </a>
                                        </li>

                                    </ul>

                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="inbox-body">
                                    <div class="inbox-header">
                                        <h1 class="pull-left"><?php echo __('Inbox')?></h1>
                                    </div>
                                    <div class="inbox-content" style="position: relative; zoom: 1;">


                    </div>
                            </div>
                        </div>
                    </div>
    </div>
<?= $this->Html->css('/assets/apps/css/inbox.min.css',['block' => true])?>
<?= $this->Html->script('/kladstat/scripts/inbox.js?a='.time(),  ['block' => 'scriptBottom'])?>
