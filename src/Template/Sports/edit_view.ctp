 <div class="row">
 	<div class="col-xs-12">
 		<div class="portlet light ">
	        <h3><?= __('Edit Sport') ?></h3> 
		    <?= $this->Form->create($sport,array('url'=>['action'=>'edit'])) ?>
		     <div class="input-group addNewEntryTable">
		       <?php echo $this->Form->input('name',array('class'=>'form-control','placeholder'=>__('Name of Sport')))?>
	           	<span class="input-group-btn"> 
	            	   <?= $this->Form->submit(__('Submit'),array('id'=>'editRowEntityButtonAction','type'=>'button','style'=>'margin-top: 24px;','class'=>'btn green')) ?>
	       		 </span>
	      	</div>  
	    	<?= $this->Form->end() ?>
	    </div>
	</div>  
 </div>