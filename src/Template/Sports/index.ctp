
<?php echo $this->element('breadcrumps',['data'=>[ ['name'=>__('Sports')],['name'=>__('Add/List')] ]])?>
 <div class="row">
 	<div class="col-xs-12">
 		<div class="portlet light ">
 			<h4 ><?= __('Add Sport') ?></h4>
		    <?= $this->Form->create($sport,array( 'url'=>['action'=>'add'])) ?>
		     <div class="input-group addNewEntryTable">
		      <?php echo $this->Form->input('name',array('label'=>false,'required'=>true,'class'=>'form-control','placeholder'=>__('Name of Sport')))?>
	           	<span class="input-group-btn">
	            	   <?= $this->Form->button(__('Submit'),array('type' => 'button','id'=>'addEntityAction','class'=>'btn green')) ?>
	       		 </span>
	      	</div>
	    	<?= $this->Form->end() ?>
	    </div>
	</div>
 </div>

<h1 class="page-title"><?php echo __('Sports')?></h1>


                            <div class="portlet light portlet-fit portlet-datatable ">
                                 <div class="portlet-body">
                                    <div class="table-container">
                                       <div class="row">
                                      	  <?= $this->Form->create(null,['id'=>'searchEntityForm']) ?>
                                       		<div class="col-xs-6">
                                      	 <?php echo $this->Form->input('name',array('label'=>false,'required'=>true,'class'=>'form-control','placeholder'=>__('Name of Sport')))?>
                                        </div>

                                        	<div class="col-xs-6">
                                        		<button type="button" id="searchEntityAction" class="btn green"><?php echo __('Search')?></button>
                                        		<button type="button" id="searchEntityActionReset" class="btn green"><?php echo __('Reset')?></button>
                                      	  </div>
                                      	   <?= $this->Form->end() ?>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover table-checkable" data-delete="<?php echo  $this->Url->build(array('controller'=>'sports','action'=>'delete')).DS;?>"  data-edit="<?php echo  $this->Url->build(array('controller'=>'sports','action'=>'edit_view')).DS;?>" data-url="<?php echo  $this->Url->build(array('controller'=>'sports','action'=>'lists'));?>" id="entityDatatable">
                                            <thead>
                                                <tr role="row" class="heading">

                                                    <th width="5%"> <?php echo __('Name')?> </th>
                                                    <th width="15%"> <?php echo __('Edit')?> </th>
                                                </tr>
                                            </thead>
                                            <tbody> </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


   <script>
   $( document ).ready(function() {
	    entryDataTable.init()
   });
   </script>

<?= $this->Html->css('/assets/global/plugins/datatables/datatables.min.css',['block' => true])?>
<?= $this->Html->css('/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',['block' => true])?>
<?= $this->Html->script('/assets/global/scripts/datatable.js',  ['block' => 'scriptBottom'])?>
 <?= $this->Html->script('/assets/global/plugins/datatables/datatables.min.js',  ['block' => 'scriptBottom'])?>
  <?= $this->Html->script('/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js',  ['block' => 'scriptBottom'])?>
