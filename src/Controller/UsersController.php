<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use App\Model\Application\Ajax\ResponsCodes;
use Cake\Routing\Router;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

	public function beforeFilter(Event $event)
	{
		parent::beforeFilter($event);
		$this->Auth->allow(['register']);
	}

	public function login()
	{
		$user = $this->Users->newEntity();
		if ($this->request->is('post')) {
			$user = $this->Auth->identify();
			if ($user)
			{
				$this->Auth->setUser($user);
				$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_REDIRECT,'url'=>Router::url(array('controller'=>'dashboard','action'=>'index'),true)]);   
			}
			$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED,'errors'=>['email_login'=>['not_valid'=>__('Email or password not valid')]]] );
		}
		$this->set(compact('user'));
	}

	public function logout()
	{
		return $this->redirect($this->Auth->logout());
	}

    public function register()
    {
    	$user = $this->Users->newEntity();
    	if ($this->request->is('post')) {
    		$this->request->data['active']=1;
    		$this->request->data['users_role_id']=3;
    		$this->request->data['email_validated']=0;
    		$user = $this->Users->patchEntity($user, $this->request->data,[
    					'fieldList' => ['first_name', 'last_name','email','username','password','users_role_id','active','email_validated'],
					]);
    		if ($this->Users->save($user)) {
    			$user = $this->Auth->identify();
    			$this->Auth->setUser($user);
    			$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_REDIRECT,'url'=>Router::url(array('controller'=>'users','action'=>'my-account'),true)]);
    		} else {
    			$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED,'action'=>'0','errors'=>$user->errors()]);
    		}
    	}
    	$this->set(compact('user'));
    	$this->set('_serialize', ['user']);
    }

    public function myAccount()
    {
    	$user = $this->Users->get($this->Auth->user('id'), [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
        	if($this->request->data('password'))
        	{
        		if(!(new DefaultPasswordHasher)->check($this->request->data('currentpassword'),$user->password))
        		{
        			 $this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED, 'password'=>['email_login'=>['not_valid'=>__('Email or password not valid')]]]);
        		}
        	}

            $user = $this->Users->patchEntity($user, $this->request->data,
            	[
            		'fieldList' => ['first_name', 'last_name','email','newsletter','password'],
            	]
            );
            if ($this->Users->save($user)) {
               $this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_SUCCESS,'message'=>__('Updates has been saved.')]);
            } else {
              $this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED,'errors'=>$user->errors()]);
            }
        }

        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }


}
