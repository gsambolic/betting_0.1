<?php echo $this->Form->create($message,array('url'=>['action'=>'add'],'class'=>'nbox-compose form-horizontal'))?> 
    <div class="inbox-compose-btn" style="margin-bottom:10px;">
        <button id="composeInboxAction"  type="button" class="btn green">
            <i class="fa fa-check"></i><?php echo __('Send')?>
        </button>
        <button  data-type="inbox" data-title="Inbox" class="btn default inbox-discard-btn"><?php echo __('Discard')?></button> 
    </div>
    <?php /*
    <div class="inbox-form-group mail-to">
        <label class="control-label">To:</label>
        <div class="controls controls-to">
            <input type="text" class="form-control" name="to">
            <span class="inbox-cc-bcc">
                <span class="inbox-cc"> Cc </span>
                <span class="inbox-bcc"> Bcc </span>
            </span>
        </div>
    </div>
    
  
    <div class="inbox-form-group input-cc display-hide">
        <a href="javascript:;" class="close"> </a>
        <label class="control-label">Cc:</label>
        <div class="controls controls-cc">
            <input type="text" name="cc" class="form-control"> </div>
    </div>
    <div class="inbox-form-group input-bcc display-hide">
        <a href="javascript:;" class="close"> </a>
        <label class="control-label">Bcc:</label>
        <div class="controls controls-bcc">
            <input type="text" name="bcc" class="form-control"> </div>
    </div>
    */
    ?>
       <?php echo $this->Form->input('subject',array('div'=>false,'label'=>__('Subject'), "style"=>"border: 1px solid #c2cad8 !important;",'type'=>'text','class'=>"form-control"))?> 
 		<?php echo $this->Form->input('content',array('label'=>false,'type'=>'textarea','required'=>true,'rows'=>'12','class'=>"inbox-editor inbox-wysihtml5 form-control"))?> 
 
  
    <div class="inbox-compose-btn">
        <button class="btn green">
           <i class="fa fa-check"></i><?php echo __('Send')?></button>
        <button class="btn default inbox-discard-btn"><?php echo __('Discard')?></button> 
    </div>
<?php echo $this->Form->end()?>