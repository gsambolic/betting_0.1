<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Bets Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Groups
 * @property \Cake\ORM\Association\BelongsTo $BetsTypes
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Sports
 *
 * @method \App\Model\Entity\Bet get($primaryKey, $options = [])
 * @method \App\Model\Entity\Bet newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Bet[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Bet|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Bet patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Bet[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Bet findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class BetsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('bets');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Groups', [
            'foreignKey' => 'group_id',
        ]);
        $this->belongsTo('BetsTypes', [
            'foreignKey' => 'bets_type_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Sports', [
            'foreignKey' => 'sport_id'
        ]);

        $this->belongsTo('Tickets', [
        		'foreignKey' => 'ticket_id'
        ]);

        $this->belongsToMany('Tags', [
            'foreignKey' => 'bet_id',
            'targetForeignKey' => 'tag_id',
            'joinTable' => 'bets_tags'
        ]);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('name');

        $validator
            ->dateTime('date')
            ->allowEmpty('date');

        $validator
            ->allowEmpty('winner_pick');

        $validator
            ->allowEmpty('selected_pick');

        $validator
            ->numeric('selected_pick_odds')
            ->allowEmpty('selected_pick_odds');

        $validator
            ->boolean('win')
            ->allowEmpty('win');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['group_id'], 'Groups'));
        $rules->add($rules->existsIn(['bets_type_id'], 'BetsTypes'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['sport_id'], 'Sports'));
        $rules->add($rules->existsIn(['sport_id'], 'Sports'));
        $rules->add($rules->existsIn(['ticket_id'], 'Tickets'));

        return $rules;
    }

    public function getWinOrLost($byId=null,$default=null)
    {
    	$values =  array('0'=>__('Lost'),'1'=>__('Win'),);
    	if($byId!==null)
    	{
    		return $values[$byId];
    	}

    	if($default!==null)
    	{
    		return $default;
    	}

    	return $values;
    }

    public function getLocation($byId=null,$default=null)
    {
    	$values =  array('1'=>__('Home'),'2'=>__('Away'),'3'=>__('Neutral'));
    	if($byId!==null)
    	{
    		return $values[$byId];
    	}

    	if($default!==null)
    	{
    		return $default;
    	}

    	return $values;
    }


        public function getTotalCount($userId)
        {
          $ticketTableQuery = $this->find();
          return $ticketTableQuery->select(['count' => $ticketTableQuery->func()->count('Bets.user_id')])
          ->where(['Bets.user_id' => $userId ])
          ->first();
        }

        public function getWinCount($userId)
        {
          $ticketTableQuery = $this->find();
          return $ticketTableQuery->select(['count' => $ticketTableQuery->func()->count('Bets.id')])
          ->where(['Bets.win' =>1,'Bets.user_id' => $userId ])
          ->first();
        }

        public function getLostCount($userId)
        {
          $ticketTableQuery = $this->find();
          return $ticketTableQuery->select(['count' => $ticketTableQuery->func()->count('Bets.id')])
          ->where(['Bets.win' =>0,'Bets.user_id' => $userId ])
          ->first();
        }

        public function getSportCount($userId)
        {
            $query= $this->find('all');
            return $query->select([
                    'Sports.name',
                    'Bets.sport_id',
                    'count' => $query->func()->count('sport_id'),
                    'win_count' => $query->func()->sum('Bets.win')
                  ])
                  ->contain(['Sports'])
                 ->where(['Bets.user_id' => $userId,'Bets.sport_id is not null'])
                 ->group('Bets.sport_id')
                 ->order(['count' => 'DESC']);
        }

        public function getSportNullCount($userId)
        {
            $query= $this->find('all');
            return $query->select([
                    'Bets.sport_id',
                    'count' => $query->func()->count(1),
                    'win_count' => $query->func()->sum('Bets.win')
                  ])
                 ->where(['Bets.sport_id is null' ])
                 ->group('Bets.sport_id');
        }


        public function getGroupCount($userId)
        {
            $query= $this->find('all');
            return $query->select([
                    'Groups.name',
                    'Sports.name',
                    'Bets.group_id',
                    'count' => $query->func()->count('group_id'),
                    'win_count' => $query->func()->sum('Bets.win')
                  ])
                  ->contain(['Groups'=>['Sports']])
                 ->where(['Bets.user_id' => $userId,'Bets.group_id is not null'])
                 ->group('Bets.group_id')
                 ->order(['count' => 'DESC']);
        }

        public function getGroupNullCount($userId)
        {
            $query= $this->find('all');
            return $query->select([
                    'Bets.group_id',
                    'count' => $query->func()->count(1),
                    'win_count' => $query->func()->sum('Bets.win')
                  ])
                 ->where(['Bets.group_id is null' ])
                 ->group('Bets.group_id');
        }
}
