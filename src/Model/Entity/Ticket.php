<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Ticket Entity
 *
 * @property int $id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $date_of_ticket
 * @property string $system
 * @property float $amount
 * @property string $comments
 * @property int $user_id
 *
 * @property \App\Model\Entity\Bet[] $bets
 * @property \App\Model\Entity\Tag[] $tags
 * @property \App\Model\Entity\User $user
 */
class Ticket extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    	'user_id' => false
    ];
}
