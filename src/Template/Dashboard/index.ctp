 <?php echo $this->element('breadcrumps',['data'=>[ ['name'=>__('Dashboard')],['name'=>'Calendar'] ]])?>

<div class="row">
  <div class="col-xs-12">
    <div id="calendar" class="has-toolbar"> </div>
  </div>
</div>



  <?= $this->Html->css('/assets/global/plugins/fullcalendar/fullcalendar.min.css',['block' => true])?>
<?= $this->Html->script('/assets/global/plugins/moment.min.js',  ['block' => 'scriptBottom'])?>
<?= $this->Html->script('/assets/global/plugins/fullcalendar/fullcalendar.min.js',['block' => 'scriptBottom'])?>
<?= $this->Html->script('/kladstat/scripts/calendar.js',['block' => 'scriptBottom'])?>
