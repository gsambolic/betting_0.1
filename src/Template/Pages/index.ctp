
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN CONTENT HEADER -->
                    <div class="row margin-bottom-40 about-header">
                        <div class="col-md-12">
                            <h1><?php echo __('KLADSTAT')?></h1>
                            <h2><?php echo __('Ticket and Bets Stats')?></h2>
                            <button type="button" class="btn btn-danger"><?php echo __('JOIN US TODAY')?></button>
                        </div>
                    </div>
                    <!-- END CONTENT HEADER -->
                    <!-- BEGIN CARDS -->
                    <div class="row margin-bottom-20">
                        <div class="col-lg-3 col-md-6">
                            <div class="portlet light">
                                <div class="card-icon">
                                    <i class="icon-user-follow font-red-sunglo theme-font"></i>
                                </div>
                                <div class="card-title">
                                    <span> <?php echo __('Its free')?> </span>
                                </div>
                                <div class="card-desc">
                                    <span>
                                      <?php echo __('Unlimited Sports, Competitions, Bet Types')?>.<br />
                                     </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="portlet light">
                                <div class="card-icon">
                                    <i class="icon-trophy font-green-haze theme-font"></i>
                                </div>
                                <div class="card-title">
                                    <span> <?php echo __('Be A Winner')?> </span>
                                </div>
                                <div class="card-desc">
                                    <?php echo __('Improve your win rate and cut your losses')?>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="portlet light">
                                <div class="card-icon">
                                    <i class="icon-basket font-purple-wisteria theme-font"></i>
                                </div>
                                <div class="card-title">
                                    <span> <?php echo __('Managed Accounts')?> </span>
                                </div>
                                <div class="card-desc">
                                  <?php echo __('Dont have time? Send us your ticket and we will enter it for You')?>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="portlet light">
                                <div class="card-icon">
                                    <i class="icon-layers font-blue theme-font"></i>
                                </div>
                                <div class="card-title">
                                    <span> <?php echo __('Mobile and Desktop Ready')?> </span>
                                </div>
                                <div class="card-desc">
                                    <?php echo __('Optimized for desktop and mobile devices')?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END CARDS -->
                    <!-- BEGIN TEXT & VIDEO -->
                    <div class="row margin-bottom-40">
                        <div class="col-lg-6">
                            <div class="portlet light about-text">
                                <h4>
                                    <i class="fa fa-check icon-info"></i> About Kladstat.com</h4>
                                <p class="margin-top-20">
                                  Prilagodljiv software, unesite vlasite sportove, natjecanja, tipove oklada.
                                  Od kompleksne statistke do najosnovnije, unesite samo pobjednika ili unesite sve podatke vezane za okladu, izbor je na vama.
                                  Organizirajte vaše tickete and bets tagovima. Želite posebno grupirati vikend oklade od tjednih, odvojite ih tagovima.
                                  100% besplatan, pristup svim mogučnostima stoftware bez nadokande


                                 </p>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <ul class="list-unstyled margin-top-10 margin-bottom-10">
                                            <li>
                                                <i class="fa fa-check"></i> 100% Besplatan </li>
                                            <li>
                                                <i class="fa fa-check"></i> Jednostavna i kompleksna statiska </li>
                                            <li>
                                                <i class="fa fa-check"></i> Tagirate oklade i betove </li>
                                            <li>
                                                <i class="fa fa-check"></i> Mirum est notare quam </li>
                                            <li>
                                                <i class="fa fa-check"></i> Mirum est notare quam </li>
                                        </ul>
                                    </div>
                                    <div class="col-xs-6">
                                        <ul class="list-unstyled margin-top-10 margin-bottom-10">
                                            <li>
                                                <i class="fa fa-check"></i> Nam liber tempor cum soluta </li>
                                            <li>
                                                <i class="fa fa-check"></i> Mirum est notare quam </li>
                                            <li>
                                                <i class="fa fa-check"></i> Lorem ipsum dolor sit amet </li>
                                            <li>
                                                <i class="fa fa-check"></i> Mirum est notare quam </li>
                                            <li>
                                                <i class="fa fa-check"></i> Mirum est notare quam </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="about-image" style="background: url(../assets/pages/media/works/img4.jpg) center no-repeat;"></div>
                        </div>
                    </div>
                    <!-- END TEXT & VIDEO -->
                    <!-- BEGIN MEMBERS SUCCESS STORIES -->


                    <!-- END MEMBERS SUCCESS STORIES -->
                    <!-- BEGIN LINKS BLOCK -->
                    <div class="row about-links-cont" data-auto-height="true">

                          <div class="col-sm-6 about-links-item">
                                      <h4>eCommerce</h4>
                                      <ul>
                                          <li>
                                              <a href="#">Dashboard</a>
                                          </li>
                                          <li>
                                              <a href="#">Orders</a>
                                          </li>
                                          <li>
                                              <a href="#">Products</a>
                                          </li>
                                      </ul>
                            </div>
                    </div>

      </div>
