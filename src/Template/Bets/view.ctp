<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Bet'), ['action' => 'edit', $bet->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Bet'), ['action' => 'delete', $bet->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bet->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Bets'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Bet'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Groups'), ['controller' => 'Groups', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Group'), ['controller' => 'Groups', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Bets Types'), ['controller' => 'BetsTypes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Bets Type'), ['controller' => 'BetsTypes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sports'), ['controller' => 'Sports', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sport'), ['controller' => 'Sports', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="bets view large-9 medium-8 columns content">
    <h3><?= h($bet->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($bet->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Group') ?></th>
            <td><?= $bet->has('group') ? $this->Html->link($bet->group->name, ['controller' => 'Groups', 'action' => 'view', $bet->group->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Bets Type') ?></th>
            <td><?= $bet->has('bets_type') ? $this->Html->link($bet->bets_type->name, ['controller' => 'BetsTypes', 'action' => 'view', $bet->bets_type->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Winner Pick') ?></th>
            <td><?= h($bet->winner_pick) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Selected Pick') ?></th>
            <td><?= h($bet->selected_pick) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $bet->has('user') ? $this->Html->link($bet->user->id, ['controller' => 'Users', 'action' => 'view', $bet->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sport') ?></th>
            <td><?= $bet->has('sport') ? $this->Html->link($bet->sport->name, ['controller' => 'Sports', 'action' => 'view', $bet->sport->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($bet->id) ?></td>
        </tr>
         
        <tr>
            <th scope="row"><?= __('Selected Pick Odds') ?></th>
            <td><?= $this->Number->format($bet->selected_pick_odds) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date Of Event') ?></th>
            <td><?= h($bet->date_of_event) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($bet->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Win Or Lost') ?></th>
            <td><?= $bet->win ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>
