<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Application\Ajax\ResponsCodes;
use Cake\Controller\Component\AuthComponent;

/**
 * Messages Controller
 *
 * @property \App\Model\Table\MessagesTable $Messages
 */
class MessagesController extends AppController
{
	public function add()
	{
		$message = $this->Messages->newEntity();
		if ($this->request->is('post')) { 
			$message = $this->Messages->patchEntity($message, $this->request->data);
			$message->receiver_id=0;
			$message->sender_id=$this->Auth->user('id'); 
			if ($this->Messages->save($message)) {
				$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_SUCCESS,'message'=>__('Message has been sent.')]);
			} else {
				$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED, 'errors'=>$message->errors()]);
			}
		}
		$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED]);
	} 
	
	public function inbox()
	{
		 
	} 
	
	public function compose()
	{
		$message = $this->Messages->newEntity();
		$this->set('message',$message);
	}
	
	public function changeStatus($type='view')
	{
		if ($this->request->is(['patch', 'post', 'put'])) 
		{
			if($type=='view')
			{
				$messages = ($this->request->data('messages'));
				$messages = array_slice($messages, 0, 20);
			 
				$query = $this->Messages->find('all')
				->where(['Messages.sender_id' => $this->Auth->user('id')])
				->orWhere(['Messages.receiver_id' => $this->Auth->user('id')])
				->where(['Messages.id IN' => $messages])
				->limit(20);
				  
				foreach ($query as $q)
				{ 
					$messageObject = $this->Messages;
					$q->receiver_seen = 1;
					$messageObject->save($q);
				} 
			
				$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_SUCCESS]);
			}
			else 
			{
				$messages = ($this->request->data('messages'));
				$messages = array_slice($messages, 0, 20);
					
		
				$query = $this->Messages->find('all')
				->where(['Messages.sender_id' => $this->Auth->user('id')])
				->orWhere(['Messages.receiver_id' => $this->Auth->user('id')])
				->where(['Messages.id IN' => $messages])
				->limit(20);
		
					
				foreach ($query as $q)
				{
					$messageObject = $this->Messages;
					if($q->receiver_id==$this->Auth->user('id'))
					{
						$q->receiver_delete = 1;
					}
					else if($q->sender_id==$this->Auth->user('id'))
					{
						$q->sender_delete = 1;
					}
				
					$messageObject->save($q);
				}
					
				$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_SUCCESS]);
			}
		}
	} 
	 
    public function listMessages($type='inbox')
    {
    	$this->viewBuilder()->layout(false);
    	
    	if($type=='inbox')
    	{
    		$this->paginate = [
    				'conditions' => [
    						'receiver_id' => $this->Auth->user('id'),
    						'receiver_delete' => 0
    				],
    				'sort'=>'id desc',
    				'contain' => ['Senders'],
    				'maxLimit' => 20
    				 
    		];
    		$this->set('type','inbox');
    	}
    	else
    	{
    		$this->paginate = [
    				'conditions' => [
    						'sender_id' => $this->Auth->user('id'),
    						'sender_delete' => 0
    				],
    				'sort'=>'id desc',
    				'contain' => ['Receivers'],
    				'maxLimit' => 20
    					
    		];
    		$this->set('type','send');
    	}	
    	
    	
        $messages = $this->paginate($this->Messages);  
        $this->set(compact('messages'));
        $this->set('_serialize', ['messages']);
    }

    /**
     * View method
     *
     * @param string|null $id Message id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($message_id = null)
    {
    	$message =  $this->Messages
    	->find()
    	->contain([
    			'Senders' 
    	]) 
    	->where(
    			[
    				'Messages.id' => $message_id,
    				'OR' => [['sender_id' => $this->Auth->user('id')], ['receiver_id' => $this->Auth->user('id')]],
    			]
    	)->first();
    	  
    	if(!$message)
    	{
    		throw new NotFoundException(__('Message not found'));
    	}
    	
    	if($message->sender_id==$this->Auth->user('id'))
    	{
    		$message->sender_seen = 1;
    	}
    	elseif($message->receiver_id==$this->Auth->user('id'))
    	{ 
    		$message->receiver_seen = 1;
    	}

    	 
    	$messageObject = $this->Messages;  
    	$messageObject->save($message);
    	
        $this->set('message', $message);
        $this->set('_serialize', ['message']);
    }

    /*
    public function reply($message_id = null)
    {
    	
    	$message =  $this->Messages
    	->find()
    	->contain([
    			'Senders'
    	])
    	->select(['id','subject', 'content','created','Senders.username'])
    	->where(
    			[
    					'Messages.id' => $message_id,
    					'OR' => [['sender_id' => $this->Auth->user('id')], ['receiver_id' => $this->Auth->user('id')]],
    			]
    			)->first();
    	
    	if(!$message)
    	{
    		throw new NotFoundException(__('Message not found'));
    	}		
    			
        $message = $this->Messages->newEntity();
        if ($this->request->is('post')) {
            $message = $this->Messages->patchEntity($message, $this->request->data);
            if ($this->Messages->save($message)) {
                $this->Flash->success(__('The message has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The message could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('message'));
        $this->set('_serialize', ['message']);
    }
*/
   

    
}
