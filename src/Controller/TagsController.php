<?php
namespace App\Controller;

use App\Controller\AppController; 
use App\Model\Application\Ajax\ResponsCodes;
 
class TagsController extends AppController
{ 
	public function index()
	{
		$tag = $this->Tags->newEntity();
		$this->set(compact('tag'));
	}   
	
	public function lists()
	{
		$order='Tags.name asc';
		if(!empty($this->request->data['order'][0]['dir'])):
		$order='Tags.name '.$this->request->data['order'][0]['dir'];
		endif;
	
		$this->request->query['page']=(int) (((int)$this->request->data('start')+(int)$this->request->data('length'))/20);
		
		$conditions[] = array('Tags.user_id'=>$this->Auth->user('id'));
		if(!empty($this->request->data['search']['value'])):
		parse_str($this->request->data['search']['value'], $searchArray);
		$conditions[] = array('Tags.name Like'=>'%'.$searchArray['name'].'%');
		endif;
		
		$this->paginate = [
				'maxLimit' => 20,
				'order' => [
						$order
				],
				'conditions'=>$conditions
		];
	
	
		$tags = $this->paginate($this->Tags);
		$tagsPrepared = [];
		foreach($tags as $b)
		{
			$tagsPrepared[] = [$b->name,$b->id];
		}
		echo json_encode(['recordsTotal'=> $this->request->params['paging']['Tags']['count']  ,'recordsFiltered'=>$this->request->params['paging']['Tags']['count'],'data'=>$tagsPrepared]);
		exit;
	}

    public function add()
    {
        $tag = $this->Tags->newEntity();
        if ($this->request->is('post')) { 
        	$tag->user_id=$this->Auth->user('id');
        	$tag = $this->Tags->patchEntity($tag, $this->request->data);
            if ($this->Tags->save($tag)) { 
              $this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_SUCCESS,'message'=>__('Tag has been added.')]);
            } else {
              	$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED, 'errors'=>$tag->errors()]);
            } 
        }
        $this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED]);
    }

    public function edit($id = null)
    {
    	$tag = $this->Tags->get($id, [
    			'contain' => []
    	]);
    	if($tag&&$tag->user_id==$this->Auth->user('id'))
    	{
    		if ($this->request->is(['patch', 'post', 'put'])) {
    			$tag = $this->Tags->patchEntity($tag, $this->request->data);
    			if ($this->Tags->save($tag)) {
    				$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_SUCCESS,'message'=>__('Tag has been updated.')]);
    			} else {
    				$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED]);
    			}
    		}
    	}
    	$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED]);
    }
 
    public function editView($id = null)
    { 
    	$this->viewBuilder()->layout(false);
    	$tag = $this->Tags->get($id, [
    			'contain' => []
    	]);
    	if($tag&&$tag->user_id==$this->Auth->user('id'))
    	{
    		  $this->set(compact('tag'));
    	}
    	else
    	{
    		throw new MethodNotAllowedException();
    	} 
    }
 
    public function delete($id = null)
    {   
        $tag = $this->Tags->get($id);
        
        if($tag&&$tag->user_id==$this->Auth->user('id'))
        {
        	if ($this->Tags->delete($tag)) {
        		$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_SUCCESS,'message'=>__('Tag has been deleted.')]);
        	}
	    }
	    $this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED]);
    }
}
