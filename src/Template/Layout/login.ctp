<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
      <?= $this->Html->charset() ?>
       <title><?= $this->fetch('title') ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
   		<?= $this->Html->meta('icon') ?>
    	<?= $this->fetch('meta') ?>
     	<script>
   	 		function translate(string){return string}
   	 	</script>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />

        <?= $this->Html->css('/assets/global/plugins/font-awesome/css/font-awesome.min.css') ?>
        <?= $this->Html->css('/assets/global/plugins/simple-line-icons/simple-line-icons.min.css') ?>
        <?= $this->Html->css('/assets/global/plugins/bootstrap/css/bootstrap.min.css') ?>
        <?= $this->Html->css('/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') ?>

        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
         <?= $this->Html->css('/assets/global/plugins/select2/css/select2.min.css') ?>
        <?= $this->Html->css('/assets/global/plugins/select2/css/select2-bootstrap.min.css') ?>

      	 <?= $this->Html->css('/assets/global/css/components.min.css') ?>
        <?= $this->Html->css('/assets/global/css/plugins.min.css') ?>


        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <?= $this->Html->css('/assets/pages/css/login.min.css') ?>
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" />
          <?= $this->Html->css('style.css?v='.time()) ?>
     </head>
    <!-- END HEAD -->

    <body class=" login" id="contentRight">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="index.html">
                <img src="/assets/pages/img/logo-big.png" alt="" /> </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
   <?php
          echo  $this->Form->create($user,['class'=>'login-form'])
           ?>
                <h3 class="form-title font-green"><?php echo __('Sign In')?></h3>


                  <?php echo $this->Form->input('email', ['id'=>'email_login','type'=>'email','required'=>false,'label'=>['class'=>'control-label visible-ie8 visible-ie9'],'placeholder' => __('Email'),'autocomplete'=>'off','autocomplete'=>'off', 'class'=>'form-control form-control-solid placeholder-no-fix']);?>

                  <?php echo $this->Form->input('password', ['id'=>'password_login','required'=>false,'label'=>['class'=>'control-label visible-ie8 visible-ie9'],'placeholder' => __('Password'),'autocomplete'=>'off','autocomplete'=>'off', 'class'=>'form-control form-control-solid placeholder-no-fix']);?>
             
                <div class="form-actions">
                    <button type="submit" class="btn green uppercase"><?php echo __('Login')?></button>

                    <a href="javascript:;" id="forget-password" class="forget-password"><?php echo __('Forgot Password/Username?')?></a>
                </div>

                <div class="login-options">
                    <h4><?php echo __('Or login with')?></h4>
                    <ul class="social-icons">
                        <li>
                            <a class="social-icon-color facebook" data-original-title="facebook" href="javascript:;"></a>
                        </li>
                        <li>
                            <a class="social-icon-color twitter" data-original-title="Twitter" href="javascript:;"></a>
                        </li>
                        <li>
                            <a class="social-icon-color googleplus" data-original-title="Goole Plus" href="javascript:;"></a>
                        </li>
                        <li>
                            <a class="social-icon-color linkedin" data-original-title="Linkedin" href="javascript:;"></a>
                        </li>
                    </ul>
                </div>
                <div class="create-account">
                    <p>
                        <a href="javascript:;" id="register-btn" class="uppercase"><?php echo __('Create an account')?></a>
                    </p>
                </div>
          <?php echo $this->Form->end()?>
            <!-- END LOGIN FORM -->
            <!-- BEGIN FORGOT PASSWORD FORM -->
           <?php echo  $this->Form->create($user,['class'=>'forget-form'])  ?>
                <h3 class="font-green"><?php echo __('Forget Password/Username ?')?></h3>
                <p><?php echo __('Enter your e-mail address below to reset your password or retrieve username.')?> </p>
                <div class="form-group">
                  <?php echo $this->Form->input('email', ['id'=>'email_recovery','label'=>false,'placeholder' => __('Email'),'autocomplete'=>'off','autocomplete'=>'off', 'class'=>'form-control form-control-solid placeholder-no-fix']);?>
          		 </div>
                <div class="form-actions">
                    <button type="button" id="back-btn" class="btn green btn-outline"><?php echo __('Back')?></button>
                    <button type="submit" class="btn btn-success uppercase pull-right"><?php echo __('Submit')?></button>
                </div>
             <?php echo $this->Form->end()?>
            <!-- END FORGOT PASSWORD FORM -->
            <!-- BEGIN REGISTRATION FORM -->
             <?php echo  $this->Form->create($user,['class'=>'register-form','url'=>['action'=>'register']])  ?>
                <h3 class="font-green"><?php echo __('Sign Up')?></h3>
                <p class="hint"> <?php echo __('Enter your personal details below:')?> </p>
                <div class="form-group">
                  <?php echo $this->Form->input('first_name', ['label'=>['class'=>'control-label visible-ie8 visible-ie9'],'placeholder' => __('First name'),'autocomplete'=>'off','autocomplete'=>'off', 'class'=>'form-control form-control-solid placeholder-no-fix']);?>
              </div>
               <div class="form-group">
                    <?php echo $this->Form->input('last_name', ['label'=>['class'=>'control-label visible-ie8 visible-ie9'],'placeholder' => __('Last name'),'autocomplete'=>'off','autocomplete'=>'off', 'class'=>'form-control form-control-solid placeholder-no-fix']);?>
             </div>

                 <div class="form-group">
                    <?php echo $this->Form->input('email', ['type'=>'email','label'=>['class'=>'control-label visible-ie8 visible-ie9'],'placeholder' => __('Email'),'autocomplete'=>'off','autocomplete'=>'off', 'class'=>'form-control form-control-solid placeholder-no-fix']);?>
             </div>



                <p class="hint"> <?php echo __('Enter your account details below:')?> </p>
                <div class="form-group">
                	 <?php echo $this->Form->input('username', ['label'=>['class'=>'control-label visible-ie8 visible-ie9'],'placeholder' => __('Username'),'autocomplete'=>'off','autocomplete'=>'off', 'class'=>'form-control form-control-solid placeholder-no-fix']);?>
             	 </div>
             	 <div class="form-group">
                    <?php echo $this->Form->input('password', ['label'=>['class'=>'control-label visible-ie8 visible-ie9'],'placeholder' => __('Password'),'autocomplete'=>'off','autocomplete'=>'off', 'class'=>'form-control form-control-solid placeholder-no-fix']);?>
              </div>
                <div class="form-group">
                	 <?php echo $this->Form->input('repassword', ['label'=>['class'=>'control-label visible-ie8 visible-ie9'],'placeholder' => __('Re-type Your Password'),'autocomplete'=>'off','autocomplete'=>'off', 'type'=>'password','class'=>'form-control form-control-solid placeholder-no-fix']);?>
             	</div>
             	 <div class="form-group margin-top-20 margin-bottom-20">
                    <label class="mt-checkbox mt-checkbox-outline">
                        <input type="checkbox" name="tnc" /> <?php echo __('I agree to the')?>
                        <a href="javascript:;"><?php echo __('Terms of Service')?></a> &
                        <a href="javascript:;"><?php echo __('Privacy Policy')?> </a>
                        <span></span>
                    </label>
                    <div id="register_tnc_error"> </div>
                </div>
                <div class="form-actions">
                    <button type="button" id="register-back-btn" class="btn green btn-outline"><?php echo __('Back')?></button>
                    <button type="submit" id="register-submit-btn" class="registerButtonAction btn btn-success uppercase pull-right"><?php echo __('Submit')?></button>
                </div>
            <?php echo $this->Form->end()?>
            <!-- END REGISTRATION FORM -->
        </div>
        <div class="copyright"> 2014 © Metronic. Admin Dashboard Template. </div>
        <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script>
<script src="../assets/global/plugins/ie8.fix.min.js"></script>
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <?= $this->Html->script('/assets/global/plugins/jquery.min.js');?>
         <?= $this->Html->script('/assets/global/plugins/bootstrap/js/bootstrap.min.js');?>
         <?= $this->Html->script('/assets/global/plugins/js.cookie.min.js');?>
         <?= $this->Html->script('/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js');?>
         <?= $this->Html->script('/assets/global/plugins/jquery.blockui.min.js');?>
         <?= $this->Html->script('/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');?>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
         <?= $this->Html->script('/assets/global/plugins/select2/js/select2.full.min.js');?>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
         <?= $this->Html->script('/assets/global/scripts/app.min.js');?>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->

         <?= $this->Html->script('/assets/global/plugins/jquery-validation/js/jquery.validate.min.js');?>
         <?= $this->Html->script('/assets/global/plugins/jquery-validation/js/additional-methods.min.js');?>
         <?= $this->Html->script('/kladstat/scripts/login-kladstat.js?a=14');?>
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->

       <?= $this->Html->script('script.js?a=18') ?>
    	<?= $this->fetch('script') ?>

    </body>

</html>
