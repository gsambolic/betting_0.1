<?php

namespace App\View\Helper;

use Cake\View\Helper;

class LayoutHelper extends Helper
{
	 public function checkboxTemplate()
	 {
		 return   [
					 'multicheckboxTitle' => '<legend>{{text}}</legend>',
					 'multicheckboxWrapper' => '<fieldset{{attrs}}>{{content}}</fieldset>',
					 'checkbox' => '<input type="checkbox" name="{{name}}" value="{{value}}"{{attrs}}><span></span>',
					 'checkboxFormGroup' => '{{label}}',
					 'checkboxWrapper' => '<div class="mt-checkbox-inline">{{label}}</div>',
					 'nestingLabel' => '{{hidden}}<label class="mt-checkbox" {{attrs}}>{{input}}{{text}}<span></span></label>',
			 ];
	 }
}
