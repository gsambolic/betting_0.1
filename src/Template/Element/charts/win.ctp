<script type="text/javascript">

$(function() {
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
      var data = google.visualization.arrayToDataTable([
         ['Sport', 'Win', 'Lost' ],
         <?php foreach($elements as $e):?>
         ['<?php echo $gS->group->name;?>', <?php echo $gS->win_count;?>, <?php echo $gS->count - $gS->win_count;?>],
        <?php endforeach;?>
        <?php if($elementsNullCount):?>
          ['---', <?php echo $elementsNullCount->win_count;?>, <?php echo $elementsNullCount->count - $elementsNullCount->win_count;?>],
        <?php endif;?>
       ]);
         var options = {
            width: '100%',
            height: <?php echo sizeof($elements)*50+50;?>'px',
            legend: { position: 'top', maxLines: 3 },
            bar: { groupWidth: '85%', },
            isStacked: 'percent'
         };
         var chart = new google.visualization.BarChart(document.getElementById("winPlayedChartElement"));
         chart.draw(data, options);

    }
});
</script>
<?php echo $this->element('breadcrumps',['data'=>[ ['name'=>__('Dashboard')],['name'=>'Overview'] ]])?>


<div class="panel panel-default">
     <div class="panel-heading">
           <h3 class="panel-title">
           <?php echo $title?>
           </h3>
     </div>
     <div class="panel-body">
       <div class="col-lg-6">
           <div class="table-scrollable table-scrollable-borderless">
                   <table class="table table-hover table-light">
                       <thead>
                           <tr class="uppercase">
                             <th ><?php echo $title?> </th>
                             <th ><?php echo __('Played');?></th>
                             <th ><?php echo __('Win');?> </th>
                             <th><?php echo __('Lost');?> </th>
                             <th><?php echo __('Percentage');?> </th>
                           </tr>
                       </thead>
                  <tbody>
                          <?php foreach($lements as $e):?>
                             <tr>
                               <td ><?php echo $e->group->name;?></td>
                               <td><?php echo $e->count;?></td>
                               <td><?php echo $e->win_count;?></td>
                               <td><?php echo $e->count - $e->win_count;?></td>
                               <td>
                                <?php if($e->win_count==0):?>
                                    0
                                <?php endif;?>
                                <?php if($e->win_count!=0):?>
                                    <?php echo number_format(($e->win_count/$e->count)*100,2);?>%</td>
                                <?php endif; ?>
                             </tr>
                        <?php endforeach;?>

                        <?php if($elementsNullCount):?>
                          <tr>
                            <td >---</td>
                            <td><?php echo $elementsNullCount->count;?></td>
                            <td><?php echo $elementsNullCount->win_count;?></td>
                            <td><?php echo $elementsNullCount->count - $elementsNullCount->win_count;?></td>
                            <td>
                             <?php if($groupNullCount->win_count==0):?>
                                 0
                             <?php endif;?>
                             <?php if($elementsNullCount->win_count!=0):?>
                                 <?php echo number_format(($elementsNullCount->win_count/$elementsNullCount->count)*100,2);?>%</td>
                             <?php endif; ?>
                          </tr>
                       <?php endif;?>
                   </tbody>

                 </table>
               </div>
       </div>
       <div class="col-lg-6" style="height:100%;">
          <div id="winPlayedChartElement"></div>
       </div>
</div>
<?= $this->Html->script('charts/google/loader.js',['block' => 'scriptBottom'])?>
