var AddBet = function() {
    var initEditables = function() {

        //set editable mode based on URL parameter
        if (App.getURLParameter('mode') == 'inline') {
            $.fn.editable.defaults.mode = 'inline';
            $('#inline').attr("checked", true);
        } else {
            $('#inline').attr("checked", false);
        }

        //global settings
        $.fn.editable.defaults.inputclass = 'form-control';

        $("#betsNewEntrySports").typeahead({
    	    source: function(query, process) {
    	        objects = [];
    	        map = {};
    	        $.each(AVAILABLE_SPORTS_NEW_ENTRY, function(i, object) {
    	            map[object.name] = object;
    	            objects.push(object.name);
    	        });
    	        process(objects);
    	    },
    	    updater: function(item) {
    	    	$("#betsNewEntryGroup").typeahead('destroy');
    	        $('#sport-id').val(map[item].id);
    	    	 $.get(BASE_URL+"groups/user-groups/"+map[item].id, function(dataGroup){
    	    		 $("#betsNewEntryGroup").val("");
    	             $("#betsNewEntryGroup").typeahead({
    	         	    source: function(query, process) {
    	         	        objects = [];
    	         	        map = {};
    	         	        $.each(dataGroup, function(i, object) {
    	         	            map[object.name] = object;
    	         	            objects.push(object.name);
    	         	        });
    	         	        process(objects);
    	         	    },
    	         	    updater: function(item) {
    	         	        return item;
    	         	    }
    	         	});
    	           },'json');

    	        return item;
    	    }
    	});


        $("#betsNewEntryBetsTypes").typeahead({
    	    source: function(query, process) {
    	        objects = [];
    	        map = {};
    	        $.each( AVAILABLE_BETS_TYPES_NEW_ENTRY, function(i, object) {
    	            map[object.name] = object;
    	            objects.push(object.name);
    	        });
    	        process(objects);
    	    },
    	    updater: function(item) {
    	        //$('hiddenInputElement').val(map[item].id);
    	        return item;
    	    }
    	});



        $("#date-of-event").datetimepicker({
            autoclose: true,
            isRTL: App.isRTL(),
            format: "yyyy-mm-dd hh:ii",
            pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left")
        });

        $("#date-of-ticket").datetimepicker({
            autoclose: true,
            isRTL: App.isRTL(),
            format: "yyyy-mm-dd hh:ii",
            pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left")
        });

        // Workaround to fix datetimepicker position on window scroll
        $( document ).scroll(function(){
            $('.form_datetime').datetimepicker('place'); //#modal is the id of the modal
        });
    }

    return {
    	initEditables: function() {
            initEditables();

        }

    };

}();

jQuery(document).ready(function() {

	AddBet.initEditables();
	$(document).on('click',".newEntryAction",
			  function()
			  {
          var currentForm = $(this).closest('form');
          var reset = $(this).attr('data-reset');
					if( !$("#betTicketId").val() )
					{
						bootbox.alert(translate('Please add ticket before adding bets.'));
			        	return;
					}
				ajaxForm.send(currentForm,
					function(parsedData,form)
					{
						 var entityDatatableInner =  entryDataTable.getEntityDatatable().getDataTable().ajax;
							entityDatatableInner.reload();
              if(reset=='true')
              {
                var newTime = new Date().getFullYear()+"-"+(new Date().getMonth()+1)+"-"+new Date().getDate()+" 00:00:00";
                $(currentForm)[0].reset();
                  $('#date-of-event').combodate('setValue',newTime);
              }
					}
				);
			  }
		   )
});
