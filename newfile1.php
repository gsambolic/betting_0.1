 
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema betstats
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema betstats
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `betstats` DEFAULT CHARACTER SET utf8 ;
USE `betstats` ;

-- -----------------------------------------------------
-- Table `betstats`.`sports`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `betstats`.`sports` (
		`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
		`name` VARCHAR(100) NOT NULL,
		`user_id` INT(10) UNSIGNED NULL DEFAULT NULL,
		`created` DATETIME NULL DEFAULT NULL,
		PRIMARY KEY (`id`))
		ENGINE = InnoDB
		AUTO_INCREMENT = 18
		DEFAULT CHARACTER SET = utf8
		COMMENT = 'InnoDB free: 7168 kB; (`bookhouse_id`) REFER `nova_kladio';


		-- -----------------------------------------------------
		-- Table `betstats`.`groups`
		-- -----------------------------------------------------
		CREATE TABLE IF NOT EXISTS `betstats`.`groups` (
				`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
				`name` VARCHAR(100) NOT NULL,
				`sport_id` INT(11) UNSIGNED NOT NULL DEFAULT '0',
				PRIMARY KEY (`id`),
				UNIQUE INDEX `group_name` (`name` ASC, `sport_id` ASC),
				INDEX `groups_to_sports` (`sport_id` ASC),
				CONSTRAINT `groups_to_sports`
				FOREIGN KEY (`sport_id`)
				REFERENCES `betstats`.`sports` (`id`)
				ON DELETE CASCADE
				ON UPDATE CASCADE)
				ENGINE = InnoDB
				AUTO_INCREMENT = 99
				DEFAULT CHARACTER SET = utf8
				COMMENT = 'InnoDB free: 7168 kB; (`sport_id`) REFER `nova_kladionic';


				-- -----------------------------------------------------
				-- Table `betstats`.`bets_types`
				-- -----------------------------------------------------
				CREATE TABLE IF NOT EXISTS `betstats`.`bets_types` (
						`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
						`name` VARCHAR(100) NOT NULL DEFAULT '0',
						PRIMARY KEY (`id`))
						ENGINE = InnoDB
						AUTO_INCREMENT = 98
						DEFAULT CHARACTER SET = utf8
						COMMENT = 'InnoDB free: 7168 kB; (`id`) REFER `nova_k';


						-- -----------------------------------------------------
						-- Table `betstats`.`teams`
						-- -----------------------------------------------------
						CREATE TABLE IF NOT EXISTS `betstats`.`teams` (
								`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
								`name` VARCHAR(100) NOT NULL,
								`group_id` INT(11) UNSIGNED NOT NULL,
								PRIMARY KEY (`id`),
								UNIQUE INDEX `unike` (`name` ASC, `group_id` ASC))
								ENGINE = InnoDB
								AUTO_INCREMENT = 39
						DEFAULT CHARACTER SET = utf8
						COMMENT = 'InnoDB free: 7168 kB; (`id`) REFER `nova_kladionic';


						-- -----------------------------------------------------
						-- Table `betstats`.`bets`
						-- -----------------------------------------------------
						CREATE TABLE IF NOT EXISTS `betstats`.`bets` (
								`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
								`name` VARCHAR(250) NOT NULL,
								`group_id` INT(11) NOT NULL,
								`date` DATETIME NOT NULL,
								`created` DATETIME NULL,
								`bets_type_id` INT NULL,
								`winner_team_id` INT NULL,
								`selected_team_id` INT NULL,
								`odds_values` FLOAT(9,2) NULL,
								`user_id` INT NULL,
								`sport_id` INT NULL,
								`winners_bets_types_selections` INT NULL,
								`selected_bets_types_selections` INT NULL,
								PRIMARY KEY (`id`),
								INDEX `groups_id` (`group_id` ASC),
								INDEX `bets_types_idx` (`bets_type_id` ASC),
								INDEX `winner_id_idx` (`winner_team_id` ASC),
								INDEX `selected_id_idx` (`selected_team_id` ASC),
								CONSTRAINT `groups_id`
								FOREIGN KEY (`group_id`)
								REFERENCES `betstats`.`groups` (`id`)
								ON DELETE CASCADE
								ON UPDATE CASCADE,
								CONSTRAINT `bets_types`
								FOREIGN KEY (`bets_type_id`)
								REFERENCES `betstats`.`bets_types` (`id`)
								ON DELETE NO ACTION
								ON UPDATE NO ACTION,
								CONSTRAINT `winner_id`
								FOREIGN KEY (`winner_team_id`)
								REFERENCES `betstats`.`teams` (`id`)
								ON DELETE NO ACTION
								ON UPDATE NO ACTION,
								CONSTRAINT `selected_id`
								FOREIGN KEY (`selected_team_id`)
								REFERENCES `betstats`.`teams` (`id`)
								ON DELETE NO ACTION
								ON UPDATE NO ACTION)
								ENGINE = InnoDB
								AUTO_INCREMENT = 13
						DEFAULT CHARACTER SET = utf8
						COMMENT = 'InnoDB free: 7168 kB; (`group_id`) REFER `nova_kladionic';


						-- -----------------------------------------------------
						-- Table `betstats`.`bookhouses`
						-- -----------------------------------------------------
						CREATE TABLE IF NOT EXISTS `betstats`.`bookhouses` (
								`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
								`name` VARCHAR(100) NOT NULL,
								`default_money_value` DECIMAL(15,2) NOT NULL DEFAULT '0.00',
								`can_user_register` TINYINT(4) UNSIGNED NOT NULL DEFAULT '1',
								`active` TINYINT(4) UNSIGNED NOT NULL DEFAULT '0',
								`email_conf_req` TINYINT(4) UNSIGNED NOT NULL DEFAULT '0',
								PRIMARY KEY (`id`))
								ENGINE = InnoDB
								AUTO_INCREMENT = 2
								DEFAULT CHARACTER SET = utf8
								COMMENT = 'popis kladionica, u komercijalnoj verziji ograniciti na jeda';


								-- -----------------------------------------------------
								-- Table `betstats`.`users_roles`
								-- -----------------------------------------------------
								CREATE TABLE IF NOT EXISTS `betstats`.`users_roles` (
										`id` TINYINT(4) UNSIGNED NOT NULL AUTO_INCREMENT,
										`name` VARCHAR(50) NOT NULL,
										PRIMARY KEY (`id`))
										ENGINE = InnoDB
										AUTO_INCREMENT = 5
								DEFAULT CHARACTER SET = utf8
								COMMENT = 'InnoDB free: 7168 kB';


								-- -----------------------------------------------------
								-- Table `betstats`.`users`
								-- -----------------------------------------------------
								CREATE TABLE IF NOT EXISTS `betstats`.`users` (
										`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
										`username` VARCHAR(20) NOT NULL,
										`password` VARCHAR(60) NOT NULL,
										`last_login` DATETIME NULL DEFAULT NULL,
										`created` DATETIME NOT NULL,
										`first_name` VARCHAR(50) NOT NULL,
										`last_name` VARCHAR(50) NOT NULL,
										`email` VARCHAR(250) NOT NULL,
										`users_role_id` TINYINT(4) UNSIGNED NOT NULL DEFAULT '3',
										`email_validation` VARCHAR(50) NULL DEFAULT NULL,
										`active` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
										`email_validated` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
										`newsletter` TINYINT(3) UNSIGNED NOT NULL DEFAULT '1',
										PRIMARY KEY (`id`),
										UNIQUE INDEX `unike` (`username` ASC),
										UNIQUE INDEX `unikeemail` (`email` ASC),
										INDEX `Ref_39` (`users_role_id` ASC),
										CONSTRAINT `users_ibfk_1`
										FOREIGN KEY (`users_role_id`)
										REFERENCES `betstats`.`users_roles` (`id`)
										ON DELETE CASCADE
										ON UPDATE CASCADE)
										ENGINE = InnoDB
										AUTO_INCREMENT = 24
										DEFAULT CHARACTER SET = utf8
										COMMENT = 'InnoDB free: 7168 kB; (`users_status_id`) REFER `nova_klad';


										-- -----------------------------------------------------
										-- Table `betstats`.`messages`
										-- -----------------------------------------------------
										CREATE TABLE IF NOT EXISTS `betstats`.`messages` (
												`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
												`sender_id` INT(10) UNSIGNED NULL DEFAULT NULL,
												`receiver_id` INT(10) UNSIGNED NULL DEFAULT NULL,
												`subject` VARCHAR(245) CHARACTER SET 'utf8' NULL DEFAULT NULL,
												`content` VARCHAR(5000) CHARACTER SET 'utf8' NULL DEFAULT NULL,
												`message_id` INT(10) UNSIGNED NULL DEFAULT NULL,
												`created` DATETIME NOT NULL,
												`receiver_seen` INT(1) UNSIGNED NULL DEFAULT '0',
												`receiver_delete` INT(1) UNSIGNED NULL DEFAULT '0',
												`sender_delete` INT(1) UNSIGNED NULL DEFAULT '0',
												PRIMARY KEY (`id`),
												INDEX `fk_messages_1_idx` (`sender_id` ASC),
												INDEX `receiver_idx` (`receiver_id` ASC),
												CONSTRAINT `sender`
												FOREIGN KEY (`sender_id`)
												REFERENCES `betstats`.`users` (`id`)
												ON DELETE NO ACTION
												ON UPDATE NO ACTION,
												CONSTRAINT `receiver`
												FOREIGN KEY (`receiver_id`)
												REFERENCES `betstats`.`users` (`id`)
												ON DELETE NO ACTION
												ON UPDATE NO ACTION)
												ENGINE = InnoDB
												AUTO_INCREMENT = 2
												DEFAULT CHARACTER SET = utf8
												COLLATE = utf8_bin;


												-- -----------------------------------------------------
												-- Table `betstats`.`transactions_types`
												-- -----------------------------------------------------
												CREATE TABLE IF NOT EXISTS `betstats`.`transactions_types` (
														`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
														`name` VARCHAR(45) CHARACTER SET 'latin1' NULL DEFAULT NULL,
														PRIMARY KEY (`id`))
														ENGINE = InnoDB
														AUTO_INCREMENT = 4
														DEFAULT CHARACTER SET = utf8;


														-- -----------------------------------------------------
														-- Table `betstats`.`transactions`
														-- -----------------------------------------------------
														CREATE TABLE IF NOT EXISTS `betstats`.`transactions` (
																`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
																`user_id` INT(11) UNSIGNED NOT NULL DEFAULT '0',
																`money` DECIMAL(11,2) NOT NULL,
																`date_created` DATETIME NOT NULL,
																`transactions_type_id` INT(11) UNSIGNED NOT NULL,
																`transactions_type_idendifier` INT(11) UNSIGNED NULL DEFAULT NULL,
																PRIMARY KEY (`id`),
																INDEX `Ref_31` (`user_id` ASC),
																INDEX `fk_transactions_1` (`transactions_type_id` ASC),
																CONSTRAINT `fk_transactions_1`
																FOREIGN KEY (`transactions_type_id`)
																REFERENCES `betstats`.`transactions_types` (`id`))
																ENGINE = InnoDB
																AUTO_INCREMENT = 90
																DEFAULT CHARACTER SET = utf8
																COMMENT = 'InnoDB free: 7168 kB; (`user_id`) REFER `nova_kladionica/';




																-- -----------------------------------------------------
																-- Table `betstats`.`bets_types_selections`
																-- -----------------------------------------------------
																CREATE TABLE IF NOT EXISTS `betstats`.`bets_types_selections` (
																		`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
																		`name` VARCHAR(100) NOT NULL DEFAULT '0',
																		PRIMARY KEY (`id`))
																		ENGINE = InnoDB
																		AUTO_INCREMENT = 98
																		DEFAULT CHARACTER SET = utf8
																		COMMENT = 'InnoDB free: 7168 kB; (`id`) REFER `nova_k';


																		SET SQL_MODE=@OLD_SQL_MODE;
																		SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
																		SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
