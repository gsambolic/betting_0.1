<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * View Controller
 *
 * @property \App\Model\Table\ViewTable $View
 */
class ViewController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $view = $this->paginate($this->View);

        $this->set(compact('view'));
        $this->set('_serialize', ['view']);
    }

    /**
     * View method
     *
     * @param string|null $id View id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $view = $this->View->get($id, [
            'contain' => []
        ]);

        $this->set('view', $view);
        $this->set('_serialize', ['view']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $view = $this->View->newEntity();
        if ($this->request->is('post')) {
            $view = $this->View->patchEntity($view, $this->request->data);
            if ($this->View->save($view)) {
                $this->Flash->success(__('The view has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The view could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('view'));
        $this->set('_serialize', ['view']);
    }

    /**
     * Edit method
     *
     * @param string|null $id View id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $view = $this->View->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $view = $this->View->patchEntity($view, $this->request->data);
            if ($this->View->save($view)) {
                $this->Flash->success(__('The view has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The view could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('view'));
        $this->set('_serialize', ['view']);
    }

    /**
     * Delete method
     *
     * @param string|null $id View id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $view = $this->View->get($id);
        if ($this->View->delete($view)) {
            $this->Flash->success(__('The view has been deleted.'));
        } else {
            $this->Flash->error(__('The view could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
