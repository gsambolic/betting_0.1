<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BetsTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BetsTypesTable Test Case
 */
class BetsTypesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BetsTypesTable
     */
    public $BetsTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.bets_types',
        'app.users',
        'app.users_roles',
        'app.bets_slips',
        'app.transactions',
        'app.user_money_overalls',
        'app.bets',
        'app.groups',
        'app.sports'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('BetsTypes') ? [] : ['className' => 'App\Model\Table\BetsTypesTable'];
        $this->BetsTypes = TableRegistry::get('BetsTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BetsTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
