<script type="text/javascript">

$(function() {


  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawCharts);

  function drawCharts() {

    var data = google.visualization.arrayToDataTable([
      ['Task', 'Played Money'],
      ['Money on Win',     <?php echo $amountOnWinSum;?>],
      ['Money on Lost',    <?php echo $amountOnLostSum;?>],
    ]);

    var moneyPlayedChart = new google.visualization.PieChart(document.getElementById('moneyPlayedChart'));

    var options = {
      colors: ['#3366ff', '#ff3300']
    };

    moneyPlayedChart.draw(data ,options);


    var data = google.visualization.arrayToDataTable([
      ['Task', 'Played Ticket'],
      ['Win Ticket',     <?php echo $win;?>],
      ['Lost Ticket',    <?php echo $lost;?>],
    ]);

    var ticketsPlayedChart = new google.visualization.PieChart(document.getElementById('ticketsPlayedChart'));

    ticketsPlayedChart.draw(data ,options);

    var data = google.visualization.arrayToDataTable([
      ['Task', 'Played Bets'],
      ['Win Bet',     <?php echo $winBets;?>],
      ['Lost Bet',    <?php echo $lostBets;?>],
    ]);

    var betsPlayedChart = new google.visualization.PieChart(document.getElementById('betsPlayedChart'));

    betsPlayedChart.draw(data ,options);


  }
});
</script>


<?php echo $this->element('breadcrumps',['data'=>[ ['name'=>__('Dashboard')],['name'=>'Overview'] ]])?>


   <div class="panel panel-default">
        <div class="panel-heading">
                    <h3 class="panel-title">
                        <?php echo __('Money')?><span class="<?php if($totalSumValue<0){echo 'font-red-haze';}?>" data-counter="counterup" data-value="<?php echo $totalSumValue;?>"> <?php echo $totalSumValue;?></span>
                        <small class="<?php if($totalSumValue<0){echo 'font-red-haze';}else{ echo 'font-green-sharp';}?>"></small>
                    </h3>
          </div>
          <div class="panel-body">
            <div class="col-lg-6">
                <table class="table table-hover table-light">
                      <thead>
                          <tr class="uppercase">
                              <th ><?php echo __('Money Played');?></th>
                              <th ><?php echo __('Money on Win');?></th>
                              <th><?php echo __('Money on Lost');?></th>
                              <th><?php echo __('Win Played');?></th>
                          </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td >
                            <?php echo $totalSumPlayed;?>
                          </td>

                          <td>   <?php echo $amountOnWinSum;?> </td>
                          <td>   <?php echo $amountOnLostSum;?> </td>
                          <td>   <?php echo $winRatioMoney;?>% </td>
                      </tr>
                  </tbody>
                </table>
              </div>
            <div class="col-lg-6"> <div style="width:100%;height:100px" id="moneyPlayedChart"></div>  </div>
            </div>
          </div>
<div class="panel panel-default">
     <div class="panel-heading">
         <h3 class="panel-title">
             <?php echo __('Tickets')?>
        </h3>
    </div>
    <div class="panel-body">
      <div class="col-lg-6">
                 <table class="table table-hover table-light">
                     <thead>
                         <tr class="uppercase">
                             <th ><?php echo __('Tickets Played');?> </th>
                             <th ><?php echo __('Tickets Win');?> </th>
                             <th><?php echo __('Tickets Lost');?> </th>
                             <th><?php echo __('Percentage');?> </th>
                         </tr>
                     </thead>
                     <tbody>
                       <tr>
                         <td >
                           <?php echo $total;?>
                         </td>

                         <td>   <?php echo $win;?> </td>
                         <td>   <?php echo $lost;?> </td>
                         <td>   <?php echo $winRatio;?>% </td>
                     </tr>
                 </tbody>
               </table>
      </div>
      <div class="col-lg-6"> <div style="width:100%;height:100px" id="ticketsPlayedChart"></div>  </div>
      </div>
 </div>



 <div class="panel panel-default">
      <div class="panel-heading">
          <h3 class="panel-title">
            <?php echo __('Bets')?>
         </h3>
       </div>
       <div class="panel-body">
         <div class="col-lg-6">
           <div class="table-scrollable table-scrollable-borderless">
                <table class="table table-hover table-light">
                    <thead>
                        <tr class="uppercase">
                            <th ><?php echo __('Bets Played');?> </th>
                            <th ><?php echo __('Bets Win');?> </th>
                            <th><?php echo __('Bets Lost');?> </th>
                            <th><?php echo __('Percentage');?> </th>
                        </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td >
                          <?php echo $totalBets;?>
                        </td>

                        <td>   <?php echo $winBets;?> </td>
                        <td>   <?php echo $lostBets;?> </td>
                        <td>   <?php echo $winRatioBets;?>% </td>
                    </tr>
                </tbody>
              </table>
            </div>
        </div>

        <div class="col-lg-6"> <div style="width:100%;height:100px" id="betsPlayedChart"></div>  </div>


      </div>
</div> 

<?= $this->Html->script('https://www.gstatic.com/charts/loader.js',['block' => 'scriptBottom'])?>
