<?php
namespace App\Controller;

use App\Controller\AppController; 
use App\Model\Application\Ajax\ResponsCodes;
 
class BetsTypesController extends AppController
{
	
	public function userBetsTypes()
	{
	
		$betsTypes = TableRegistry::get('BetsTypes');
		$results = $betsTypes->find('all', [
				'order' => ['BetsTypes.name' => 'DESC'],
				'conditions' => [
						'OR'=>
						[
								['user_id' => $this->Auth->user('id')],
								['user_id' => 0]
						]
				],
				'fields'=>['BetsTypes.id','BetsTypes.name']
		]);
	
		$jsonData = array();
	
		foreach ($results as $s)
		{
			$jsonData[]=array('id'=>$s->id,'name'=>$s->name);
		}
		echo json_encode($jsonData);
		exit;
	}

	public function index()
	{
		$betsType = $this->BetsTypes->newEntity();
		$this->set(compact('betsType'));
	}   
	
	public function lists()
	{
		$order='BetsTypes.name asc';
		if(!empty($this->request->data['order'][0]['dir'])):
		$order='BetsTypes.name '.$this->request->data['order'][0]['dir'];
		endif;
	
		$this->request->query['page']=(int) (((int)$this->request->data('start')+(int)$this->request->data('length'))/20);
		
		$conditions[] = array('BetsTypes.user_id'=>$this->Auth->user('id'));
		if(!empty($this->request->data['search']['value'])):
		parse_str($this->request->data['search']['value'], $searchArray);
		$conditions[] = array('BetsTypes.name Like'=>'%'.$searchArray['name'].'%');
		endif;
		
		$this->paginate = [
				'maxLimit' => 20,
				'order' => [
						$order
				],
				'conditions'=>$conditions
		];
	
	
		$betsTypes = $this->paginate($this->BetsTypes);
		$betsTypesPrepared = [];
		foreach($betsTypes as $b)
		{
			$betsTypesPrepared[] = [$b->name,$b->id];
		}
		echo json_encode(['recordsTotal'=> $this->request->params['paging']['BetsTypes']['count']  ,'recordsFiltered'=>$this->request->params['paging']['BetsTypes']['count'],'data'=>$betsTypesPrepared]);
		exit;
	}

    public function add()
    {
        $betsType = $this->BetsTypes->newEntity();
        if ($this->request->is('post')) { 
        	$betsType->user_id=$this->Auth->user('id');
        	$betsType = $this->BetsTypes->patchEntity($betsType, $this->request->data);
            if ($this->BetsTypes->save($betsType)) { 
              $this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_SUCCESS,'message'=>__('Bets Type has been added.')]);
            } else {
              	$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED, 'errors'=>$betsType->errors()]);
            } 
        }
        $this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED]);
    }

    public function edit($id = null)
    {
    	$betsType = $this->BetsTypes->get($id, [
    			'contain' => []
    	]);
    	if($betsType&&$betsType->user_id==$this->Auth->user('id'))
    	{
    		if ($this->request->is(['patch', 'post', 'put'])) {
    			$betsType = $this->BetsTypes->patchEntity($betsType, $this->request->data);
    			if ($this->BetsTypes->save($betsType)) {
    				$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_SUCCESS,'message'=>__('Bets Type has been updated.')]);
    			} else {
    				$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED]);
    			}
    		}
    	}
    	$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED]);
    }
 
    public function editView($id = null)
    { 
    	$this->viewBuilder()->layout(false);
    	$betsType = $this->BetsTypes->get($id, [
    			'contain' => []
    	]);
    	if($betsType&&$betsType->user_id==$this->Auth->user('id'))
    	{
    		  $this->set(compact('betsType'));
    	}
    	else
    	{
    		throw new MethodNotAllowedException();
    	} 
    }
 
    public function delete($id = null)
    {   
        $betsType = $this->BetsTypes->get($id);
        
        if($betsType&&$betsType->user_id==$this->Auth->user('id'))
        {
        	if ($this->BetsTypes->delete($betsType)) {
        		$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_SUCCESS,'message'=>__('Bets Type has been deleted.')]);
        	}
	    }
	    $this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED]);
    }
}
