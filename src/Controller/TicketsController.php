<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Application\Ajax\ResponsCodes;
use Cake\Controller\Component\AuthComponent;
use Cake\ORM\TableRegistry;
/**
 * Tickets Controller
 *
 * @property \App\Model\Table\TicketsTable $Tickets
 */
class TicketsController extends AppController
{
    public function index()
    {
      $tagsTable = TableRegistry::get('Tags');
      $tagsQuery =   $tagsTable->find('list');
      $tagsQuery->where(['Tags.user_id'=>$this->Auth->user('id')]);
      $tagsArray = $tagsQuery->toArray();
      $this->set('tags',$tagsArray);
    }

    public function lists()
    {
    	$order='Tickets.created desc';
      $fields=['Tickets.name','Tickets.amount','Tickets.system','Tickets.date_of_ticket','TicketsTags.ticket_id'];

    	if(!empty($this->request->data['order'][0]['dir'])):
    		 $order='Tickets.date_of_ticket '.$this->request->data['order'][0]['dir'];
    	endif;

      if(!empty($this->request->data['order'][1]['dir'])):
         $order='Tickets.win'.$this->request->data['order'][1]['dir'];
      endif;

      if(!empty($this->request->data['order'][2]['dir'])):
    		 $order='Tickets.name'.$this->request->data['order'][2]['dir'];
    	endif;

      if(!empty($this->request->data['order'][3]['dir'])):
    		 $order='Tickets.amount'.$this->request->data['order'][3]['dir'];
    	endif;

      if(!empty($this->request->data['order'][4]['dir'])):
         $order='Tickets.system'.$this->request->data['order'][4]['dir'];
      endif;


      $conditions[] = array('Tickets.user_id'=>$this->Auth->user('id'));
      $joins = array();
      if(!empty($this->request->data['search']['value'])):
        parse_str($this->request->data['search']['value'], $searchArray);

        if(!empty($searchArray['name']))
        {
            $conditions[] = array('Tickets.name Like'=>'%'.$searchArray['name'].'%');
        }

        if(!empty($searchArray['system']))
        {
            $conditions[] = array('Tickets.system'=>$searchArray['system']);
        }

        if(!empty($searchArray['amount']))
        {
            if(!empty($searchArray['amount_filter'])||@$searchArray['amount_filter']==0)
            {
              if($searchArray['amount_filter']==0)
              {
                  $conditions[] = array('Tickets.amount'=>$searchArray['amount']);
              }
              if($searchArray['amount_filter']==1)
              {
                  $conditions[] = array('Tickets.amount >'=>$searchArray['amount']);
              }
              if($searchArray['amount_filter']==2)
              {
                  $conditions[] = array('Tickets.amount <'=>$searchArray['amount']);
              }
            }
        }

        if(!empty($searchArray['created_from']))
        {
            $conditions[] = array('Tickets.created >='=>date('Y-m-d H:i:s',strtotime($searchArray['created_from'])));
        }

        if(!empty($searchArray['created_to']))
        {
            $conditions[] = array('Tickets.created <='=>date('Y-m-d H:i:s',strtotime($searchArray['created_to'])));
        }

        if(!empty($searchArray['date_of_ticket_from']))
        {
            $conditions[] = array('Tickets.date_of_ticket >='=>date('Y-m-d H:i:s',strtotime($searchArray['date_of_ticket_from'])));
        }

        if(!empty($searchArray['date_of_ticket_to']))
        {
            $conditions[] = array('Tickets.date_of_ticket <='=>date('Y-m-d H:i:s',strtotime($searchArray['date_of_ticket_to'])));
        }

        if(!empty($searchArray['tags'])&&is_array($searchArray['tags']))
        {
            $conditions[] = array('TicketsTags.tag_id IN'=>$searchArray['tags']);
            $joins[]=[
                'alias'=>'TicketsTags',
                'table' => 'tickets_tags',
                'type' => 'LEFT',
                'conditions' => 'TicketsTags.ticket_id = Tickets.id',
            ];

        }

      endif;


    	$this->request->query['page']=(int) (((int)$this->request->data('start')+(int)$this->request->data('length'))/20);
    	$this->paginate = [
          'join'=>$joins,
    			'maxLimit' => 20,
          'contain'=>['Tags'],
    			'order' => [
    					$order
    			],
    			'conditions'=>$conditions
    	];


    	$tickets = $this->paginate($this->Tickets);
    	$ticketPrepared = [];

    	foreach($tickets as $t)
    	{
    		$tagsString='';
    		if($t->tags)
    		{
    			$divader="";
    			foreach($t->tags as $tag)
    			{
    				$tagsString.=$divader.$tag->name ;
    				$divader.=", ";
    			}

    		}	;

        if($t->win)
  			{
  				$winOrLost =_("Win");
  			}
  			else
  			{
  				$winOrLost =_("Lost");
  			}

    		$ticketPrepared[] = [date('d-m-Y',strtotime($t->date_of_ticket)),$winOrLost,$t->name,$t->amount,$t->system,$tagsString,$t->id];
    	}
    	echo json_encode(['recordsTotal'=> $this->request->params['paging']['Tickets']['count']  ,'recordsFiltered'=>$this->request->params['paging']['Tickets']['count'],'data'=>$ticketPrepared]);
    	exit;
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
    	if($this->request->data('id'))
    	{
    		$this->edit($this->request->data('id'));
    	}
        $ticket = $this->Tickets->newEntity();
        $tags = $this->Tickets->Tags->find('list', ['limit' => 200]);
        $tagsArray = $tags->toArray();
        if ($this->request->is('post')) {

        	if(!empty($this->request->data['tags']['_ids']))
        	{
        		if(!is_array($this->request->data['tags']['_ids']))
        		{
        			$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED, 'errors'=>['tags'=>['not valid'=>__('Tags are not valid')]]]);
        		}
        		else
        		{
        			$allowedTagsIds = array_keys($tagsArray);
        			foreach ($this->request->data['tags']['_ids'] as $t)
        			{
        				if(!in_array($t, $allowedTagsIds))
        				{
        					$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED, 'errors'=>['tags'=>['not allowed'=>__('Tag are not allowed')]]]);
        				}
        			}
        		}
        	}

        	if(!empty($this->request->data['tags_text']))
        	{
        		$explodedTags = explode(',', $this->request->data['tags_text']);
        		foreach ($explodedTags as $keyET=>$eT)
        		{
        			if(!in_array($eT, $tagsArray))
        			{
        				$explodedTagsFiltered[$keyET]['name']=trim($eT);
        				$explodedTagsFiltered[$keyET]['user_id']=$this->Auth->user('id');
        			}
        		}
        	}
        	if(!empty($explodedTagsFiltered))
        	{
        		if(!empty($this->request->data['tags']['_ids']))
        		{
        			foreach ($this->request->data['tags']['_ids'] as $tIDS)
        			{
        				$explodedTagsFiltered[]=array('id'=>$tIDS);
        			}
        		}
        	}

        	if(!empty($explodedTagsFiltered))
        	{
        		$this->request->data['tags']=$explodedTagsFiltered;
        	}

        	$ticket->user_id = $this->Auth->user('id');
            $ticket = $this->Tickets->patchEntity($ticket, $this->request->data
            , [
                'associated' => [
                    'Tags' => [
                          'accessibleFields' => ['user_id' => true]
                        ]
                    ]
                ]
          );
            if ($this->Tickets->save($ticket)) {
               $this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_SUCCESS,'message'=>__('Ticket has been saved, you are now ready to add bets.'),'id'=>$ticket->id]);
            } else {
                $this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED, 'errors'=>$ticket->errors()]);
            }
        }

        $this->set(compact('ticket', 'tags'));
        $this->set('_serialize', ['ticket']);
    }

     public function edit($id = null)
    {
        $ticket = $this->Tickets->get($id, [
            'contain' => []
        ]);

        if(!empty($this->request->data('date_of_ticket')))
        {
          $this->request->data['date_of_ticket'] = date('Y-m-d H:i:s',strtotime($this->request->data('date_of_ticket')));
        }

        if($ticket&&$ticket->user_id==$this->Auth->user('id'))
        {
        	if ($this->request->is(['patch', 'post', 'put'])) {
        		$ticket = $this->Tickets->patchEntity($ticket, $this->request->data);
        		if ($this->Tickets->save($ticket)) {
        			$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_SUCCESS,'message'=>__('Ticket has been updated.')]);
        		} else {
        			 $this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED,'message'=>__('Update failed.')]);
        		}
        	}
        }
        $this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED]);
    }


    public function delete($id = null)
    {
        $ticket = $this->Tickets->get($id);

        if($ticket&&$ticket->user_id==$this->Auth->user('id'))
        {
        	if ($this->Tickets->delete($ticket)) {
        		$this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_SUCCESS,'message'=>__('Ticket has been deleted.')]);
        	}
	    }
	    $this->renderJsonResponse(['status'=>ResponsCodes::$STATUS_FORM_FAILED]);
    }
}
