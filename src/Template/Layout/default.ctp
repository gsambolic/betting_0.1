
<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
    	<?= $this->Html->charset() ?>
        <title><?= $this->fetch('title') ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
     	<?= $this->Html->meta('icon') ?>
    	<?= $this->fetch('meta') ?>

   	 	<script>
   	 var BASE_URL = '<?php echo $this->Url->build("/",true);?>';
   	 function translate(string){return string}
   	 	</script>

        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <?= $this->Html->css('/assets/global/plugins/font-awesome/css/font-awesome.min.css') ?>
        <?= $this->Html->css('/assets/global/plugins/simple-line-icons/simple-line-icons.min.css') ?>
        <?= $this->Html->css('/assets/global/plugins/bootstrap/css/bootstrap.min.css') ?>
        <?= $this->Html->css('/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') ?>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <?= $this->Html->css('/assets/global/css/components.min.css') ?>
        <?= $this->Html->css('/assets/global/css/plugins.min.css') ?>
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <?= $this->Html->css('/assets/pages/css/about.min.css') ?>
       <?= $this->Html->css('/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') ?>
       <?= $this->Html->css('/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css') ?>
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <?= $this->Html->css('/assets/layouts/layout2/css/layout.min.css') ?>
        <?= $this->Html->css('/assets/layouts/layout2/css/themes/blue.min.css') ?>
        <?= $this->Html->css('/assets/layouts/layout2/css/custom.min.css') ?>

        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->
	<?= $this->Html->css('style.css?=t'.time()) ?>
	<?= $this->fetch('css') ?>

	            <?= $this->Html->script('/assets/global/plugins/jquery.min.js')?>
            <?= $this->Html->script('/assets/global/plugins/bootstrap/js/bootstrap.min.js')?>
            <?= $this->Html->script('/assets/global/plugins/js.cookie.min.js')?>
            <?= $this->Html->script('/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')?>
            <?= $this->Html->script('/assets/global/plugins/jquery.blockui.min.js')?>>
            <?= $this->Html->script('/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')?>
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner ">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a style="COLOR: WHITE;FONT-SIZE: 25PX;MARGIN-TOP: 13PX;" href="/">
                        KLADSTAT  </a>
                    <div  class="menu-toggler sidebar-toggler">
                        <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
                    </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN PAGE ACTIONS -->
                <!-- DOC: Remove "hide" class to enable the page header actions -->
               	 <?php
                 if ($this->request->session()->read('Auth.User.id'))
                 {
                 ?>
	                <div class="page-actions">
	                    <div class="btn-group">
	                        <a href="<?php echo $this->Url->build(["controller" => "bets", "action" => "entry", ]);?>" class="btn btn-circle btn-outline red  "  >
	                            <i class="fa fa-plus"></i>&nbsp;
	                            <span class=""><?php echo __('New')?>&nbsp;</span>&nbsp;
	                        </a>
	                    </div>
	                </div>
                <?php
                }
                ?>
                <!-- END PAGE ACTIONS -->
                <!-- BEGIN PAGE TOP -->
                <div class="page-top">
                    <!-- BEGIN HEADER SEARCH BOX -->
                    <!-- DOC: Apply "search-form-expanded" right after the "search-form" class to have half expanded search box -->

                    <!-- END HEADER SEARCH BOX -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                        	 <?php
                            if ($this->request->session()->read('Auth.User.id'))
                            {
                            ?>
                            <li  id="header_inbox_bar">
                                <a id="header_inbox_bar_link" href="<?php echo $this->Url->build([
    "controller" => "messages",
    "action" => "inbox",
]);?>"   >
                                    <i class="icon-envelope-open"></i>
                                    <span class="badge badge-default"> <?php echo @$new_message;?> </span>
                                </a>
                            </li>

                            <li  >
                                <a href="<?php echo $this->Url->build([
    "controller" => "users",
    "action" => "my-account",
]);?>" >
                               		 <button type="button" class="btn green-haze"><?php echo $this->request->session()->read('Auth.User.username')?></button>
                                </a>
                            </li>

                            <li >
                               <a href="<?php echo $this->Url->build([
									    "controller" => "Users",
									    "action" => "logout",
									]);?>" ><i class="icon-logout"></i></a>
                            </li>

                              <?php
                            }
                            else
                            {
                            ?>
                            <li class="dropdown ">
                             	<a type="button" href="<?php echo $this->Url->build([
    "controller" => "Users",
    "action" => "login",
]);?>" class="btn  green uppercase margin-top-10"><?php echo __('Login/Register')?></a>
                            </li>
                            <?php
                            }
                            ?>
                            <!-- END QUICK SIDEBAR TOGGLER -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END PAGE TOP -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- END SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                        <li class="nav-item start ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">Dashboard</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item start ">
                                    <a href="<?php echo $this->Url->build(["controller" => "dashboard", "action" => "index", ]);?>" class="nav-link ">
                                        <i class="fa fa-calendar"></i>
                                        <span class=""><?php echo __('Calendar')?></span>
                                    </a>
                                </li>
                                <li class="nav-item start ">
                                    <a href="<?php echo $this->Url->build(["controller" => "dashboard", "action" => "overview", ]);?>"   class="nav-link ">
                                        <i class="icon-graph"></i>
                                        <span class="title"><?php echo __('Overview')?></span>
                                    </a>
                                </li>
                                <li class="nav-item start ">
                                    <a href="javascript:void(0)" class="nav-link ">
                                        <i class="icon-graph"></i>
                                        <span class="title"><?php echo __('Stats')?></span>
                                    </a>
                                    <ul class="sub-menu">
                                      <li class="nav-item">
                                          <a href="<?php echo $this->Url->build(["controller" => "dashboard", "action" => "generator", ]);?>" class="nav-link">
                                              <i class="icon-camera"></i><?php echo __('Stats Generator')?></a>
                                      </li>
                                      <li class="nav-item">
                                          <a href="<?php echo $this->Url->build(["controller" => "dashboard", "action" => "sports", ]);?>" class="nav-link">
                                              <i class="icon-camera"></i><?php echo __('Sports/Groups')?></a>
                                      </li>

                                  </ul>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-folder"></i>
                                <span class="title"><?php echo __('Bets')?></span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu">
                            	 <li class="nav-item">
                                    <a href="<?php echo $this->Url->build(["controller" => "tickets", "action" => "index", ]);?>" class="nav-link">
                                        <i class="icon-settings"></i> <?php echo __('List Tickets')?>
                                        <span class="arrow"></span>
                                    </a>
                                </li>

                                <li class="nav-item">
                                     <a href="<?php echo $this->Url->build(["controller" => "bets", "action" => "index", ]);?>" class="nav-link">
                                         <i class="icon-settings"></i> <?php echo __('List Bets')?>
                                         <span class="arrow"></span>
                                     </a>
                                 </li>

                                <li class="nav-item">
                                    <a href="<?php echo $this->Url->build(["controller" => "bets", "action" => "index", ]);?>" class="nav-link">
                                        <i class="icon-settings"></i> <?php echo __('List')?>
                                        <span class="arrow"></span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?php echo $this->Url->build(["controller" => "bets", "action" => "add", ]);?>"   class="nav-link">
                                        <i class="icon-globe"></i> <?php echo __('Add')?>
                                        <span class="arrow"></span>
                                    </a>
                                </li>
                            </ul>
                        </li>



                          <li class="nav-item">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-folder"></i>
                                <span class="title"><?php echo __('Manage')?></span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item">
                                    <a href="<?php echo $this->Url->build(["controller" => "sports", "action" => "index", ]);?>" class="nav-link ">
                                        <i class="icon-settings"></i> <?php echo __('Sports')?>
                                        <span class="arrow"></span>
                                    </a>

                                </li>
                                <li class="nav-item">
                                    <a href="<?php echo $this->Url->build(["controller" => "groups", "action" => "index", ]);?>"   class="nav-link">
                                        <i class="icon-globe"></i> <?php echo __('Groups')?>
                                        <span class="arrow"></span>
                                    </a>
                                </li>

                                 <li class="nav-item">
                                    <a href="<?php echo $this->Url->build(["controller" => "bets-types", "action" => "index", ]);?>"   class="nav-link">
                                        <i class="icon-globe"></i> <?php echo __('Bet Types')?>
                                        <span class="arrow"></span>
                                    </a>
                                </li>

                                 <li class="nav-item">
                                    <a href="<?php echo $this->Url->build(["controller" => "tags", "action" => "index", ]);?>"   class="nav-link">
                                        <i class="icon-globe"></i> <?php echo __('Tags')?>
                                        <span class="arrow"></span>
                                    </a>
                                </li>

                            </ul>
                        </li>


                    </ul>
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content" id="contentRight">
                	<?= $this->Flash->render() ?>
                  <?= $this->fetch('content') ?>

            </div>
            <!-- END CONTENT -->
        </div>




            <!-- END QUICK NAV -->
            <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script>
<script src="../assets/global/plugins/ie8.fix.min.js"></script>
<![endif]-->
            <!-- BEGIN CORE PLUGINS -->

            <?= $this->Html->script('/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js')?>
            <?= $this->Html->script('/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js')?>
            <?= $this->Html->script('/assets/global/plugins/bootbox/bootbox.min.js')?>
            <?= $this->Html->script('/assets/pages/scripts/ui-bootbox.min.js')?>
            <?= $this->Html->script('/assets/global/scripts/app.min.js')?>
            <?= $this->Html->script('/assets/layouts/layout2/scripts/layout.min.js')?>
            <?= $this->Html->script('/assets/layouts/layout2/scripts/demo.min.js')?>
            <?= $this->Html->script('/assets/layouts/global/scripts/quick-sidebar.min.js')?>
            <?= $this->Html->script('/assets/layouts/global/scripts/quick-nav.min.js')?>
            <!-- END THEME LAYOUT SCRIPTS -->
            <?= $this->fetch('scriptBottom') ?>
        <?= $this->Html->script('script.js?=t'.time()) ?>

    </body>

</html>
